<?php 
include("packages/require.php");
// include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<div class="device-max">
	
	</div> 
    <script src="<?php echo $global['absolute-url'];?>js/home.js"></script>
	</body>	
</html>