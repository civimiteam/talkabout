<?php 
//include("packages/require.php");
//include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<div class="device-max">
		<!-- START SECTION HEADER -->
		<?php include("parts/part-header.php");?>
		<!-- END SECTION HEADER -->
		<!-- START SECTION BODY -->	
		<div class="section-body up2">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="title center border-top border-bottom font16px padding-top-5 padding-bottom-5 MontserratRegular">
							HOME / SHOP
						</div>
					</div>
				</div>
			</div>
			<div class="container margin-bottom-xs-50 margin-bottom-100">
				<div class="row up4">
					<div class="col-sm-2 hidden-xs">
						<div class="row margin-bottom-100">
							<div class="col-sm-12 pad0">
								<ul class="list-category pad0 font12">
									<li class="MontserratBlack down1 padding-bottom-10 border-bottom-grey block">MAKEUP</li>
									<li class="MontserratLight block">Matte Lipliquid</li>
								</ul>
							</div>
						</div>
						<div class="row margin-bottom-100">
							<div class="col-sm-12 pad0">
								<ul class="list-category pad0 font12">
									<li class="MontserratBlack down1 padding-bottom-10 border-bottom-grey block">SKINCARE</li>
								</ul>
							</div>
						</div>
					</div>
					<div class="col-sm-10 col-xs-12">
						<div class="row">
							<div class="banner-product" style="background-image: url('img/source/productPage_banner01.png');">
								<div class="description-text description-text-xs">
									<h1 class="font-85px font-30px-xs ACaslonProBold">"Claim"</h1>
									<p class="product-description-text product-description-text-xs white">A comfortable matte lipquid with light texture, intense pay-off, non-transfer formula, and stays put for hours</p>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-sm-4 col-xs-6">
								<div class="products center">
									<a href="#">
										<img src="img/source/productPage_sandsome.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
									</a>
									<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>SANDSOME</strong></h3>
									<h6 class="product-description-2 MontserratLight">Represent creativity for easy going personality</h6>
									<h6 class="padding-top-1" style="height: 14px"></h6>
									<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 160.000</h4>
									<div class="col-xs-12 pad0">
										<div class="col-sm-3 col-xs-0">
										</div>
										<div class="col-sm-6 col-xs-12 pad0">
											<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-xs-6">
								<div class="products center">
									<a href="#">
										<img src="img/source/productPage_marsala.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
									</a>
									<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>MARSALA</strong></h3>
									<h6 class="product-description-2 MontserratLight">For those who is mysteriously attractive, the chaming one</h6>
									<h6 class="padding-top-1" style="height: 14px"></h6>
									<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 160.000</h4>
									<div class="col-xs-12 pad0">
										<div class="col-sm-3 col-xs-0">
										</div>
										<div class="col-sm-6 col-xs-12 pad0">
											<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-xs-6">
								<div class="products center">
									<a href="#">
										<img src="img/source/productPage_agate.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
									</a>
									<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>AGATE</strong></h3>
									<h6 class="product-description-2 margin-bottom-0 MontserratLight">Lucky charm for the intellect, courageous, and tough one</h6>
									<h6 class="padding-top-1 MontserratBold font-color-grey" style="height: 14px; text-decoration: line-through;">IDR 160.000</h6>
									<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 149.999</h4>
									<div class="col-xs-12 pad0">
										<div class="col-sm-3 col-xs-0">
										</div>
										<div class="col-sm-6 col-xs-12 pad0">
											<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-xs-6">
								<div class="products center">
									<a href="detail-shop.php">
										<img src="img/source/productPage_mimosa.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
									</a>
									<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>MIMOSA</strong></h3>
									<h5 class="product-description-2 MontserratLight">She’s generous, kind, sensitive, lady like but a tough one</h5>
									<h6 class="padding-top-1 MontserratBold font-color-grey" style="height: 14px; text-decoration: line-through;">IDR 160.000</h6>
									<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 149.999</h4>
									<div class="col-xs-12 pad0">
										<div class="col-sm-3 col-xs-0">
										</div>
										<div class="col-sm-6 col-xs-12 pad0">
											<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
										</div>
									</div>
								</div>
							</div>
							<div class="col-sm-4 col-xs-6">
								<div class="products center">
									<a href="#">
										<img src="img/source/productPage_marsala.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
									</a>
									<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>MARSALA</strong></h3>
									<h5 class="product-description-2 MontserratLight">For those who is mysteriously attractive, the chaming one</h5>
									<h6 class="padding-top-1" style="height: 14px"></h6>
									<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 160.000</h4>
									<div class="col-xs-12 pad0">
										<div class="col-sm-3 col-xs-0">
										</div>
										<div class="col-sm-6 col-xs-12 pad0">
											<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END SECTION BODY -->
		<!-- START SECTION FOOTER -->
		<?php include("parts/part-footer.php");?>
		<!-- END SECTION FOOTER -->
	</div> 
    <script src="<?php echo $global['absolute-url'];?>js/global.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/home.js"></script>
</body>	
</html>