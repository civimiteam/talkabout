<div class="section-footer margin-top-xs-20 up5 center-xs">
	<div class="container">
		<div class="row">
			<div class="col-md-3 col-sm-6 col-xs-12 down1">
				<ul class="pad0">
					<li class="nav-footer up2"><a href="#"><strong>SHOP</strong></a></li>
					<li class="nav-footer up2"><a href="#"><strong>ABOUT US</strong></a></li>
					<li class="nav-footer up2"><a href="#"><strong>PRESS</strong></a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 margin-top-xs-10 margin-bottom-sm-50 margin-bottom-xs-20">
				<ul class="pad0">
					<li class="nav-footer up2"><a href="#"><strong>HELP & FAQ</strong></a></li>
					<li class="nav-footer up2"><a href="#"><strong>CONTACT</strong></a></li>
				</ul>
			</div>
			<div class="col-md-3 col-sm-6 margin-top-xs-10">
				<div class="footer-list">
					<ul class="pad0">
						<li class="nav-footer up2"><a href="#"><strong>PRIVACY POLICY</strong></a></li>
						<li class="nav-footer up2"><a href="#"><strong>TERMS OF USE</strong></a></li>	
					</ul>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="footer-list">
					<ul class="pad0">
						<li class="nav-footer up2"><strong>CS@THEYTALKABOUT.COM</strong></li>
						<ul class="list-sosmed up1" style="margin-left: -40px;">
							<li class="nav-footer socMed"><a href="#"><img src="<?php echo $global['absolute-url'];?>img/twitter.png" height="35px" width="35px" class="margin-left-5 margin-right-5"></a></li>
							<li class="nav-footer socMed"><a href="#"><img src="<?php echo $global['absolute-url'];?>img/instagram.png" height="35px" width="35px" class="margin-left-5 margin-right-5"></a></li>
							<li class="nav-footer socMed"><a href="#"><img src="<?php echo $global['absolute-url'];?>img/facebook.png" height="35px" width="35px" class="margin-left-5 margin-right-5"></a></li>
						</ul>
						<li class="nav-footer up1"><strong>NEWSLETTER</strong>
							<div class="subscribe-form up2">
								<form name="form-subscribe" action="" method="" class="subscribe" onsubmit="">
									<div class="input-group">
										<input name="url" type="hidden" value="" class="" placeholder="">
										<input type="text" id="input-subscribe" name="subscribe" class="form-control">
										<span class="input-group-btn">
											<button class="btn btn-subscribe" type="submit">Submit</button>
										</span>
									</div>
									<div id="alert-subscibe" class="alert-text"></div>
								</form>
							</div>
						</li>	
					</ul>
				</div>
			</div>
		</div>
		<div class="row background-grey-2 up3">
			<div class="col-xs-12">
				<div class="up15 margin-bottom-15">
					<p class="center margin-bottom-15 text-color-grey-2 font9 text-copyright"><strong>Copyright &copy; 2016 THEYTALKABOUT</strong></p>
				</div>
			</div>
		</div>
	</div>
</div>