<div id="section-slider">
	<div id="swiper-home" class="swiper-container homeswiper-container">
		<div class="swiper-wrapper homeswiper-wrapper">
			<?php foreach ($data_sliders as $data_slider) { ?>
				<div class="swiper-slide homeswiper-slide">
					<a href="<?=$data_slider['homepage_link']?>" class="slider-image">
						<img src="<?=$global['absolute-url'].$data_slider['homepage_img']?>" alt="image">
					</a>
				</div>
			<?php } ?>
		</div>
	</div>
</div>