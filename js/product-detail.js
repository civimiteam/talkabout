$(document).ready(function(){
	getSize();
    function getSize(){
        var maxHeight = 0;

        $(".col-product").each(function(){
         var thisH = $(this).height();
         if (thisH > maxHeight) { maxHeight = thisH; }
     });
        $(".col-product").height(maxHeight);
        console.log(maxHeight);
    }
    $(window).resize(function(){
        $(".col-product").height("auto");
        getSize();
    });
    var swiperProduct = new Swiper('#product-slider', {
        speed: 800,
        slidesPerView: 1,
        spaceBetween: 0,
        nextButton: '.pdetail-next',
		prevButton: '.pdetail-prev',
    });
    var productThmb = new Swiper('#product-thmb', {
        spaceBetween: 15,
        slidesPerView: 3,
        centeredSlides: true,
        slideToClickedSlide: true
    });
    swiperProduct.params.control = productThmb;
    productThmb.params.control = swiperProduct;
});