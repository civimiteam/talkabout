$(document).ready(function(){
    getSize();
    function getSize(){
        var maxHeight = 0;

        $(".col-product").each(function(){
         var thisH = $(this).height();
         if (thisH > maxHeight) { maxHeight = thisH; }
     });
        $(".col-product").height(maxHeight);
        console.log(maxHeight);
    }
    $(window).resize(function(){
        $(".col-product").height("auto");
        getSize();
    });
});