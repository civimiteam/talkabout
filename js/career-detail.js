function validateFormCareer(){
	var name = $("#career-name").val();
	var email = $("#career-email").val();
	var phone = $("#career-phone").val();
	var files = $("#career-file").val();
	var captcha = $("#career-captcha").val();
	var numformat = /^[0-9]+$/;
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

	if(name != ""){
		$("#career-name").removeClass("career-error");
		$(".cform-alert").html("");
	} else {
		$("#career-name").addClass("career-error");
		$(".cform-alert").html("Insert your fullname!");
		$("#career-name").focus();
		return false;
	}
	if(email != ""){
		if(email.match(mailformat)){
			$("#career-email").removeClass("career-error");
			$(".cform-alert").html("");
		} else {
			$("#career-email").addClass("career-error");
			$(".cform-alert").html("Incorrect email format!");
			$("#career-email").focus();
			return false;
		}
	} else {
		$("#career-email").addClass("career-error");
		$(".cform-alert").html("Insert your email address!");
		$("#career-email").focus();
		return false;
	}
	if(phone != ""){
		if(phone.match(numformat)){
			$("#career-phone").removeClass("career-error");
			$(".cform-alert").html("");
		} else {
			$("#career-phone").addClass("career-error");
			$(".cform-alert").html("Phone only contain number!");
			$("#career-phone").focus();
			return false;
		}
	} else {
		$("#career-phone").addClass("career-error");
		$(".cform-alert").html("Insert your phone number!");
		$("#career-phone").focus();
		return false;
	}
	if(files != ""){
		$("#btn-file").removeClass("career-error");
		$(".cform-alert").html("");
	} else {
		$("#btn-file").addClass("career-error");
		$(".cform-alert").html("Insert your files!");
		$("#btn-file").focus();
		return false;
	}
	if(captcha != ""){
		$("#career-captcha").removeClass("career-error");
		$(".cform-alert").html("");
	} else {
		$("#career-captcha").addClass("career-error");
		$(".cform-alert").html("Insert your captcha!");
		$("#career-captcha").focus();
		return false;
	}
}