function validateFormReg(){
  var email = $("#reg-email").val();
  var pass = $("#reg-pass").val();
  //var repass = $("#reg-repass").val();
  var name = $("#reg-name").val();
  var phone = $("#reg-phone").val();
  var address = $("#reg-address").val();
  var province = $("#input-state").val();
  var city = $("#input-city").val();
  var zip = $("#reg-zip").val();
  var country= $("#reg-country").val();
  var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
  var numberformat = /^[0-9]+$/;
  var check = document.getElementById('reg-checkbox');
  //var captcha = $("#reg-captcha").val(); 

  if(email != ""){
    if(email.match(mailformat)){ 
      $("#form-alert").html("");
      $("#form-alert").hide();
    } else {
      $("#form-alert").html("Incorrect email format! <i class='fa fa-exclamation-circle'></i>");
      $("#reg-email").focus();
      $("#form-alert").show();
      return false;
    }
  } else {
    $("#form-alert").html("Insert your email address! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-email").focus();
    $("#form-alert").show();
    return false;
  }
  if(pass != ""){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Insert your password! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-pass").focus();
    $("#form-alert").show();
    return false;
  }
  // if(repass != ""){
  //   $("#form-alert").html("");
  //   $("#form-alert").hide();
  // } else {
  //   $("#form-alert").html("Re type your password! <i class='fa fa-exclamation-circle'></i>");
  //   $("#reg-repass").focus();
  //   $("#form-alert").show();
  //   return false;
  // }
  // if(pass == repass){
  //   $("#form-alert").html("");
  //   $("#form-alert").hide();
  // } else {
  //   $("#form-alert").html("Your password did not match! <i class='fa fa-exclamation-circle'></i>");
  //   $("#reg-repass").focus();
  //   $("#form-alert").show();
  //   return false;
  // }
  if(name != ""){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Insert your full name! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-name").focus();
    $("#form-alert").show();
    return false;
  }
  if(phone != ""){
    if(phone.match(numberformat)){ 
      $("#form-alert").html("");
      $("#form-alert").hide();
    } else {
      $("#form-alert").html("Incorrect phone number format! <i class='fa fa-exclamation-circle'></i>");
      $("#reg-phone").focus();
      $("#form-alert").show();
      return false;
    }
  } else {
    $("#form-alert").html("Insert your phone number! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-phone").focus();
    $("#form-alert").show();
    return false;
  }
  if(address != ""){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Insert your address! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-address").focus();
    $("#form-alert").show();
    return false;
  }
  if(province != ""){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Insert your province! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-province").focus();
    $("#form-alert").show();
    return false;
  }
  if(city != ""){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Insert your city! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-city").focus();
    $("#form-alert").show();
    return false;
  }
  if(zip != ""){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Insert your zip code! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-zip").focus();
    $("#form-alert").show();
    return false;
  }
  // if(captcha != ""){
  //   $("#form-alert").html("");
  //   $("#form-alert").hide();               
  // } else {
  //   $("#form-alert").html("Please input the captcha <i class='fa fa-exclamation-circle'></i>");                               
  //   $("#reg-captcha").focus();
  //   $("#form-alert").show();
  //   return false;
  // }
  if(country != ""){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Insert your country! <i class='fa fa-exclamation-circle'></i>");
    $("#reg-country").focus();
    $("#form-alert").show();
    return false;
  }
  if(check.checked){
    $("#form-alert").html("");
    $("#form-alert").hide();
  } else {
    $("#form-alert").html("Please agree the terms and condition! <i class='fa fa-exclamation-circle'></i>");
    $("#form-alert").show();
    return false;
  }
}