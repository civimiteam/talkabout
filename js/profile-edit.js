function validateProfile() {
	// var password = $("#password").val();
	// var repassword = $("#repassword").val();
	var fullname = $("#fullname").val();
	var email = $("#email").val();
	var paket = $("#paket").val();
	var day = $("#day").val();
	var month = $("#month").val();
	var year = $("#year").val();
	var ktp = $("#ktp").val();
	var address = $("#address").val();
	var city = $("#city").val();
	var province = $("#province").val();
	var zip = $("#zip").val();
	var phone = $("#phone").val();
	var bank = $("#bank").val();
	var rek = $("#rek").val();
	var no_rek = $("#no_rek").val();
	var pinbb = $("#pinbb").val();
	var delivery = $("#delivery").val();
	var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
	// if(password != ""){
	// 	$("#alert-password").hide();
	// 	$("#alert-password").text("");
	// } else {
	// 	$("#password").focus();
	// 	$("#alert-password").show();
	// 	$("#alert-password").text("masukan password anda!");
	// 	return false;
	// }
	// if(repassword != ""){
	// 	$("#alert-repassword").hide();
	// 	$("#alert-repassword").text("");
	// } else {
	// 	$("#repassword").focus();
	// 	$("#alert-repassword").show();
	// 	$("#alert-repassword").text("ketik ulang password anda!");
	// 	return false;
	// }
	// if(password == repassword){
	// 	$("#alert-repassword").hide();
	// 	$("#alert-repassword").text("");
	// } else {
	// 	$("#repassword").focus();
	// 	$("#alert-repassword").show();
	// 	$("#alert-repassword").text("password yang anda ketik tidak sama!");
	// 	return false;
	// }
	if(fullname != ""){
		$("#alert-fullname").hide();
		$("#alert-fullname").text("");
	} else {
		$("#fullname").focus();
		$("#alert-fullname").show();
		$("#alert-fullname").text("masukan nama lengkap anda!");
		return false;
	}
	if(email != ""){
		if(email.match(mailformat)){
			$("#alert-email").hide();
			$("#alert-email").text("");
		} else {
			$("#email").focus();
			$("#alert-email").show();
			$("#alert-email").text("format email yang anda masukan salah!");
			return false;
		}
	} else {
		$("#email").focus();
		$("#alert-email").show();
		$("#alert-email").text("masukan email anda!");
		return false;
	}
	if(paket != ""){
		$("#alert-paket").hide();
		$("#alert-paket").text("");
	} else {
		$("#paket").focus();
		$("#alert-paket").show();
		$("#alert-paket").text("pilih paket anda!");
		return false;
	}
	if(day != ""){
		$("#alert-dob").hide();
		$("#alert-dob").text("");
	} else {
		$("#day").focus();
		$("#alert-dob").show();
		$("#alert-dob").text("Pilih tanggal lahir anda!");
		return false;
	}
	if(month != ""){
		$("#alert-dob").hide();
		$("#alert-dob").text("");
	} else {
		$("#month").focus();
		$("#alert-dob").show();
		$("#alert-dob").text("masukan bulan lahir anda!");
		return false;
	}
	if(year != ""){
		$("#alert-dob").hide();
		$("#alert-dob").text("");
	} else {
		$("#year").focus();
		$("#alert-dob").show();
		$("#alert-dob").text("masukan tahun lahir anda!");
		return false;
	}
	if(ktp != ""){
		$("#alert-ktp").hide();
		$("#alert-ktp").text("");
	} else {
		$("#ktp").focus();
		$("#alert-ktp").show();
		$("#alert-ktp").text("masukan nomor ktp anda!");
		return false;
	}
	if(address != ""){
		$("#alert-address").hide();
		$("#alert-address").text("");
	} else {
		$("#address").focus();
		$("#alert-address").show();
		$("#alert-address").text("masukan alamat anda tinggal saat ini!");
		return false;
	}
	if(city != ""){
		$("#alert-city").hide();
		$("#alert-city").text("");
	} else {
		$("#city").focus();
		$("#alert-city").show();
		$("#alert-city").text("masukan kota tempat anda tinggal!");
		return false;
	}
	if(province != ""){
		$("#alert-province").hide();
		$("#alert-province").text("");
	} else {
		$("#province").focus();
		$("#alert-province").show();
		$("#alert-province").text("masukan provinsi tempat anda tinggal!");
		return false;
	}
	if(zip != ""){
		$("#alert-zip").hide();
		$("#alert-zip").text("");
	} else {
		$("#zip").focus();
		$("#alert-zip").show();
		$("#alert-zip").text("masukan kode pos anda!");
		return false;
	}
	if(phone != ""){
		$("#alert-phone").hide();
		$("#alert-phone").text("");
	} else {
		$("#phone").focus();
		$("#alert-phone").show();
		$("#alert-phone").text("masukan nomor telepon atau handphone anda!");
		return false;
	}
	if(bank != ""){
		$("#alert-bank").hide();
		$("#alert-bank").text("");
	} else {
		$("#bank").focus();
		$("#alert-bank").show();
		$("#alert-bank").text("masukan nama bank anda!");
		return false;
	}
	if(rek != ""){
		$("#alert-rek").hide();
		$("#alert-rek").text("");
	} else {
		$("#rek").focus();
		$("#alert-rek").show();
		$("#alert-rek").text("masukan nama pemilik kartu ATM!");
		return false;
	}
	if(no_rek != ""){
		$("#alert-no_rek").hide();
		$("#alert-no_rek").text("");
	} else {
		$("#no_rek").focus();
		$("#alert-no_rek").show();
		$("#alert-no_rek").text("masukan nomor rekening!");
		return false;
	}
	if(pinbb != ""){
		$("#alert-pinbb").hide();
		$("#alert-pinbb").text("");
	} else {
		$("#pinbb").focus();
		$("#alert-pinbb").show();
		$("#alert-pinbb").text("masukan pin BBM anda!");
		return false;
	}
	if(delivery != ""){
		$("#alert-delivery").hide();
		$("#alert-delivery").text("");
	} else {
		$("#delivery").focus();
		$("#alert-delivery").show();
		$("#alert-delivery").text("pilih kota tujuan pengiriman!");
		return false;
	}
	return true;
}