function validateFormAcc(){
    var name = $("#acc-name").val();
    var phone = $("#acc-phone").val();

    if(name != ""){
        $("#form-alert").html("");
        $("#form-alert").hide(); 
    } else {
        $("#form-alert").html("Insert your name! <i class='fa fa-exclamation-circle'></i>");
        $("#acc-name").focus();
        $("#form-alert").show();
        return false;
    }
    if(phone != ""){
        $("#form-alert").html("");
        $("#form-alert").hide(); 
    } else {
        $("#form-alert").html("Insert your phone number! <i class='fa fa-exclamation-circle'></i>");
        $("#acc-phone").focus();
        $("#form-alert").show();
        return false;
    }
}