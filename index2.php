<?php 
//include("packages/require.php");
//include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<div class="device-max">
		<!-- START SECTION HEADER -->
		<?php include("parts/part-header.php");?>
		<!-- END SECTION HEADER -->
		<!-- START SECTION BODY -->	
		<div class="section-body">
			<div class="container" style="width: 100%;">
				<div class="row">
					<div class="col-xs-12">
						<div class="swiper-container home-swiper" style="width: 100%">
			        		<div class="swiper-wrapper">
					            <div class="swiper-slide"><a href=""><img src="<?php echo $global['absolute-url'];?>img/source/banner01.jpg" width="100%"></a></div>
					            <div class="swiper-slide"><a href=""><img src="<?php echo $global['absolute-url'];?>img/source/banner01.jpg" width="100%"></a></div>
					            <div class="swiper-slide"><a href=""><img src="<?php echo $global['absolute-url'];?>img/source/banner01.jpg" width="100%"></a></div>
					            <div class="swiper-slide"><a href=""><img src="<?php echo $global['absolute-url'];?>img/source/banner01.jpg" width="100%"></a></div>
					            <div class="swiper-slide"><a href=""><img src="<?php echo $global['absolute-url'];?>img/source/banner01.jpg" width="100%"></a></div>
					        </div>
			        		<div class="swiper-pagination home-pagination"></div>
			    		</div>
			    	</div>
			    </div>
    		</div>
    		<div class="row up5 margin-top-xs-20">
    			<div class="col-sm-5 col-xs-12">
    				<div class="product1">
   						<a href=""><img src="<?php echo $global['absolute-url'];?>img/source/powder.png" width="100%"></a>
   					</div>
    			</div>
    			<div class="col-sm-7 padding-right-0 padding-left-0">
    				<div class="row">
    					<div class="col-sm-6 col-xs-12">
	    					<div class="product2 margin-top-xs-20">
	    						<a href=""><img src="<?php echo $global['absolute-url'];?>img/source/share-to.png" width="100%"></a>
    						</div>
    					</div>
    					<div class="col-sm-6 col-xs-12">
	    					<div class="product3 margin-top-xs-20">
	    						<a href=""><img src="<?php echo $global['absolute-url'];?>img/source/womantalkabout (1).png" width="100%"></a>
    						</div>
    					</div>
    				</div>
    			</div>
    		</div>
    		<div class="row up5 margin-top-xs-20">
    			<div class="col-xs-12">
    				<strong><h3 class="text-title center">Share your #theytalkabout style!</h3></strong>
    			</div>
    		</div>
    		<div class="row up5">
    			<div class="col-xs-12 hidden-xs hidden-sm visible-md visible-lg">
    				<div id="swiper-footer" class="swiper-container home-swiper-2 down2" style="width: 100%">
        				<div class="swiper-wrapper" style="width: 100%">
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy1.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy2.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy3.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy4.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy5.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy6.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy7.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy8.jpg);"></a>
				            </div>
				        </div>
			    	</div>
    			</div>
    			<div class="col-xs-12 visible-sm hidden-xs">
    				<div id="swiper-footer-sm" class="swiper-container home-swiper-2 down2" style="width: 100%">
        				<div class="swiper-wrapper" style="width: 100%">
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy1.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy2.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy3.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy4.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy5.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy6.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy7.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy8.jpg);"></a>
				            </div>
				        </div>
			    	</div>
    			</div>
    			<div class="col-xs-12 hidden-sm visible-xs">
    				<div id="swiper-footer-xs" class="swiper-container home-swiper-3 down2" style="width: 100%">
        				<div class="swiper-wrapper" style="width: 100%">
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy1.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy2.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy3.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy4.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy5.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy6.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy7.jpg);"></a>
				            </div>
				            <div class="swiper-slide">
				            	<a href="" class="swiper-image" style="background-image: url(<?php echo $global['absolute-url'];?>img/source/soyjoy8.jpg);"></a>
				            </div>
				        </div>
			    	</div>
    			</div>
    		</div>
    		<div class="row up4">
    			<div class="col-xs-12">
    				<div class="prev-slide"><img src="<?php echo $global['absolute-url'];?>img/left-chevron.png"></div>
					<div class="next-slide"><img src="<?php echo $global['absolute-url'];?>img/right-chevron.png"></div>
    			</div>
    		</div>
		</div>
		<!-- END SECTION BODY -->
		<!-- START SECTION FOOTER -->
		<?php include("parts/part-footer.php");?>
		<!-- END SECTION FOOTER -->
	</div> 
    <script src="<?php echo $global['absolutee-url'];?>js/global.js"></script>
    <script src="<?php echo $global['absolutee-url'];?>js/home.js"></script>
   	<script>
		var swiper = new Swiper('.home-swiper', {
	        pagination: '.home-pagination',
	        paginationClickable: true,
			autoplay:  5000,
			speed: 800,
			parallax: false,
			slidesPerView: 1,
			loop:true,
			paginationClickable: true,
			spaceBetween: 0
		});
	    var swiperFooter = new Swiper('#swiper-footer', {
	        nextButton: '.next-slide',
	        prevButton: '.prev-slide',
	        slidesPerView: 5,
	        paginationClickable: true,
	        spaceBetween: 30,
	    });
	    var swiperFooterSM = new Swiper('#swiper-footer-sm', {
	        nextButton: '.next-slide',
	        prevButton: '.prev-slide',
	        slidesPerView: 3,
	        paginationClickable: true,
	        spaceBetween: 30,
	    });
	    var swiperFooterXS = new Swiper('#swiper-footer-xs', {
	        nextButton: '.next-slide',
	        prevButton: '.prev-slide',
	        slidesPerView: 2,
	        paginationClickable: true,
	        spaceBetween: 30,
	    });
    </script>
</body>	
</html>