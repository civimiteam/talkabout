<?php 
//include("packages/require.php");
//include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<div class="device-max">
		<!-- START SECTION HEADER -->
		<?php include("parts/part-header.php");?>
		<!-- END SECTION HEADER -->
		<!-- START SECTION BODY -->	
		<div class="section-body up2">
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12">
						<div class="title center border-top border-bottom font16px padding-top-5 padding-bottom-5 MontserratRegular">
							LOOKBOOK
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12 pad0">
						<div class="up4">
							<ol class="breadcrumb background-white MontserratLight">
								<li class="breadcrumb-item"><a href="index2.php">HOME</a></li>
							  	<li class="breadcrumb-item active">LOOKBOOK</li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div>
							<a href="#">
								<img src="<?php echo $global['absolute-url'];?>img/Lookbook/lookbookBanner.png" width="100%">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="padding-right-30 padding-left-30 padding-xs-0">
				<div class="row">
					<div class="col-sm-6 col-xs-12 up6 margin-top-xs-35 padding-xs-0">
						<a href="#">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/01/37-edit.jpg" style="width: 100%" class="padding-left-15 padding-right-15">
						</a>
					</div>
					<div class="col-sm-6 col-xs-12 up6 margin-top-xs-20 padding-xs-0">
						<a href="quirkyparty.php">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/02/LB-1_copy.jpg" style="width: 100%" class="padding-left-15 padding-right-15">
						</a>
					</div>
				</div>
				<div class="row margin-bottom-sm-60 margin-bottom-xs-15 padding-xs-0">
					<div class="col-sm-6 col-xs-12 up7 margin-top-xs-20">
						<a href="#">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/03/1-new.jpg" style="width: 100%" class="padding-left-15 padding-right-15">
						</a>
					</div>
					<div class="col-sm-6 col-xs-12 up7 margin-top-xs-20 padding-xs-0">
						<a href="#">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/04/940x669.jpg" style="width: 100%" class="padding-left-15 padding-right-15">
						</a>
					</div>
				</div>
			</div>
		</div>
	<!-- END SECTION BODY -->
	<!-- START SECTION FOOTER -->
	<?php include("parts/part-footer.php");?>
	<!-- END SECTION FOOTER -->
	</div>
    <script src="<?php echo $global['absolute-url'];?>js/global.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/home.js"></script>
</body>	
</html>