<?php 
//include("packages/require.php");
//include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<div class="device-max">
		<!-- START SECTION HEADER -->
		<?php include("parts/part-header.php");?>
		<!-- END SECTION HEADER -->
		<!-- START SECTION BODY -->	
		<div class="section-body up2">
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12">
						<div class="title center border-top border-bottom font16px padding-top-5 padding-bottom-5 MontserratRegular">
							BLOG
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12 pad0">
						<div class="up4">
							<ol class="breadcrumb background-white MontserratLight">
								<li class="breadcrumb-item"><a href="index2.php">HOME</a></li>
								<li class="breadcrumb-item"><a href="blog.php">BLOG</a></li>
							  	<li class="breadcrumb-item active">Summer color that catches eyes</li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row down1">
					<div class="col-xs-12 center">
						<div class="artikel-theme"><a href="#">Beauty</a></div>
						<div class="artikel-theme"><a href="#">Colors</a></div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 center">
						<div class="blog-content-title blog-content-title-xs">
							Summer color that catches the eyes
						</div>
					</div>
				</div>
				<div class="row down3">
					<div class="col-xs-12 center">
						<div class="blog-content-date blog-content-date-xs">
							February 24, 2017
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row down5 margin-bottom-xs-20">
					<div class="col-xs-12">
						<div>
							<img src="<?php echo $global['absolute-url'];?>img/Blog/pexels-photo-27696-cropped.jpg" style="width: 100%">
						</div>
					</div>
				</div>
			</div>
			<div class="container">
				<div class="row down5 margin-bottom-xs-0">
					<div class="col-xs-12 margin-bottom-xs-20 padding-left-60 padding-right-60 padding-left-xs-15 padding-right-xs-15">
						<div class="artikel-content artikel-content-xs">
							<p>
								I am back with another review of the most famous products on Instagram thorough 2014 in my opinion, it is Anastasia Beverly Hills Dipbrow Pomade that breaks the internet!! Yes, not only Kim Kardashian who can do that lol.
							</p>
							<p>
								I couldn’t remember when I first knew about Anastasia Beverly Hills Dipbrow Pomade, all I knew is that it is just as popular as Urban Decay NAKED because everyone uses this!! Like literally, almost all make up gurus overseas [mostly USA] swore by it. And thanks to that, I also knew about it and wanted to try it for so long! However it is not available yet in Indonesia so mostly people get it from online stores. Now let’s review it yeaayy
							</p>
							<p>
								There are color swatches for consumers to check which box is which through the box. The color is slightly different from the real thing though imo. So finding swatches from internet and bloggers are more reliable I think.
							</p>
							<p>
								The box packaging is just like any normal professional make up products packaging, with sleek black design and white formal typography that explains the ingredients and the whereabout of this product.
							</p>
							<p>
								The best thing about Anastasia Beverly Hills is they are cruelty free. So for those of you who are animal lover and don’t want to support any brands that test on animal cruelly, you can believe yourself with the brand.
							</p>
							<p>
								Anastasia Beverly Hills Dipbrow Pomade comes in a small glass bottle that is pretty heavy and wide. The design reminds me of any gel eyeliner available in the market, and considering that this product is a gel based product for eyebrow that has similarity with it, that explains. On the cap they print out the logo of Anastasia Beverly Hills that is a represented as a beautiful typography of a letter of A, that has butterfly shape.
							</p>
							<p>
								Although there’s no way to tell the color from the packaging itself, they print out the color name and the simple explanation on the bottom of the bottle.
							</p>
							<p>	
								I mostly use Dark Brown or Taupe for my eyebrow because I like ashy tone and my hair is ash blonde too so it suits me best. Ebony suits people with dark hair more, and Auburn and Chocolate suits people who are red haired or warm-toned hair.
							</p>
						</div>
					</div>					
				</div>			
			</div>
			<div class="container">
				<div class="row margin-bottom-xs-10">
					<div class="col-xs-12 padding-left-60 padding-right-60 padding-left-xs-15 padding-right-xs-15">
						<div class="font-30 MontserratRegular">
							Popular Post
						</div>
					</div>
				</div>
				<div class="row margin-bottom-80 margin-bottom-xs-0 padding-left-48 padding-right-48 padding-left-xs-15 padding-right-xs-15">
					<div class="col-sm-4 col-xs-12 margin-bottom-xs-20 padding-xs-0">
						<div class="popular-post">
							<a href="#">
								<img src="<?php echo $global['absolute-url'];?>img/Blog/pexels-photo-199165.jpg" style="width: 100%">
							</a>
							<a href="#">
								<h3 class="artikel-title-small up05"><strong>It's Never Too Much Sexyness</strong></h3>
							</a>
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 margin-bottom-xs-20 padding-xs-0">
						<div class="popular-post">
							<a href="#">
								<img src="<?php echo $global['absolute-url'];?>img/Blog/pexels-photo-47401.jpg" style="width: 100%">
							</a>
							<a href="#">
								<h3 class="artikel-title-small up05"><strong>The Definitive Order Of All Your Holiday Beauty Prep</strong></h3>
							</a>
						</div>
					</div>
					<div class="col-sm-4 col-xs-12 margin-bottom-xs-20 padding-xs-0">
						<div class="popular-post">
							<a href="#">
								<img src="<?php echo $global['absolute-url'];?>img/Blog/14799024431812376333Blog-8.png" style="width: 100%">
							</a>
							<a href="#">
								<h3 class="artikel-title-small up05"><strong>L.A. Girl Pro Conceal HD Concealer Review</strong></h3>
							</a>
						</div>
					</div>
				</div>
			</div>
				
		</div>
		<!-- END SECTION BODY -->
		<!-- START SECTION FOOTER -->
		<?php include("parts/part-footer.php");?>
		<!-- END SECTION FOOTER -->
	</div> 
    <script src="<?php echo $global['absolute-url'];?>js/global.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/home.js"></script>
</body>	
</html>