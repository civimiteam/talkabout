<?php
if(!function_exists('smtpmailer'))
{
    function smtpmailer($to, $from_url, $from, $from_password, $from_name, $subject, $body){
        global $error;
        $mail = new PHPMailer(); // create a new object
        $mail->IsSMTP(); // enable SMTP
        $mail->SMTPDebug = ''; // debugging: 1 = errors and messages, 2 = messages only
        $mail->SMTPAuth = true; // authentication enabled
        $mail->SMTPSecure = ''; // secure transfer enabled REQUIRED for Gmail
        $mail->Host = $from_url;
        $mail->Port = 587;
        $mail->Username = $from;
        $mail->Password = $from_password;
        $mail->SetFrom($from, $from_name);
        $mail->Subject = $subject;
        $mail->isHTML(true); //Send HTML or Plain Text email
        $mail->Body = $body;
        $mail->AddAddress($to);
        if(!$mail->Send()) {
            $error = 'Mail error: '.$mail->ErrorInfo;
            return false;
        } else {
            $error = 'Message sent!';
            return true;
        }
    }
}

if(!function_exists('metaDesc'))
{
    function metaDesc($string,$length){
        $content = stripslashes(strip_tags(htmlspecialchars_decode($string, ENT_QUOTES)));
        $content = preg_replace("/<p[^>]*?>/", "", $content);
        $content = str_replace("</p>", "<br />", $content);
        $content = str_replace('"', "", $content);
        $strLength = strlen($content);
        if ($strLength > $length) {
            $content = substr($content, 0, $length) . "...";
        }
        return $content;
    }
}
if(!function_exists('checkFont'))
{
    function checkFont($data) {
        $result = "";

        $strLength = strlen($data);
        if($strLength > 35){
            $result = "refont-35";
        } else if($strLength > 55){
            $result = "refont-50";
        }
        return $result;
    }
}
if(!function_exists('convert_status'))
{
    function convert_status($status){
        $result = "";
        switch ($status) {
            case 7:
                $result = "<span class='label label-danger'>ON HOLD</span>";
                break;
            case 6:
                $result = "<span class='label label-danger'>CANCELED</span>";
                break;
            case 5:
                $result = "<span class='label label-success'>COMPLETED</span>";
                break;
            case 4:
                $result = "<span class='label label-success'>SHIPPED</span>";   
                break;
            case 3:
                $result = "<span class='label label-info'>ON PROGRESS</span>"; 
                break;
            case 2:
                $result = "<span class='label label-warning'>VERIFYING PAYMENT</span>";  
                break;
            case 1:
                $result = "<span class='label label-default'>AWAITING PAYMENT</span>";
                break;
            default:
                $result = "<span class='label label-default'>AWAITING PAYMENT</span>";
                break;
        }
        return $result ;                                       
    }
}
// checkrupiah khusus cheveux
if(!function_exists('checkRupiah'))
{
    function checkRupiah($input) {
        $precision = 1;

        if($input < 900) {
            // 0 - 900
            $n_format = number_format($input, $precision);
            $suffix = '';
        } else if ($input < 900000) {
            // 0.9k-850k
            $n_format = number_format($input / 1000, $precision);
            $suffix = 'K';
        } else if ($input < 900000000) {
            // 0.9m-850m
            $n_format = number_format($input / 1000000, $precision);
            $suffix = 'M';
        } else if ($input < 900000000000) {
            // 0.9b-850b
            $n_format = number_format($input / 1000000000, $precision);
            $suffix = 'B';
        } else {
            // 0.9t+
            $n_format = number_format($input / 1000000000000, $precision);
            $suffix = 'T';
        }
        // Remove unecessary zeroes after decimal. "1.0" -> "1"; "1.00" -> "1"
        // Intentionally does not affect partials, eg "1.50" -> "1.50"
        if ( $precision > 0 ) {
            $dotzero = '.' . str_repeat( '0', $precision );
            $n_format = str_replace( $dotzero, '', $n_format );
        }
        return $n_format . $suffix;
    }
}

if(!function_exists('contentDisplay'))
{
    function contentDisplay($data, $size) {
        $result = "";

        $string = preg_replace("/<img[^>]+\>/i", " ", $data);
        $content = htmlspecialchars_decode(stripslashes($string), ENT_QUOTES);
        $strLength = strlen($content);
        if ($strLength > $size) {
            $result = substr(strip_tags($content), 0, $size) . "...";
        } else {
            $result = $content;
        }

        return $result;
    }
}
if(!function_exists('get_user_image'))
{
    function get_user_image($url){
        if(isset($url) && $url != ""){
            if(strpos($url, "http://") > -1 || strpos($url, "https://") > -1){
                $result = $url;
            }//end check if contains http://
            else{
                $result = "https://www.eannovate.com/dev/brown/admin2/".$url;
            }//end else if not contains http://
        }else{
            $result = "https://www.eannovate.com/dev/brown/images/dummy.jpg";
        }
        return $result;
    }
}

if(!function_exists('checkImage'))
{
    function checkImage($url,$img){
        $result = "";

        $full_url = $url.$img;
        if(isset($img) && $img != ""){
            if (file_exists($_SERVER['DOCUMENT_ROOT']."/dev/fas/".$img)) {
                $result = $full_url;
            } else {
                $result = $url."images/no-image.png";
            }
        } else {
            $result = $url."images/no-image.png";
        }

        return $result;
    }
}


if(!function_exists('contentDisplay'))
{
    function contentDisplay($data_short, $data_long, $size) {
        $result = "";

        if($data_short != ""){
            $content = htmlspecialchars_decode(stripslashes($data_short), ENT_QUOTES);
            $strLength = strlen($content);
            if ($strLength > $size) {
                $result = substr(strip_tags($content), 0, $size) . "...";
            } else {
                $result = $content;
            }
        } else {
            $string = preg_replace("/<img[^>]+\>/i", " ", $data_long);
            $content = htmlspecialchars_decode(stripslashes($string), ENT_QUOTES);
            $strLength = strlen($content);
            if ($strLength > $size) {
                $result = substr(strip_tags($content), 0, $size) . "...";
            } else {
                $result = $content;
            }
        }

        return $result;
    }
}
if(!function_exists('checkLastPost'))
{
    function checkLastPost($post, $datas, $anonymous="no"){
        $result = "";

        if($post == "post_date"){
            if($datas == "0000-00-00 00:00:00" || $datas == NULL){ 
                $result = "-";
            }else{
                $result = relative_time($datas);
            }
        }
        else if($post == "posted_by"){
            if($datas != '' || $datas != NULL){
                if($anonymous == "yes"){
                    $result = "Anonymous";
                }else{
                    $result = $datas;
                }
            }else{
                $result = "-";
            }
        }
        return $result;
    }
}

if(!function_exists('checkAnonymous'))
{
    function checkAnonymous($anonymous="no", $user){
        $result = "";

        if($user != '' || $user != NULL){
            if($anonymous == "yes"){
                $result = "Anonymous";
            }else{
                $result = $user;
            }
        }else{
            $result = "-";
        }
        return $result;
    }
}

if(!function_exists('checkLang'))
{
    function checkLang($lang){
        $result = "";

        if($lang == 'id'){
            if (strpos($_SERVER['REQUEST_URI'], "/en/") !== false){
                $oldurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $result = str_replace('/en/', '/id/', $oldurl);
            } else {
                $result = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            }
        } else {
            if (strpos($_SERVER['REQUEST_URI'], "/id/") !== false){
                $oldurl = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $result = str_replace('/id/', '/en/', $oldurl);
            } else {
                $result = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
            }
        }
        return $result;
    }
}

if(!function_exists('detectResult'))
{
    function detectResult($frequent, $action, $val){
        $result = 0;

        if($frequent >= 3){
            if($action == 1 && $val == 1){
                $result = 1;
            }else if($action == 1 && $val == 2){
                $result = 2;
            }else if($action == 1 && ($val == 3 || $val == 4)){
                $result = 3;
            }else if($action == 1 && $val == 5){
                $result = 4;
            }else if($action == 2 && $val == 1){
                $result = 5;
            }else if($action == 2 && $val == 2){
                $result = 6;
            }else if($action == 2 && ($val == 3 || $val == 4)){
                $result = 7;
            }else if($action == 2 && $val == 5){
                $result = 8;
            }else if($action == 3 && $val == 1){
                $result = 9;
            }else if($action == 3 && $val == 2){
                $result = 10;
            }else if($action == 3 && ($val == 3 || $val == 4)){
                $result = 11;
            }else if($action == 3 && $val == 5){
                $result = 12;
            } 
        }else if($frequent < 3){
            $result = 13;
        }else{
            $result = 0;
        }
        return $result;
    }
}

if(!function_exists('generateRandomString'))
{
    function generateRandomString($length){
        $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
        $charactersLength = strlen($characters);
        $randomString = "";
        for($i = 0; $i < $length; $i++){
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}

if(!function_exists('correctText'))
{
    function correctText($data){
        $data = nl2br(htmlspecialchars_decode(stripslashes($data), ENT_QUOTES));
        return $data;
    }
}

if(!function_exists('check_image_url'))
{
    //function for check retrieved image
    function check_image_url($image_url){
        $result = 0;

        if($image_url != null){
            if(strpos($image_url, "http://") > -1 || strpos($image_url, "https://") > -1){
                $result = $image_url;
            }//end check if contains http://
            else{
                $result = "http://www.eannovate.com/dev/ab/".$image_url;
            }//end else if not contains http://
        }else{
            $result = "http://www.eannovate.com/dev/ab/img/dummy.jpg";
        }
        return $result;
    }
}

if(!function_exists('cleanSpace'))
{
    function cleanSpace($string) {
        $string = trim($string);
        while(strpos($string,' ')){
            $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        }
        return $string;
    }

}

if(!function_exists('checkInterest'))
{
    function checkInterest($val){
        $result = "";
        if($val == "1"){
            $result = "Open for any new opportunities";
        } else if ($val == "2"){
            $result = "Available for freelance work";
        } else if ($val == "3"){
            $result = "Looking for a full-time job";
        } else if ($val == "4"){
            $result = "Looking for an internship";
        } else if ($val == "5"){
            $result = "Starting-up a company";
        } else if ($val == "6"){
            $result = "Getting higher education";
        } else if ($val == "7"){
            $result = "Finishing college";
        } else if ($val == "8"){
            $result = "Not so sure yet";
        } else {
            $result = "";
        }
        return $result;
    }
}

if(!function_exists('replace_rate'))
{
    function replace_rate($param){
        $rate = "";
        if($param == 1){
          $rate = "Beginner";
        } else if ($param == 2){
          $rate = "Intermediate";
        } else if ($param == 3){
          $rate = "Advanced";
        } else if ($param == 4){
          $rate = "Expert";
        } else if ($param == 5){
          $rate = "Native";
        }
        return $rate;
    }
}

if(!function_exists('calculate_age'))
{
    function calculate_age($date){
        list($day, $month, $year) = explode("-",$date);
        $year_diff  = date("Y") - $year;
        $month_diff = date("m") - $month;
        $day_diff   = date("d") - $day;
        if ($day_diff < 0 && $month_diff==0) $year_diff--;
        if ($day_diff < 0 && $month_diff < 0) $year_diff--;
        return $year_diff;
    }
}

if(!function_exists('create_session'))
{
    function create_session($id, $fname, $email, $via){
        $_SESSION['customer_id'] = $id;
        $_SESSION['customer_name'] = $fname;
        $_SESSION['customer_email'] = $email;
        $_SESSION['customer_via'] = $via;
    }
}

if(!function_exists('end_session'))
{
    function end_session(){
        unset($_SESSION['customer_id']);
        unset($_SESSION['customer_name']);
        unset($_SESSION['customer_email']);
        unset($_SESSION['customer_via']);
    }
}

if(!function_exists('generate_code'))
{
    function generate_code($length=6,$type=1){
        $key = '';
        switch($type){
            case 2:
            $pattern = "abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz";
            break;
            case 3:
            $pattern = "12345678901234567890123456789012345678901234567890";
            break;
            default:
            $pattern = "1234567890abcdefghijklmnopqrstuvwxyz";
            break;
        }
        for($i=0;$i<$length;$i++){
            $key .= $pattern{rand(0,35)};
        }
        return strtoupper($key);
    }
}

if(!function_exists('relative_time'))
{
    function relative_time($ts)
    {
        if(!ctype_digit($ts))
            $ts = strtotime($ts);

        $diff = time() - $ts;
        if($diff == 0)
            return 'now';
        elseif($diff > 0)
        {
            $day_diff = floor($diff / 86400);
            if($day_diff == 0)
            {
                if($diff < 60) return 'just now';
                if($diff < 120) return '1 minute ago';
                if($diff < 3600) return floor($diff / 60) . ' minutes ago';
                if($diff < 7200) return '1 hour ago';
                if($diff < 86400) return floor($diff / 3600) . ' hours ago';
            }
            if($day_diff == 1) return 'Yesterday';
            if($day_diff < 7) return $day_diff . ' days ago';
            if($day_diff < 31) return ceil($day_diff / 7) . ' weeks ago';
            if($day_diff < 60) return 'last month';
            return date('F Y', $ts);
        }
        else
        {
            $diff = abs($diff);
            $day_diff = floor($diff / 86400);
            if($day_diff == 0)
            {
                if($diff < 120) return 'in a minute';
                if($diff < 3600) return 'in ' . floor($diff / 60) . ' minutes';
                if($diff < 7200) return 'in an hour';
                if($diff < 86400) return 'in ' . floor($diff / 3600) . ' hours';
            }
            if($day_diff == 1) return 'Tomorrow';
            if($day_diff < 4) return date('l', $ts);
            if($day_diff < 7 + (7 - date('w'))) return 'next week';
            if(ceil($day_diff / 7) < 4) return 'in ' . ceil($day_diff / 7) . ' weeks';
            if(date('n', $ts) == date('n') + 1) return 'next month';
            return date('F Y', $ts);
        }
    }
}

if(!function_exists('check_select'))
{
    function check_select($real_value,$value){
        $result = "";
        if($real_value != null && $value != null){
           if($real_value == $value){
               $result = " selected";
           }
       }
       return $result;
    }
}

if(!function_exists('check_image'))
{
    function check_image($image){
        $image_default = "http://apappun.com/DOKDIG/img/img-default.png";
        if(file_exists($image) || $image != null){
           $image_default = $image;
       }
       return $image_default;
    }
}

if(!function_exists('getBatch'))
{
    function getBatch($page){
        $test = 5;
        $batch =1;
        $flag = false;
        while(!$flag){
          if($page > $test){
              $test = $test + 5;
              $batch++;
          }else{  
              $flag = true;
          }
      }
      return $batch;
    }
}

if(!function_exists('clean'))
{
    function clean($string) {
        $string = str_replace(' ', '-', $string); // Replaces all spaces with hyphens.
        $string = preg_replace('/[^A-Za-z0-9\-]/', '', $string); // Removes special chars.
        $string = strtolower($string); // Convert to lowercase

        return $string;
    }
}

if(!function_exists('compare_search_type'))
{
    function compare_search_type($array, $value){
        $len = count($array);
        $result = null;
        for($m = 0;$m < $len; $m++){
    //echo $array[$m]."-".$value.'<br/>';
           if(strcmp($array[$m],$value) == 0){
               $result = $array[$m];
               break;
           }
       }
       return $result;
   }
}

if(!function_exists('encode'))
{
   function encode($param){
        $new_result = $param;
        if($param != null){
            $result = clean($param);
            $new_result = rawurlencode($result);
        }
        return $new_result;
    }
}

if(!function_exists('decode'))
{
    function decode($param){
        $new_result = $param;
        if($param != null){
           $new_result = str_replace('-',' ',$param);
       }
       return $new_result;
    }
}

if(!function_exists('disease_bracket'))
{
    function disease_bracket($alias){
        $text= '';
        if($alias != null && $alias != ''){
           $text = "(".$alias.")";
       }
       return $text;
    }
}

if(!function_exists('isSelected'))
{
    function isSelected($real_value, $data){
        $text = "";
        if($real_value == $data){
           $text = " selected";
       }
       return $text;
    }
}

//FUNCTION TO CHECK WHETHER TO RETURN '?' OR '&' for the next parameter in query string
if(!function_exists('checkGet'))
{
    function checkGet(){
        $result = '?';
        $q = $_SERVER['QUERY_STRING'];
        if($q != null){
           $result = '&';
       }
       return $result;
    }
}

if(!function_exists('checkUrl'))
{
    function checkUrl($N_url,$param,$value){
        if(strpos($N_url,'?') || strpos($N_url,'&')){
            $url = $N_url."&".$param."=".$value;
        }else{
            $url = $N_url."?".$param."=".$value;
        }
        return $url;
    }
}

if(!function_exists('dropLang'))
{
    function dropLang($newLang){
        if(strpos(curPageName(),'?lang')){
            $urlTest = substr(curPageName(),0,-2);
            $url = $urlTest."$newLang";
                                    // echo "1";
        }
        else if(strpos(curPageName(),'&lang')){
            $urlTest = substr(curPageName(),0,-2);
            $url = $urlTest."$newLang";
                                    // echo "2";
        }
        else if(strpos(curPageName(),'&') || strpos(curPageName(),'?')){
            $url = curPageName()."&lang=".$newLang;
                                    // echo "3";
        }
        else{
            $url = curPageName()."?lang=".$newLang;
                                    // echo "4";
        }
        return $url;
    }
}

if(!function_exists('checkSession'))
{
    function checkSession($newLang=null){
        $timeout = 1800;
        if($newLang == null){
            $newLang = 'EN';
        }

        if($_GET['lang'] != null){
            if($_GET['lang'] != null){
                $newLang = $_GET['lang'];               
            }
            if(isset($_SESSION['lang'])){
                // session_destroy();
                unset($_SESSION['lang']);
            }
            session_start();
            $_SESSION['lang'] = $newLang;
            $_SESSION['time'] = time();
            $lang = $_SESSION['lang'];
        }
        else if($_SESSION['lang'] == null){
            if(isset($_SESSION['lang'])){
                unset($_SESSION['lang']);
            }
            session_start();
            $_SESSION['lang'] = $newLang;
            $_SESSION['time'] = time();
            $lang = $_SESSION['lang'];
        }
        else if($_GET['lang'] == null && $_SESSION['lang'] != null){
            $duration = time() - (int)$_SESSION['time'];
    // echo time()."<br/>";
    // echo (int)$_SESSION['time'];
            if($duration > $timeout){
                unset($_SESSION['lang']);
                $lang = checkSession($newLang);
            }
            $lang = $_SESSION['lang'];
        }

        return $lang;
    }
}

if(!function_exists('curPageName'))
{
    function curPageName() {
        /*     return substr($_SERVER["REQUEST_URI"],strrpos($_SERVER["SCRIPT_NAME"],"/")+1); */
        $url ="http://{$_SERVER['HTTP_HOST']}{$_SERVER['REQUEST_URI']}";
        return $url;
    }
}

if(!function_exists('defaultPic'))
{
    //BILLY - MPJ - function to display default image on categories.php
    function defaultPic($catID,$defaultIMG,$desiredIMG){
        if($catID == null || $catID == ''){
            return $defaultIMG;
        }else{
            return $desiredIMG;
        }
    }
}

if(!function_exists('tab'))
{
    function tab($n) {
        $tabs = null;
        while ($n > 0) {
            $tabs .= "\t";
            --$n;
        }
        return $tabs;
    }
}



if(!function_exists('checkImage1'))
{
    function checkImage1($photoThmb,$brandThmb) {
        if ($photoThmb == NULL) {
            $temp = "$brandThmb";
        } else {
            $temp = "$photoThmb";
        }
        return $temp;
    }
}

if(!function_exists('nowDate'))
{
    function nowDate() {
        $date = date_create("", timezone_open('Asia/Jakarta'));
        $date = date_format($date, 'Y-m-d');
        return $date;
    }
}

if(!function_exists('nowDateComplete'))
{
    function nowDateComplete() {
        $date = date_create("", timezone_open('Asia/Jakarta'));
        $date = date_format($date, 'jS F Y - g:i A');
        return $date;
    }
}

if(!function_exists('check_input'))
{
    //Function To check and secure the login info from hacker attack
    function check_input($data) {
        $data = trim($data);
        $data = stripslashes($data);
        $data = addslashes($data);
        $data = htmlspecialchars($data, ENT_QUOTES);
        return $data;
    }
}

if(!function_exists('getDocType'))
{
    //Function get the extension of the document source
    function getDocType($data) {
        $data = substr($data, -5);
        $pos = strpos($data, ".");

        if ($pos !== false) {
            $data = substr($data, $pos + 1);
            if ($data == "pdf" || $data == "doc" || $data == "docx") {
                return $data;
            } else {
                return "link";
            }
        } else {
            return "link";
        }
    }
}

if(!function_exists('checkLink'))
{
    //Function to check "http://"
    function checkLink($data) {
        $preFix = substr($data, 0, 4);
        if ($preFix != 'http') {
            $data = "http://" . $data;
        }
        return $data;
    }
}

if(!function_exists('checkHref'))
{
    //Function to check "http://"
    function checkHref($data) {
        $result = "";
        
        if ($data != '') {
            $result = $data;
        } else {
            $result = "#";
        }
        return $result;
    }
}

if(!function_exists('charLength'))
{
    //function to put 3dots after lengthy string
    function charLength($data, $length) {
        $strLength = strlen($data);
        if ($strLength > $length) {
            $data = substr(strip_tags($data), 0, $length) . "...";
        }
        return $data;
    }
}

if(!function_exists('correctDisplay'))
{
    function correctDisplay($data) {
        $data = htmlspecialchars_decode(stripslashes($data), ENT_QUOTES);
        return $data;
    }
}

if(!function_exists('checkVideoSource'))
{
    //function
    function checkVideoSource($data) {
        $findme = "youtube.com/watch?v=";
        $findme2 = "youtu.be/";
        $pos = strpos($data, $findme);
        $pos2 = strpos($data, $findme2);
        $dataLength = strlen($data);

        if ($pos !== false) {
            $findAmp = stripos($data, '&');

            if ($findAmp === false) {
                $data = str_replace("youtube.com/watch?v=", "youtube.com/embed/", $data);
                return $data;
            } else {
                $lengthTrimmed = $dataLength - ($findAmp);
                $data = substr($data, 0, -$lengthTrimmed);
                $data = str_replace("youtube.com/watch?v=", "youtube.com/embed/", $data);
                return $data;
            }
        } else if ($pos2 !== false) {
            $data = str_replace("youtu.be/", "youtube.com/embed/", $data);
            return $data;
        } else {
            return $data;
        }
    }
}

if(!function_exists('encodeURLslash'))
{
    function encodeURLslash($data) {
        $data = str_replace("/", "&slash", $data);
        return $data;
    }
}

if(!function_exists('decodeURLslash'))
{
    function decodeURLslash($data) {
        $data = str_replace("&slash", "/", $data);
        return $data;
    }
}

if(!function_exists('encodeURLspace'))
{
    function encodeURLspace($data) {
        $data = str_replace(" ", "%20", $data);
        return $data;
    }
}

if(!function_exists('curPageURL'))
{
    function curPageURL() {
        $pageURL = 'http';
        if ($_SERVER["HTTPS"] == "on") {
            $pageURL .= "s";
        }
        $pageURL .= "://";
        if ($_SERVER["SERVER_PORT"] != "80") {
            $pageURL .= $_SERVER["SERVER_NAME"] . ":" . $_SERVER["SERVER_PORT"] . $_SERVER["REQUEST_URI"];
        } else {
            $pageURL .= $_SERVER["SERVER_NAME"] . $_SERVER["REQUEST_URI"];
        }
        return $pageURL;
    }
}

if(!function_exists('getCurrentRating'))
{
    function getCurrentRating($shopID) {
        require("Shop-dbConn.php");
        $shopIDIn = mysql_real_escape_string(check_input($shopID));
        $sql = "SELECT AVG(Review_rating) as avgRating FROM T_Review WHERE Review_shopID= '$shopIDIn'";
        $result = @mysql_query($sql);
        $rs = @mysql_fetch_array($result);
        return @round($rs[avgRating], 1);
        mysql_close($dbh);
    }
}

if(!function_exists('countRating'))
{
    function countRating($shopID) {
        require("Shop-dbConn.php");
        $shopIDIn = mysql_real_escape_string(check_input($shopID));
        $sql = "SELECT COUNT(Review_rating) as countRating FROM T_Review WHERE Review_shopID= '$shopIDIn'";
        $result = @mysql_query($sql);
        $rs = @mysql_fetch_array($result);
        return $rs[countRating];
        mysql_close($dbh);
    }
}

if(!function_exists('getCurrentProduct'))
{
    function getCurrentProduct($shopID) {
        require("Shop-dbConn.php");
        $shopIDIn = mysql_real_escape_string(check_input($shopID));
        $sql = "SELECT Product_ID, Product_name FROM T_Product WHERE Product_userID= '$shopIDIn'  ORDER BY Product_name ASC";
        $result = @mysql_query($sql);
        $productCounter = 0;
        while ($row = mysql_fetch_array($result, MYSQL_ASSOC)) {
            $O_productID[$productCounter] = correctDisplay($row['Product_ID']);
            $O_productName[$productCounter] = correctDisplay($row['Product_name']);
            echo "<option value='$O_productID[$productCounter]'>$O_productName[$productCounter]</option>";
            $productCounter++;
        }
    //return $productCounter;
        mysql_close($dbh);
    }
}

if(!function_exists('getRatingColor'))
{
    function getRatingColor($rating) {
        if ($rating == null || $rating == '' || $rating == 0) {
            $rating = '?';
            $color = "#3366cc";
        } else if ($rating < 3) {
            $color = "#CA0606";
        } else if ($rating >= 4) {
            $color = "#64AA2B";
        } else {
            $color = "#999";
        }
        $result = "<font color='$color'>$rating</font>";
        return $result;
    }
}

if(!function_exists('getRatingTitle'))
{
    function getRatingTitle($rating) {
        if ($rating == '?' || $rating == null || $rating == '' || $rating == 0) {
            $title = "Be the first person to review...";
        } else if ($rating < 3) {
            $title = "Need Improvements";
        } else if ($rating >= 4) {
            $title = "Recommended Seller";
        } else {
            $title = "So Far So Good";
        }
        return $title;
    }
}

if(!function_exists('checkPlural'))
{
    function checkPlural($data) {
        if ($data > 1) {
            $plural = 's';
        }
        return $plural;
    }
}

if(!function_exists('doHash'))
{
    function doHash($secData, $salt) {
    //creates a random 5 character sequence
        $secData = hash('sha256', $salt . $secData);
        return $secData;
    }
}
?>