<?php
//function recursive for category shop in part-dropdown_shop.php
function category_shop($parent=0, $path){
    require_once("model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_parentID, category_name, category_gen FROM T_CATEGORY WHERE 
        category_publish = 'Publish' AND category_parentID = '$parent' ORDER BY category_sort ASC";
    $sql = mysql_query($query);
    $count = mysql_num_rows($sql);

    $num=0;
    $count=1;
    while($row = mysql_fetch_assoc($sql)) {
        $url = $path."?page=1&id=".$row['category_ID'];
    
        if($row['category_gen'] == 2){
            if($num == 0){
                echo "<div class='col-shop'>";
            }
            echo "<div class='shop-wrap'>";
            echo "<div class='shop-head'>";
            echo "<a href='".$url."'>".$row['category_name']."</a>";
            echo "</div>";
            echo "<ul class='shop-child'>";
        }

        if($row['category_gen'] != 2){
            echo "<li>";
            echo "<a href='".$url."'>".$row['category_name']."</a>";
        }
        
        category_shop($row['category_ID'], $path); //panggil diri sendiri
        
        if($row['category_gen'] != 2){
             echo "</li>";
        }

        if($row['category_gen'] == 2){
            echo "</ul>";
            echo "</div>";
            $num++;
            if($num == 2){
                if($count < 2){
                    $num=0;
                    $count++;
                    echo "</div>";    
                }
            }
        }
    }

    if($count > 1){
        echo "</div>";
    }

    $obj_con->down();
}

//function recursive for category sidebar by child in sub-category.php
function category_sidebar_by_child($child, $category_id, $path){
    require_once("model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_parentID, category_name, category_gen FROM T_CATEGORY 
        WHERE category_publish = 'Publish' AND category_parentID = '$child' ORDER BY category_sort ASC";
    $sql = mysql_query($query);
    $count = mysql_num_rows($sql);

    while($row = mysql_fetch_assoc($sql)) {
        echo "<li>";
        $active = $category_id == $row['category_ID'] ? "active" : "";
        $url = $path."?page=1&id=".$row['category_ID'];
        echo "<a href='".$url."' class='".$active."'>".$row['category_name']."</a>";

        category_sidebar_by_child($row['category_ID'], $category_id, $path); //panggil diri sendiri
        echo "</li>";
    }

    $obj_con->down();
}

//function recursive for sale sidebar by child in sale.php
function sale_sidebar_by_child($child, $category_id, $path, $sort, $view, $brand, $min, $max){
    require_once("model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_parentID, category_name, category_gen FROM T_CATEGORY 
        WHERE category_publish = 'Publish' AND category_parentID = '$child' ORDER BY category_sort ASC";
    $sql = mysql_query($query);
    $count = mysql_num_rows($sql);

    while($row = mysql_fetch_assoc($sql)) {
        echo "<li>";
        $head = $row['category_gen'] == 2 ? "head" : "";
        $active = $category_id == $row['category_ID'] ? "active" : "";
        $url = $path."?page=1&id=".$row['category_ID']."&sort=".$sort."&view=".$view."&brand=".$brand."&min=".$min."&max=".$max;
        echo "<a href='".$url."' class='".$active." ".$head."'>".$row['category_name']."</a>";

        sale_sidebar_by_child($row['category_ID'], $category_id, $path, $sort, $view, $brand, $min, $max); //panggil diri sendiri
        echo "</li>";
    }

    $obj_con->down();
}

//function recursive for brand sidebar by child in search-brand.php
function brand_sidebar_by_child($child, $category_id, $path, $sort, $view, $brand, $min, $max){
    require_once("model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_parentID, category_name, category_gen FROM T_CATEGORY 
        WHERE category_publish = 'Publish' AND category_parentID = '$child' ORDER BY category_sort ASC";
    $sql = mysql_query($query);
    $count = mysql_num_rows($sql);

    while($row = mysql_fetch_assoc($sql)) {
        echo "<li>";
        $head = $row['category_gen'] == 2 ? "head" : "";
        $active = $category_id == $row['category_ID'] ? "active" : "";
        $url = $path."?page=1&id=".$row['category_ID']."&sort=".$sort."&view=".$view."&brand=".$brand."&min=".$min."&max=".$max;
        echo "<a href='".$url."' class='".$active." ".$head."'>".$row['category_name']."</a>";

        brand_sidebar_by_child($row['category_ID'], $category_id, $path, $sort, $view, $brand, $min, $max); //panggil diri sendiri
        echo "</li>";
    }

    $obj_con->down();
}

//function recursive for category sidebar by parent in part-category_nav.php
function category_sidebar_by_parent($parent, $path){
    require_once("model/Connection.php");
    $obj_con = new Connection();

    $obj_con->up();
    $query = "SELECT category_ID, category_parentID, category_name, category_gen FROM T_CATEGORY 
        WHERE category_publish = 'Publish' AND category_parentID = '$parent' ORDER BY category_sort ASC";
    $sql = mysql_query($query);
    $count = mysql_num_rows($sql);

    while($row = mysql_fetch_assoc($sql)) {
        echo "<li>";
        $head = $row['category_gen'] == 2 ? "head" : "";
        $url = $path."?page=1&id=".$row['category_ID'];
        echo "<a href='".$url."' class='".$head."'>".$row['category_name']."</a>";

        category_sidebar_by_parent($row['category_ID'], $path); //panggil diri sendiri
        echo "</li>";
    }

    $obj_con->down();
}

//function recursive get id for feature product in controller_category.php, controller_sub_category.php
function get_id_by_parent(&$strid="", $parent) {
    $query = "SELECT category_ID, category_parentID FROM T_CATEGORY WHERE category_parentID = '$parent'";
    $sql = mysql_query($query);
 
    while ($row = mysql_fetch_assoc($sql)) {
        $strid .= ",".$row['category_ID'];
        get_id_by_parent($strid, $row['category_ID']); //panggil diri sendiri
    }
}
?>