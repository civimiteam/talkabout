<!-- jQuery Include -->
<script src="<?=$global['absolute-url'];?>libraries/jquery.min.js"></script>	
<script src="<?=$global['absolute-url'];?>libraries/jquery.easing.min.js"></script><!-- Easing Animation Effect -->
<script src="<?=$global['absolute-url'];?>libraries/bootstrap/bootstrap.min.js"></script> <!-- Core Bootstrap v3.3.4 -->
<script src='https://maps.google.com/maps/api/js?sensor=false' type="text/javascript"></script>
<script src="<?=$global['absolute-url'];?>libraries/gmap/jquery.gmap.min.js"></script> <!-- Light Box -->
<script src="<?=$global['absolute-url'];?>libraries/fuelux/jquery-ui.min.js"></script> <!-- Price Filter -->
<script src="<?=$global['absolute-url'];?>libraries/jquery.animateNumber.min.js"></script> <!-- Used for Animated Numbers -->
<script src="<?=$global['absolute-url'];?>libraries/jquery.appear.js"></script> <!-- It Loads jQuery when element is appears -->
<script src="<?=$global['absolute-url'];?>libraries/jquery.knob.js"></script> <!-- Used for Loading Circle -->
<script src="<?=$global['absolute-url'];?>libraries/wow.min.js"></script> <!-- Use For Animation -->
<script src="<?=$global['absolute-url'];?>libraries/owl-carousel/owl.carousel.min.js"></script> <!-- Core Owl Carousel CSS File  *	v1.3.3 -->
<script src="<?=$global['absolute-url'];?>libraries/portfolio-filter/jquery.quicksand.js"></script> <!-- Portfolio Filter -->
<script src="<?=$global['absolute-url'];?>libraries/expanding-search/modernizr.custom.js"></script> <!-- Core Owl Carousel CSS File  *	v1.3.3 -->
<script src="<?=$global['absolute-url'];?>libraries/flexslider/jquery.flexslider-min.js"></script> <!-- flexslider -->
<script src="<?=$global['absolute-url'];?>libraries/jquery.magnific-popup.min.js"></script> <!-- Light Box -->
<script src="<?=$global['absolute-url'];?>libraries/expanding-search/modernizr.custom.js"></script> <!-- Core Owl Carousel CSS File  *	v1.3.3 -->
<script src="<?=$global['absolute-url'];?>libraries/expanding-search/classie.js"></script> 
<!-- Customized Scripts -->
<script src="<?=$global['absolute-url'];?>js/functions.js"></script>
<script type="text/javascript">
	$('.dropdown-cat').click(function(){ 
	    var target = $(this).data('id'); 
	    // console.log(target);
	    if($("#collapse-"+target).hasClass("in")){
	    	$("#caret-"+target).removeClass("cat-caret-up");
		} else {
			$("#caret-"+target).addClass("cat-caret-up");
		}
	});
	<?php if(isset($curpage)) { if($curpage == 'product'){ ?>
	$(document).ready(function() {
    	var rootID = $("#root-id").val();
    	$("#caret-"+rootID).addClass("cat-caret-up");
    	$("#collapse-"+rootID).addClass("in");
	});
	<?php } } ?>
</script>