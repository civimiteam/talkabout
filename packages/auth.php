<?php 
//check session
if(isset($_SESSION['userData']['id'])){
	//print_r($_SESSION['userData']);
	//logging out
	if(isset($_GET['action'])){
		ob_start();
		$http = "https://www.eannovate.com/dev/brown/";
		if($_GET['action']== "logout"){
			end_session(); //set end data session
			$_SESSION = array(); //set session data to an empty array
			session_destroy(); //destroy the session variables

			//double check to see if thier sessions exists
			if(isset($_SESSION['userData']['id'])){
				header("Location:".$http."errorLogout/");
			}else{
				echo "<script>window.history.go(-1);</script>";
				exit();
			}
		}else{
			header("Location:".$http."error/");
		}
		ob_end_flush();
	}
}

?>