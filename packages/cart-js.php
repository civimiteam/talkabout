<script type="text/javascript">
	function change_url(val) {
        window.location=val;
    }
    function convertToRupiah(angka)
    {   
        var rupiah = '';        
        // var money = angka.toFixed(0)
        var angkarev = angka.toString().split('').reverse().join('');
        for(var i = 0; i < angkarev.length; i++) if(i%3 == 0) rupiah += angkarev.substr(i,3)+',';
        return 'IDR '+rupiah.split('',rupiah.length-1).reverse().join('');
    }
	function update_cart(temp) {
        // The id of the item to update
        var cart_id = temp;
        var cart_qty = $("#tcart-qty-"+temp).val();
        var cart_price = $("#tcart-price-"+temp).val();
        var cart_token = $("#tcart-token").val();
        var cart_checkout = $("#tcart-checkout").val();
        var new_price = eval(cart_price*cart_qty);

        // As long as the visitor has entered a quantity
        if (cart_qty) {
            // Update the item and refresh cart display
            $.ajax({
            	data: {
                    "jcartUpdate": 1, // Only the name in this pair is used in jcart.php, but IE chokes on empty values
                    "itemId": cart_id,
                    "itemQty": cart_qty,
                    "jcartIsCheckout": cart_checkout,
                    "jcartToken": cart_token
                }
            });
            format_price = convertToRupiah(new_price);
            $("#price-line-"+temp).text(format_price);
            $("#alcart-text").text('Item Has Been Updated!');
            $("#alert-cart").show();
            $("#alert-cart").delay(3000).fadeOut();

        }
    }
    function remove_cart(removeId) {
    	var cart_checkout = $("#tcart-checkout").val();
		// Remove the item and refresh cart display
		$.ajax({
			type: 'GET',
			data: {
				"jcartRemove": removeId,
				"jcartIsCheckout": cart_checkout
			}
		});
		$("#alcart-text").text('Item Has Been Deleted!');
		$("#alert-cart").show();
		$("#alert-cart").delay(3000).fadeOut();
		var total_line = $(".cart-line").length;
		$("#cart-line-"+removeId).remove();
		if(total_line == 1){
			$("#table-cart").html("<tr><th>Image</th><th>Product Name</th><th style='display:none;'>Product Color</th><th>Quantity</th><th>Unit Price</th><th>Total</th></tr><tr><td colspan='5' class='cart-empty'>Your cart is empty!</td></tr>");
			$(".btn-checkout").hide();
		}
	}
</script>