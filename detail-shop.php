<?php 
//include("packages/require.php");
//include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/jquery.elevatezoom.js"></script>
</head>
<body>
	<div class="device-max">
		<!-- START SECTION HEADER -->
		<?php include("parts/part-header.php");?>
		<!-- END SECTION HEADER -->
		<!-- START SECTION BODY -->	
		<div class="section-body up2">
			<div class="container">
				<div class="row">
					<div class="col-xs-12">
						<div class="title center border-top border-bottom font16px padding-top-5 padding-bottom-5 MontserratRegular">
							HOME / Matte Liquid / MIMOSA
						</div>
					</div>
				</div>
			</div>
			<div class="container margin-bottom-xs-0">
				<div class="row up4">
					<div class="col-sm-6 col-xs-12 padding-right-50 padding-xs-0">
						<div class="product-big"> 
							<img id="elevate" src="img/source/productPage_mimosa-crop-2-crop.png" data-zoom-image="img/source/productPage_mimosa-crop-2-crop.png" class="image-primary">
							<div class="photo-title"></div>
						</div>
						<div id="gal1" class="thumb-list">
							<a href="javascript:void(0);" class="product-thumb active" data-title="" data-image="img/source/productPage_mimosa-crop-2-crop.png" data-zoom-image="img/source/productPage_mimosa-crop-2-crop.png">
								<img id="img_598" src="img/source/productPage_mimosa-crop-2-crop.png">
							</a>
							<a href="javascript:void(0);" class="product-thumb" data-title="" data-image="img/source/S791030001.2-crop.jpg" data-zoom-image="img/source/S791030001.2-crop.jpg">
								<img id="img_599" src="img/source/S791030001.2-crop.jpg">
							</a>
							<a href="javascript:void(0);" class="product-thumb" data-title="" data-image="img/source/test.jpg" data-zoom-image="img/source/test.jpg">
								<img id="img_600" src="img/source/test.jpg">
							</a>
						</div>
					</div>
					<div class="col-sm-6 col-xs-12 left center-xs padding-left-50 padding-xs-0">
						<div class="product-name margin-top-xs-0">
							<h1 class="font16">MIMOSA</h1>
						</div>
						<div class="product-price text-color-grey-2 left center-xs">
							<h3>IDR 160.000</h3>
						</div>
						<div class="product-description-1">
							<h6>Nude soft tone greyed-out beige</h6>
						</div>
						<div class="product-description-long">
							<h6>This neutral flush can be combined with other shades as your base to create ombre colour or toning down your other shades on both lips and cheeks. Use it alone to achieve that edgy look of the day!</h6>
						</div>
						<div class="row">
							<div class="col-sm-2 col-xs-5 pad0">
								<h5 class="padding-top-8 text-color-grey-2 MontserratLight">Quantity</h5>
							</div>
							<div class="col-sm-3 col-xs-6 pad0">
								<div class="input-group">
							      	<span class="input-group-btn">
							        	<button class="btn btn-default" type="button" id="btn-plus"><span class="glyphicon glyphicon-minus"></span></button>
							      	</span>
							      	<input type="number" class="form-control center" name="qty" id="qty" value="1">
						      		<span class="input-group-btn">
						        		<button class="btn btn-default" type="button" id="btn-minus"><span class="glyphicon glyphicon-plus"></span></button>
						      		</span>
							    </div>
						    </div>
					    </div>
					    <div class="col-xs-12 pad0 margin-top-xs-20 up2">
							<div class="col-sm-3 col-xs-4 pad0 hidden-xs">
								<h4 class="buy-now center">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
							</div>
							<div class="col-xs-4 hidden-sm">
								
							</div>
							<div class="col-xs-4 pad0 visible-xs hidden-sm">
								<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
							</div>
						</div>
						<div class="row border-grey">
							<ul class="icon-share margin-top-65 pad0 margin-bottom-20">
								<li><img src="img/twitter.png" class="icon-share-image"></li>
								<li><img src="img/instagram.png" class="icon-share-image"></li>
								<li><img src="img/facebook.png" class="icon-share-image"></li>
							</ul>
						</div>
						<p class="attention padding-xs-0 center-xs">The colors shown maybe different with the original products because of the settings
						of your display screen. We can not guarantee it can be accurate</p>
					</div>
				</div>
				<div class="row margin-top-50 border-black">
					<div class="col-xs-12">
						<h4 class="margin-bottom-10 MontserratBold center-xs black font-22px">YOU MIGHT ALSO LIKE</h4>
					</div>
				</div>
				<div class="row margin-bottom-40">
					<div class="col-sm-3 col-xs-6">
						<div class="products center">
							<a href="#">
								<img src="img/source/productPage_sandsome.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
							</a>
							<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>SANDSOME</strong></h3>
							<h6 class="product-description-2 MontserratLight">Represent creativity for easy going personality</h6>
							<h6 class="padding-top-1" style="height: 14px"></h6>
							<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 160.000</h4>
							<div class="col-xs-12 pad0">
								<div class="col-sm-3 col-xs-0">
								</div>
								<div class="col-sm-6 col-xs-12 pad0">
									<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-xs-6">
						<div class="products center">
							<a href="#">
								<img src="img/source/productPage_marsala.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
							</a>
							<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>MARSALA</strong></h3>
							<h6 class="product-description-2 MontserratLight">For those who is mysteriously attractive, the chaming one</h6>
							<h6 class="padding-top-1" style="height: 14px"></h6>
							<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 160.000</h4>
							<div class="col-xs-12 pad0">
								<div class="col-sm-3 col-xs-0">
								</div>
								<div class="col-sm-6 col-xs-12 pad0">
									<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-xs-6">
						<div class="products center">
							<a href="#">
								<img src="img/source/productPage_agate.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
							</a>
							<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>AGATE</strong></h3>
							<h6 class="product-description-2 margin-bottom-0 MontserratLight">Lucky charm for the intellect, courageous, and tough one</h6>
							<h6 class="padding-top-1 MontserratBold font-color-grey" style="height: 14px; text-decoration: line-through;">IDR 160.000</h6>
							<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 149.999</h4>
							<div class="col-xs-12 pad0">
								<div class="col-sm-3 col-xs-0">
								</div>
								<div class="col-sm-6 col-xs-12 pad0">
									<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-3 col-xs-6">
						<div class="products center">
							<a href="detail-shop.php">
								<img src="img/source/productPage_mimosa.png" style="width: 100%" class="margin-bottom-min50 margin-bottom-xs-min10">
							</a>
							<h3 class="font20px margin-bottom-15 ACaslonProBold"><strong>MIMOSA</strong></h3>
							<h5 class="product-description-2 MontserratLight">She’s generous, kind, sensitive, lady like but a tough one</h5>
							<h6 class="padding-top-1 MontserratBold font-color-grey" style="height: 14px; text-decoration: line-through;">IDR 160.000</h6>
							<h4 class="margin-bottom-15 MontserratBold font-color-grey">IDR 149.999</h4>
							<div class="col-xs-12 pad0">
								<div class="col-sm-3 col-xs-0">
								</div>
								<div class="col-sm-6 col-xs-12 pad0">
									<h4 class="buy-now">BUY NOW <img src="img/shopping-cart.png" class="cart-image"></h4>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END SECTION BODY -->
		<!-- START SECTION FOOTER -->
		<?php include("parts/part-footer.php");?>
		<!-- END SECTION FOOTER -->
	</div> 
    <script src="<?php echo $global['absolute-url'];?>js/global.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/home.js"></script>
    <script type="text/javascript">
		$('.product-thumb').click(function(){
		    var title = $(this).data("title");
		    $(".photo-title").text(title);
		});

		// $("#elevate").elevateZoom({
		// 	gallery:'gal1', 
		// 	cursor: 'pointer', 
		// 	galleryActiveClass: 'active', 
		// 	imageCrossfade: true, 
		// 	loadingIcon: 'http://www.elevateweb.co.uk/spinner.gif'
		// }); 
		$("#elevate").elevateZoom({
			gallery:'gal1', 
			cursor: 'pointer', 
			galleryActiveClass: 'active',
			lensSize: 150,
			zoomLevel: 0.4,
			zoomType: 'window',
			lensBorder: 1,
			borderSize: 1,
			scrollZoom : true
		});
		$('#contact-cart').click(function(event){
		    $('html, body').animate({
		        scrollTop: $( $.attr(this, 'href') ).offset().top
		    }, 500);
		    event.preventDefault();
		});
	</script>
	<!--<div class="zoomContainer" style="transform: translateZ(0px); position: absolute; left: 104.5px; top: 288px; height: 463px; width: 555px;">
		<div class="zoomLens" style="background-position: 0px 0px; float: right; overflow: hidden; z-index: 999; transform:translateZ(0px); opacity: 0.4; zoom: 1; width: 161.455px; height: 161.394px; background-color: white; cursor: pointer; border: 1px solid rgb(0, 0, 0); background-repeat: no-repeat; position: absolute; left: 394px; top: 136px; display: none;">&nbsp;</div>
		<div class="zoomWindowContainer" style="width: 400px;">
			<div style="overflow: hidden; background-position: -977px -336.335px; text-align: center; background-color: rgb(255, 255, 255); width: 400px; height: 400px; float: left; background-size: 1375px 1147.5px; z-index: 100; border: 1px solid rgb(136, 136, 136); background-repeat: no-repeat; position: absolute; background-image: url(&quot;img/source/productPage_mimosa-crop-2-crop.png&quot;); top: 0px; left: 555px; display: none;" class="zoomWindow">&nbsp;</div>
		</div>
	</div>-->
</body>	
</html>