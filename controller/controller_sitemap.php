<?php
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Product.php");
$obj_product = new Product();

if(!isset($_GET['action'])){
    $obj_connect->up();
    //product best seller
    $bests = $obj_product->get_home_best();
    $item_bests = array();
    foreach($bests as $best){
        $item_bests[] = array(
            'title' => $best['product_name'],
            'url' => $path['product'].encode($best['product_name'])."_".$best['product_ID'].".html"
        );
    }

    //product by category
    $categorys = $obj_product->get_category_product();
    $item_categorys = array();
    foreach($categorys as $category){
        $item_products = array();
        foreach($category['product'] as $product){
            $item_products[] = array(
                'title' => $product['product_name'],
                'url' => $path['product'].encode($product['product_name'])."_".$product['product_ID'].".html"
            );
        }
        $item_categorys[] = array(
            'title' => $category['category_name'],
            'url' => $path['product'].$category['category_slug']."/1/",
            'datas' => $item_products
        );
    }

    $sitemaps = array(
        0 => array(
            'title' => "Help",
            'url' => "#",
            'content' => array(
                0 => array(
                    'title' => "FAQs",
                    'url' => $path['faq']
                ),
                1 => array(
                    'title' => "How to Order",
                    'url' => $path['how']
                ),
                2 => array(
                    'title' => "Shipping",
                    'url' => $path['shipping']
                ),
                3 => array(
                    'title' => "Returns",
                    'url' => $path['return']
                ),
            )
        ),
        1 => array(
            'title' => "New Arrivals",
            'url' => $path['new-arrival']
        ),
        2 => array(
            'title' => "Cart",
            'url' => $path['cart']
        ),
        3 => array(
            'title' => "Fun",
            'url' => "#",
            'content' => array(
                0 => array(
                    'title' => "About Us",
                    'url' => $path['about']
                ),
                1 => array(
                    'title' => "Our Instagram",
                    'url' => "#"
                ),
                2 => array(
                    'title' => "#hairpotion",
                    'url' => "#"
                )
            )
        ),
        4 => array(
            'title' => "Product",
            'url' => "#",
            'content' => $item_categorys
        ),
        5 => array(
            'title' => "Best Seller",
            'url' => "#",
            'content' => $item_bests
        ),
    );

    //var_dump($sitemaps);
    $obj_connect->down();
}
?>