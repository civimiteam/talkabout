<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Ongkir.php");
$obj_ongkir = new Ongkir();

if(isset($_SESSION['customer_id'])){
	if(!isset($_GET['action'])){
		if(isset($_GET['q']) && $_GET['q'] != ""){
			$obj_connect->up();
		
			$O_resi = mysql_real_escape_string($_REQUEST['q']);
			$data_resi = $obj_ongkir->trackingResi($O_resi);
			if($data_resi != "error"){
				$datas = json_decode($data_resi, true);
				$data_stat = $datas['rajaongkir']['status'];
				if($data_stat['code'] != "400"){
					$data_summary = $datas['rajaongkir']['result']['summary'];
					$data_detail = $datas['rajaongkir']['result']['details'];
					$data_delivery = $datas['rajaongkir']['result']['delivery_status'];
					$data_manifest = $datas['rajaongkir']['result']['manifest'];
				}
			}
			$obj_connect->down();
		} else {
			header("Location:{$path['home']}");
		}
	}
} else {
    $_SESSION['loginStatus'] = "failed";
    $_SESSION['loginMessage'] = "You Must Login First!";
    header("Location:{$path['login']}");
}
?>