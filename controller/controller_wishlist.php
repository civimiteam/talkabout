<?php 
require_once("model/Connection.php");
$obj_con = new Connection();
require_once("model/AT_Wishlist.php");
$obj_wishlist = new AT_Wishlist();

if(isset($_SESSION['customer_id'])){
	if(!isset($_GET['action'])){
		$obj_con->up();

	    $data_wishlists = $obj_wishlist->get_customer_data_list($_SESSION['customer_id']);
	    if(isset($_SESSION['wishStatus'])){
	        $wishStatus = $_SESSION['wishStatus'];
	        unset($_SESSION['wishStatus']);
	    } else {
	        $wishStatus = "";
	    }
	    if(isset($_SESSION['wishMessage'])){
	        $wishMessage = $_SESSION['wishMessage'];
	        unset($_SESSION['wishMessage']);
	    } else {
	        $wishMessage = "";
	    }
	    
	    $obj_con->down();

	} else { 
		if($_GET['action'] == 'remove'){
			$obj_con -> up(); 

			$O_awID = mysql_real_escape_string($_GET['id']);
			$result = $obj_wishlist->delete_data($O_awID);
			if($result == 1){
				$message = "Success removing product wishlist";
	            $status = "success";    	    
			} else {
				$message = "Failed removing product wishlist!";
	            $status = "failed";    	    
			}

			$_SESSION['wishStatus'] = $status;
	        $_SESSION['wishMessage'] = $message;
			header("Location:{$path['wishlist']}");
			$obj_con -> down();
		}
	}

} else {

    $_SESSION['loginStatus'] = "failed";
    $_SESSION['loginMessage'] = "You Must Login First!";
    header("Location:{$path['login']}");
    
}