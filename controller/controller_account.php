<?php
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Customer.php");
$obj_customer = new Customer();

if(isset($_SESSION['customer_id'])){

	if(!isset($_GET['action'])){
		$obj_connect->up();
		
	    $result_cust = $obj_customer->get_data_edit($_SESSION['customer_id']);
	    //var_dump($result_cust);
	    
	    if(isset($_SESSION['accountStatus'])){
	        $accountStatus = $_SESSION['accountStatus'];
	        unset($_SESSION['accountStatus']);
	    } else {
	        $accountStatus = "";
	    }

	    if(isset($_SESSION['accountMessage'])){
	        $accountMessage = $_SESSION['accountMessage'];
	        unset($_SESSION['accountMessage']);
	    } else {
	        $accountMessage = "";
	    }

		$obj_connect->down();

	} else if(isset($_GET['action'])){

		if($_GET['action'] == 'update'){
			$obj_connect->up();
			$O_custID = $_POST['id'];
			$O_name = $_POST['name'];
			$O_phone = $_POST['phone']; 

			$result = $obj_customer->update_data_customer($O_custID, $O_name, $O_phone);
			if($result == 1){
				$_SESSION['customer_name'] = $O_name;
				$message = "Update Account Success!";
	            $status = "success";
			} else {
				$message = "Update Account failed!";
	            $status = "failed";
			}

			$_SESSION['accountStatus'] = $status;
	        $_SESSION['accountMessage'] = $message;
	        header("Location:".$path['account']);
			$obj_connect->down();
		}
	}
} else {
    $_SESSION['loginStatus'] = "failed";
    $_SESSION['loginMessage'] = "You Must Login First!";
    header("Location:".$path['login']);
}
?>