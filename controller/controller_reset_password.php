<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Customer.php") ;
$obj_cust = new Customer() ; 

if(!isset($_GET['action'])){
    if($_GET['fname'] != '' && $_GET['code'] != ''){

        $obj_connect->up();
        $message = "";

        $O_fname = mysql_real_escape_string(check_input($_GET['fname']));
        $O_code = mysql_real_escape_string(check_input($_GET['code']));

        $result = $obj_cust->check_reset_password($O_fname, $O_code);
        if($result != 1){
            echo "<script>alert('Incorrect. Code is expired')</script>";
            echo "<meta http-equiv='refresh' content='0;url={$path['login']}'>";
        }


        if(isset($_SESSION['loginStatus'])){
            $loginStatus = $_SESSION['loginStatus'];
            unset($_SESSION['loginStatus']);
        } else {
            $loginStatus = "";
        }
        if(isset($_SESSION['loginMessage'])){
            $loginMessage = $_SESSION['loginMessage'];
            unset($_SESSION['loginMessage']);
        } else {
            $loginMessage = "";
        }

        $obj_connect->down();
    }
} else {
    
    if($_GET['action'] == 'reset'){
        $obj_connect->up();

        $N_fname = mysql_real_escape_string(check_input($_POST['fname']));
        $N_code = mysql_real_escape_string(check_input($_POST['code']));
        $N_password = mysql_real_escape_string(check_input($_POST['password']));
        $N_re_password = mysql_real_escape_string(check_input($_POST['repassword']));

        if($N_password == $N_re_password){
            $result = $obj_cust->reset_password($N_fname, $N_password, $N_code);
            if($result == 1){              
                $message = "Password successfully updated! Please login using the new password!";
                $status = "success";
                $location = $path['login']; 
            }else{
                $message = "Password failed to be updated. Something is wrong!";
                $status = "failed";
                $location = $path['reset-password']."/".$N_fname."/".$N_code;
            }
        }else{
            $message = "Password does not match!";
            $status = "failed";
            $location = $path['reset-password']."/".$N_fname."/".$N_code;
        }   

        $_SESSION['loginStatus'] = $status;
        $_SESSION['loginMessage'] = $message;
        header("Location:".$location);
        $obj_connect->down();
    }
}
?> 