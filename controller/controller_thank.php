<?php
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/PO.php");
$obj_po = new PO();

require_once("model/Mail.php");
$obj_mail = new Mail();

require_once("model/Midtrans.php");
$obj_midtrans = new Midtrans();

if(isset($_GET['order_id']) && $_GET['order_id'] != ""){    
    $obj_connect->up();
    $sent = false;
    $po_name = mysql_real_escape_string(check_input($_GET['order_id'])); //po name
    if(isset($_SESSION['checkoutStatus'])){
        unset($_SESSION['checkoutStatus']);
    }
    if(isset($_SESSION['checkoutMessage'])){
        unset($_SESSION['checkoutMessage']);
    }
    
    //check transaction status using midtrans
    if(isset($_GET['status_code']) && isset($_GET['transaction_status'])){
        require_once("veritrans/Veritrans.php");
        if(isset($_SESSION['snapToken'])){
            unset($_SESSION['snapToken']);
        }
        $status_code = $_GET['status_code'];
        $transaction = $_GET['transaction_status'];
        // Uncomment for production environment
        //Veritrans_Config::$isProduction = true;
        $result_midtrans = $obj_midtrans->getTransactionStatus(Veritrans_Config::getBaseUrl(), $po_name, $veritrans_server_key_base64);
        if(is_array($result_midtrans)){
            if($po_name == $result_midtrans['order_id']){
                if($status_code == $result_midtrans['status_code']){
                    if($result_midtrans['status_code'] == "200"){
                        if($transaction == $result_midtrans['transaction_status']){
                            if($result_midtrans['transaction_status'] == "capture"){
                                if($result_midtrans['fraud_status'] == "accept"){
                                    $sent = true;
                                    $obj_po->update_transaction_status($result_midtrans['order_id'], 3, 1);
                                    //echo "Transaction order_id: " . $result_midtrans['order_id'] ." successfully captured.";
                                }
                            }
                        }
                    }
                }
            }
        }
    }else{
        $sent = true;
    }

    if($sent){
        $datas = $obj_po->get_by_po($po_name); //GET DATA PO
        if(!is_array($datas)){
            header("Location:{$path['404']}");  
        } else {
            $jcart->empty_cart();
            $po_id = $datas[0]['po_ID'];
            $data_products = $obj_po->get_product_by_idpo($po_id); //GET DATA PRODUCT
            $data_details = $obj_po->get_order_detail($po_id);
            $file_json = "uploads/json/company_profile.json";
            $json = json_decode(file_get_contents($file_json),TRUE);
            $J_Company_email = $json['email1'];
            $O_toCompany = $J_Company_email;
            $J_Company_bank = $json['bank'];
            $J_bank = str_replace('\r\n','<br/>',$J_Company_bank);
        }

        $loop = "";
        foreach ($data_products as $data_product ) {
            $unit_price = $data_product['atpp_price'] / $data_product['atpp_qty'];
            $loop .= "
            <tr>
            <td class='text-left'>
            <a href='".$path['product-detail'].encode($data_product['product_name'])."_".$data_product['product_ID'].".html'>".$data_product['product_name']."</a>
            </td>
            <td class='text-right'>".$data_product['atpp_qty']."</td>
            <td class='text-right'>Rp.".number_format($unit_price,0,'.',',')."</td>
            <td class='text-right'>Rp.".number_format($data_product['atpp_price'],0,'.',',')."</td>
            </tr>
            ";
        }

        $message = $obj_mail->payment($po_name, $J_bank, $loop, $datas, $data_details, $path['confirm-payment']);
        $messageAdmin = $obj_mail->paymentAdmin($po_name, $loop, $datas, $data_details);
        $O_to = $_SESSION['customer_email'];
        $O_subject = "Checkout Confirmation ".$po_name." From Volia Website";
        $O_subjectAdmin = "Order Information ".$po_name." From Volia Website";

        $sent = smtpmailer($O_to, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subject, $message);
        $sentAdmin = smtpmailer($O_toCompany, $smtp['url'], $smtp['from'], $smtp['password'], $seo['company-name'], $O_subjectAdmin, $messageAdmin);
    }else{
        header("Location:{$path['404']}");
    }

    $obj_connect->down();    
    
} else { 
    header("Location:{$path['404']}");
}
?>