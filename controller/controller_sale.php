<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Category.php");
$obj_category = new Category();

require_once("model/Product.php");
$obj_product = new Product();

require_once("model/Brand.php");
$obj_brand = new Brand();

if(!isset($_GET['action'])){

    $obj_connect->up();

    $O_id = mysql_real_escape_string(check_input($_GET['id']));

    if(isset($_GET['page'])){
        $O_page =  mysql_real_escape_string(check_input($_GET['page']));
    } else {
        $O_page =  1;
    }
    if(isset($_GET['sort'])){
        $O_sort =  mysql_real_escape_string(check_input($_GET['sort']));
    } else {
        $O_sort =  "recent";
    }
    if(isset($_GET['view'])){
        $O_view =  mysql_real_escape_string(check_input($_GET['view']));
    } else {
        $O_view =  12;
    }
    if(isset($_GET['brand'])){
        $O_brand =  mysql_real_escape_string(check_input($_GET['brand']));
        $storebox = explode(";", $O_brand);
    } else {
        $O_brand =  "";
        $storebox = "";
    }
    
    if(isset($_GET['min'])){
        $O_min =  mysql_real_escape_string(check_input($_GET['min']));
        if($O_min > 0 && $O_min != ""){
            $O_min_param = $O_min / 1000;
        } else {
            $O_min_param = 0;
        }
    } else {
        $O_min =  0;
        $O_min_param = 0;
    }
    if(isset($_GET['max'])){
        $O_max =  mysql_real_escape_string(check_input($_GET['max']));
        if($O_max > 0 && $O_max != ""){
            $O_max_param = $O_max / 1000;
        } else {
            $O_max_param = 0;
        }
    } else {
        $O_max =  1000000;
        $O_max_param = 1000000;
    }
    
    $datas = $obj_category->get_category_by_child($O_id);
    //var_dump($datas);
    if(is_array($datas)){
    	if($datas[0]['category_gen'] == 2){
    		$category_id = $datas[0]['category_ID'];
        	$strid = "";
	        get_id_by_parent($strid, $datas[0]['category_ID']);
	        $id = $strid != "" ? substr($strid, 1) : 0;
    	}else{
        	$id = $datas[0]['category_ID'];
    	}
    }else{
        $id = "";
    }

    $data_brand = $obj_brand->get_index_sale($id);
    $data_product = $obj_product->get_product_sale($O_page, $id, $O_sort, $O_view, $O_brand, $O_min, $O_max);
    $total_data = is_array($data_product) ? $data_product[0]['total_data_all'] : 0;
    $total_page = is_array($data_product) ? $data_product[0]['total_page'] : 0;
    //var_dump($data_product);
    
    $obj_connect->down();
}
?>