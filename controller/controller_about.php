<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Team.php");
$obj_team = new Team();

$file_json = "uploads/json/content_about_us.json";


if(!isset($_GET['action'])){
	$obj_connect->up();
  	$json = json_decode(file_get_contents($file_json),TRUE);
 
    $J_about_content = $json['brief'];
    $J_about_photo = $json['img'];

    $data_teams = $obj_team->get_team();

	$obj_connect->down();
}
?>