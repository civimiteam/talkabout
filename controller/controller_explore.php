<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Blog.php");
$obj_blog = new Blog();

if(!isset($_GET['action'])){
    $obj_connect->up();
    $O_page = isset($_GET['page']) ? mysql_real_escape_string(check_input($_GET['page'])) : 1;
    
    $datas = $obj_blog->get_blog($O_page);
    $total_data = is_array($datas) ? $datas[0]['total_data_all'] : 0;
    $total_page = is_array($datas) ? $datas[0]['total_page'] : 0;
   	//var_dump($datas);

    $obj_connect->down();
}
?>