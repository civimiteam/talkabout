<?php 
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Confirm.php");
$obj_confirm = new Confirm();

if(isset($_GET['name']) && $_GET['name'] != "")
{    
    $obj_connect->up();

    $co_name = mysql_real_escape_string(check_input($_GET['name'])); //po name

    if(isset($_SESSION['confirmStatus'])){
        unset($_SESSION['confirmStatus']);
    }
    if(isset($_SESSION['confirmMessage'])){
        unset($_SESSION['confirmMessage']);
    }
    
    $obj_connect->down();    
    
} else { 
	header("Location:{$path['404']}");
}
?>