<?php
require_once("model/Connection.php");
$obj_connect = new Connection();

require_once("model/Product.php");
$obj_product = new Product();

require_once("model/PO.php");
$obj_po = new PO();

require_once("model/Address.php");
$obj_address = new Address();

if(isset($_SESSION['customer_id'])){

    if(!isset($_GET['action'])){
        $obj_connect->up();
        //$data_sides = $obj_product->get_sidebar();
    
        $customer_id = $_SESSION['customer_id'];
        $data_address = $obj_address->get_address_by_customer($customer_id);

        if(isset($_SESSION['checkoutStatus'])){
            $checkoutStatus = $_SESSION['checkoutStatus'];
            unset($_SESSION['checkoutStatus']);
        } else {
            $checkoutStatus = "";
        }
    
        if(isset($_SESSION['checkoutMessage'])){
            $checkoutMessage = $_SESSION['checkoutMessage'];
            unset($_SESSION['checkoutMessage']);
        } else {
            $checkoutMessage = "";
        }

        if(isset($_SESSION['snapToken'])){
            $snapToken = $_SESSION['snapToken'];
            unset($_SESSION['snapToken']);
        } else {
            $snapToken = "";
        }

        $obj_connect->down(); 

    } else if(isset($_GET['action'])){

        if($_GET['action'] == "checkout"){
            $obj_connect->up();

            $po_custID = mysql_real_escape_string(check_input($_POST['po_cust']));   
            $po_ship = mysql_real_escape_string(check_input($_POST['po_shipping']));
            $po_weight = mysql_real_escape_string(check_input($_POST['po_weight']));
            $po_service = mysql_real_escape_string(check_input($_POST['po_service']));
            $po_addressID = mysql_real_escape_string(check_input($_POST['po_address']));
            $po_city = mysql_real_escape_string(check_input($_POST['po_city']));
            $po_subtotal= mysql_real_escape_string(check_input($_POST['po_subtotal']));
            $po_payment = mysql_real_escape_string(check_input($_POST['po_payment']));
            $po_total = $_POST['po_subtotal'] + $po_ship;
            $array = count($_POST['product_ID']);
            $po_name = $obj_po->generate_po();
            $po_couponID= mysql_real_escape_string(check_input($_POST['po_coupon_id']));
            $po_couponName= mysql_real_escape_string(check_input($_POST['po_coupon']));
            $po_couponPrice= mysql_real_escape_string(check_input($_POST['po_disc']));
            if($po_couponName != ""){
                $po_total = ($_POST['po_subtotal'] + $po_ship) - $po_couponPrice;
            } else {
                $po_total = $_POST['po_subtotal'] + $po_ship;
            }

            if($po_name != ""){
                if($po_payment == "bank"){
                    $resultPO = $obj_po->create_po($po_name, $po_payment, $po_custID, $po_ship, $po_weight, $po_addressID, $po_subtotal, $po_total, $po_service, $po_couponID, $po_couponName, $po_couponPrice);
                    if (!$resultPO) {
                        $message = "Failed to place order<br/>Please Try Again!";
                        $status = "failed";
                        $m_page = $path['checkout'];
                    } 
                    else if ($resultPO) {
                        for($i=0; $i < $array; $i++){
                            $result = $obj_po->create_atpo($resultPO, $_POST['product_ID'][$i], $_POST['product_qty'][$i], $_POST['product_weight'][$i], $_POST['product_price'][$i], $_POST['product_name'][$i]);
                        }
                        $message = "Thank you for placing order at Volia.";
                        $status = "success";
                        $m_page = $path['thank'].$po_name."/";
                    }
                }else{
                    $resultPO = $obj_po->create_po($po_name, $po_payment, $po_custID, $po_ship, $po_weight, $po_addressID, $po_subtotal, $po_total, $po_service, $po_couponID, $po_couponName, $po_couponPrice);
                    if (!$resultPO) {
                        $message = "Failed to place order via Credit Card<br/>Please Try Again!";
                        $status = "failed";
                        $m_page = $path['checkout'];
                    }
                    else if ($resultPO) {
                        for($i=0; $i < $array; $i++){
                            $result = $obj_po->create_atpo($resultPO, $_POST['product_ID'][$i], $_POST['product_qty'][$i], $_POST['product_weight'][$i], $_POST['product_price'][$i], $_POST['product_name'][$i]);
                        }
                        
                        require_once("veritrans/Veritrans.php");
                        //Set Your server key
                        Veritrans_Config::$serverKey = $veritrans_server_key;

                        // Uncomment for production environment
                        // Veritrans_Config::$isProduction = true;

                        // Uncomment to enable sanitization
                        Veritrans_Config::$isSanitized = true;

                        // Uncomment to enable 3D-Secure
                        Veritrans_Config::$is3ds = true;

                        //array transaction details
                        $transaction_details = array(
                            'order_id' => $po_name,
                            //'order_id' => rand(),
                            'gross_amount' => (int) $po_total, // no decimal allowed for creditcard
                        );

                        //array item details
                        $item_details = array();
                        for($i=0; $i < $array; $i++){
                            $item_details[] = array(
                                'id' => $_POST['product_ID'][$i],
                                'price' => (int) $_POST['product_price'][$i] / $_POST['product_qty'][$i],
                                'quantity' => (int) $_POST['product_qty'][$i],
                                'name' => $_POST['product_name'][$i]
                            );
                        }
                        $item_details[] = array(
                            'id' => "123456",
                            'price' => (int) $po_ship,
                            'quantity' => 1,
                            'name' => "Shipping"
                        );
                        if($po_couponName != ""){
                            $item_details[] = array(
                                'id' => $po_couponName,
                                'price' => (int) -$po_couponPrice,
                                'quantity' => 1,
                                'name' => $po_couponName
                            );
                        }

                        $customer_address = $obj_address->get_by_id($po_addressID);
                        //var_dump($customer_address);
                        if(is_array($customer_address)){
                            $customer_name = $customer_address[0]['customer_fname'];
                            $street = $customer_address[0]['address_street1'] != "" ? $customer_address[0]['address_street1'] : $customer_address[0]['address_name'];
                            $city = $customer_address[0]['address_city'];
                            $zip = $customer_address[0]['address_zip'];
                            $phone = $customer_address[0]['customer_phone'];
                            $email = $customer_address[0]['customer_email'];

                            //array billing or shipping address
                            $billing_address = array(
                                'first_name'    => $customer_name,
                                'last_name'     => "",
                                'address'       => $street,
                                'city'          => $city,
                                'postal_code'   => $zip,
                                'phone'         => $phone,
                                'country_code'  => 'IDN'
                            );

                            //array customer details
                            $customer_details = array(
                                'first_name'    => $customer_name,
                                'last_name'     => "",
                                'email'         => $email,
                                'phone'         => $phone,
                                'billing_address'  => $billing_address,
                                'shipping_address' => $billing_address
                            );
                        }

                        // Fill transaction details
                        $transaction = array(
                          'enabled_payments' => array('credit_card'),
                          'transaction_details' => $transaction_details,
                          'customer_details' => $customer_details,
                          'item_details' => $item_details,
                        );

                        try {
                            $snapToken = Veritrans_Snap::getSnapToken($transaction);
                            $_SESSION['snapToken'] = $snapToken;
                        }
                        catch (Exception $e) {
                            echo $e->getMessage();
                            if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
                                echo "<code>";
                                echo "<h4>Please set real server key from sandbox</h4>";
                                echo "In file: " . __FILE__;
                                echo "<br>";
                                echo "<br>";
                                echo htmlspecialchars('Veritrans_Config::$serverKey = \'$veritrans_server_key\';');
                                die();
                            }
                        }

                        $message = "Processing payment with midtrans at Volia.";
                        $status = "success";
                        $m_page = $path['checkout'];
                    }
                }
            } else {
                $message = "Failed To Confirm Order<br/>Please Try Again !";
                $status = "failed";
                $m_page = $path['checkout'];
            }

            $_SESSION['checkoutStatus'] = $status;
            $_SESSION['checkoutMessage'] = $message;
            header("Location:".$m_page);
            $obj_connect->down();
        }
    }
} else {
    $_SESSION['loginStatus'] = "failed";
    $_SESSION['loginMessage'] = "You Must Login First Before <br/>Checkout Product!";
    header("Location:".$path['login']);
}
?>