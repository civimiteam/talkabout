<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate

	require_once("../model/Connection.php");
	$obj_connect = new Connection();
	
	require_once("../model/Coupon.php");
	$obj_coupon = new Coupon();

	if($_GET['action'] == 'get_coupon'){
		$obj_connect->up();
		$R_message = array("status" => "400", "message" => "No Data");

		$N_coupon = mysql_real_escape_string($_REQUEST['coupon']);
		$result = $obj_coupon->get_coupon($N_coupon);
		if(is_array($result)){
			$R_message = array("status" => "200", "message" => "Data Exists", "data" => $result);
		}

		$obj_connect->down();
		echo json_encode($R_message);
	} else {
		echo "error";
	}
}//end gate
else{
	echo "error";
} 
?>