<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate

	require_once("../model/Connection.php");
	$obj_connect = new Connection();

	require_once("../model/Ongkir.php");
	$obj_ongkir = new Ongkir();

	if($_GET['action'] == 'get_province'){
		$obj_connect->up();

		$datas = $obj_ongkir->showProvince();
		echo $datas;

		$obj_connect->down();		
	}
	else if ($_GET['action'] == 'get_city'){
		$obj_connect->up();

		$province_id = mysql_real_escape_string($_REQUEST['province_id']);
		$datas = $obj_ongkir->showCity($province_id);

		echo $datas;

		$obj_connect->down();
	}
	else if ($_GET['action'] == 'get_ongkir'){
		$obj_connect->up();

		$result = 0;

		$destination = mysql_real_escape_string($_REQUEST['destination']);
		$weight = mysql_real_escape_string($_REQUEST['weight']);
		$service = mysql_real_escape_string($_REQUEST['service']);
		if($service == "REG"){
			$services = "REG";
			$services_city = "CTC";
		} else {
			$services = "YES";
			$services_city = "CTCYES";
		}
		$datas = $obj_ongkir->countOngkir($destination,$weight);

		//echo $datas;

		//parse json
		$costarray = json_decode($datas);
		$results = $costarray->rajaongkir->results;
		if(!empty($results)){
			foreach($results as $r){
				foreach($r->costs as $rc){
					if($rc->service == $services || $rc->service == $services_city){
						// echo "<div>".$rc->service."</div>";
						foreach($rc->cost as $rcc){
							$result = $rcc->value;
						}
					}
				}
			}
		}

		echo $result;
		$obj_connect->down();
	}
	else {
		echo "error";
	}
}//end gate
else{
	echo "error";
} 
?>