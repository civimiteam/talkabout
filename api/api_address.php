<?php 
header('content-type: application/json');
header("access-control-allow-origin: *");

if(isset($_GET['action'])){//start gate

	require_once("../model/Connection.php");
	$obj_connect = new Connection();

	require_once("../model/Address.php");
	$obj_address = new Address();

	//===================================== get story ========================================
	//start get story
	if($_GET['action'] == 'insert'){//get_story
		$obj_connect->up();

		$R_message = array("status" => "400", "message" => "Data not found");

		$N_cust_id = mysql_real_escape_string($_REQUEST['customer']);
        $N_name = mysql_real_escape_string($_REQUEST['name']);
        $N_address = mysql_real_escape_string($_REQUEST['address']);
        $N_cityid = mysql_real_escape_string($_REQUEST['cityid']);
        $N_city = mysql_real_escape_string($_REQUEST['city']);
        $N_stateid = mysql_real_escape_string($_REQUEST['stateid']);
        $N_state = mysql_real_escape_string($_REQUEST['state']);
        $N_zip = mysql_real_escape_string($_REQUEST['zip']);
        $N_country = mysql_real_escape_string($_REQUEST['country']);

		$result = $obj_address->insert_address($N_cust_id, $N_name, $N_address, $N_cityid, $N_city, $N_state, $N_stateid, $N_zip, $N_country);
		echo $result;
	
		$obj_connect->down();		
	}//end get story

	else{
		$R_message = array("status" => "404", "message" => "Action Not Found");
		echo json_encode($R_message);
	}
}//end gate
else{
	$R_message = array("status" => "404", "message" => "Not Found");
	echo json_encode($R_message);
}
?>