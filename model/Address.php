<?php
class Address{

	private $table = "T_ADDRESS";
	private $itemPerPageAdmin = 10;
    private $joinCustomer = "LEFT JOIN T_CUSTOMER ON address_customerID = customer_ID";

//START FUNCTION FOR CLIENT PAGE
    //function for get by id in /controller/controller_checkout.php
    public function get_by_id($id){
        $result = 0;

        $text = "SELECT address_ID, address_name, address_street1, address_street2, address_city, address_zip, 
            customer_fname, customer_phone, customer_email FROM $this->table $this->joinCustomer 
            WHERE address_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
    
    //function insert address in login.php
    public function insert_address($cust_id, $name, $address1, $cityid, $city, $state, $stateid, $zip, $country){
        $result = 0;

        $text = "INSERT INTO $this->table (address_customerID, address_name, address_street1, address_cityID, address_city, address_state, address_stateID, address_zip, address_country, address_createDate) 
            VALUES('$cust_id', '$name', '$address1', '$cityid', '$city', '$state', '$stateid', '$zip', '$country', NOW())";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }
        return $result;
    }

    //function for get addres by customer in address.php
    public function get_address_by_customer($id){
        $result = 0;

        $text = "SELECT * FROM $this->table WHERE address_customerID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
    //function for get data address in module/customer/address.php, public_html/address.php
    public function get_data_address($cust_id){
        $result = 0;

        $text = "SELECT customer_ID, customer_fname, address_ID, address_name, address_type, address_street1, address_street2, address_cityID,
            address_city, address_zip, address_country, address_stateID, address_state FROM $this->table LEFT JOIN T_CUSTOMER ON
            address_customerID = customer_ID WHERE address_customerID = '$cust_id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        //$result = $text;
        return $result;
    }

    //function for insert in module/customer/insert.php
    public function insert_data($cust_id, $name, $address1, $address2, $cityid, $city, $state, $stateid, $zip, $country){
        $result = 0;
        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");

        $text = "INSERT INTO $this->table (address_customerID, address_name, address_street1, address_street2, address_cityID, address_city, address_state, address_stateID, address_zip, address_country, address_createDate) 
            VALUES('$cust_id', '$name', '$address1', '$address2', '$cityid', '$city', '$state', '$stateid', '$zip', '$country', '$now')";
        $query = mysql_query($text);
        if($query){
            $result = 1;
        }
        return $result;
    }

    public function update_data($id, $name, $address1, $address2, $cityid, $city, $state, $stateid, $zip, $country){
        $result = 0;

        $text = "UPDATE $this->table SET address_name = '$name', address_street1 = '$address1', address_street2 = '$address2', address_cityID = '$cityid', address_city = '$city', address_state = '$state', address_stateID = '$stateid', address_zip = '$zip', address_country = '$country' WHERE address_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function delete_data($id){
        $result = 0;

        $text = "DELETE FROM $this->table WHERE address_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
//END FUNCTION FOR ADMIN PAGE
}
?>