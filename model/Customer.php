<?php
class Customer{
	private $table = "T_CUSTOMER";
	private $itemPerPageAdmin = 10;

//START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1){
        $result = 0;  
        //get total data
        $text_total = "SELECT customer_ID FROM $this->table";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }
        $text = "SELECT customer_ID, customer_fname, customer_balance, customer_phone, customer_since, customer_email,
        	(SELECT po_date FROM T_PO WHERE po_custID = customer_ID ORDER BY UNIX_TIMESTAMP(po_modifyDate) DESC LIMIT 0,1) AS po_date, 
        	customer_member FROM $this->table ORDER BY customer_ID DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        //$result = $text;
        return $result;
    }

    public function generate_reset_code($email){
        $result = 0;
        $reset_code = generate_code(8);
        $text = "UPDATE $this->table SET customer_resetCode = '$reset_code' WHERE customer_email = '$email'";
        $query = mysql_query($text);
        if(mysql_affected_rows() >= 1){
            $result = $reset_code;
        } else {
            $result = 0;
        }
        //$result = $text;
        return $result;
    }

    //check the code and username before reset password
    public function check_reset_password($fname, $code){
        $result = 0;

        $text = "SELECT customer_ID FROM $this->table WHERE customer_fname = '$fname' AND customer_resetCode = '$code'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $result = 1;//can be used
        }
        return $result;  
    }

    //function to reset your password and code
    public function reset_password($fname, $password, $code){
        $result = 0;

        $N_password = md5($password);

        $text = "UPDATE $this->table SET customer_password = '$N_password', customer_resetCode = '' WHERE customer_fname = '$fname' AND customer_resetCode = '$code'";
        $query = mysql_query($text);
        if(mysql_affected_rows() >= 1){
            $result = 1;
        }
        return $result;
    }
    public function check_forget_password($email){
        $result = 0;
    
        $text = "SELECT * FROM $this->table WHERE customer_email = '$email' LIMIT 0,1";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
    public function get_data_detail($id){
        $result = 0;
        $text = "SELECT * FROM $this->table WHERE customer_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

    public function update_data($id, $name, $phone, $fax, $note){
        $result = 0;
        $text = "UPDATE $this->table SET customer_fname = '$name', customer_phone = '$phone', customer_fax = '$fax', customer_note = '$note' WHERE customer_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function get_data_wishlist_by_page($id, $page=1){
        $result = 0;
        //get total data
        $text_total = "SELECT aw_ID FROM AT_WISHLIST WHERE aw_customerID = '$id' ORDER BY aw_timestamp DESC";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}
        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);
        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }
        $text = "SELECT * FROM AT_WISHLIST LEFT JOIN T_PRODUCT ON aw_productID = product_ID 
                LEFT JOIN T_CATEGORY ON product_ref_categoryID = category_ID
                LEFT JOIN T_PHOTO ON product_ID = photo_productID
                WHERE aw_customerID = '$id' AND photo_main = 1
                ORDER BY aw_timestamp DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;
    }
//END FUNCTION FOR ADMIN PAGE
    
//START FUNCTION FOR CLIENT PAGE
    //function get data edit in account.php
    public function get_data_edit($id){
        $result = 0;

        $text = "SELECT customer_ID, customer_fname, customer_phone, customer_email FROM $this->table WHERE customer_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }

    //function update data customer in account.php
    public function update_data_customer($id, $name, $phone){
        $result = 0;

        $text = "UPDATE $this->table SET customer_fname = '$name', customer_phone = '$phone' WHERE customer_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function insert_cust($name, $email, $pass, $phone){
        $result = 0;    
        $pass = md5($pass) ;      
        date_default_timezone_set('Asia/Jakarta');
        $now = date("Y-m-d H:i:s");    

        $text = "INSERT INTO $this->table ( customer_fname, customer_phone, customer_email, customer_password, customer_since) VALUES('$name', '$phone', '$email', '$pass', '$now')";
        $query = mysql_query($text);
        if($query){
            $result = mysql_insert_id();
        }        
        return $result;
    }

    public function login($email, $password){        
        $result = 0; //FAILED
        $pass = md5($password) ;
        $text = "SELECT customer_ID, customer_fname, customer_email, customer_login_via FROM $this->table WHERE customer_email = '$email' AND customer_password = '$pass' LIMIT 0,1";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){//HAS TO BE EXACT 1 RESULT
            $row = mysql_fetch_array($query,MYSQL_ASSOC);
             create_session($row['customer_ID'],$row['customer_fname'], $row['customer_email'], $row['customer_login_via']);
            $result = 1;
        }       
        return $result;
    }

    //function check password in password.php
    public function check_password($id, $password){        
        $result = 0; //FAILED

        $pass = md5($password);
        $text = "SELECT customer_password FROM $this->table WHERE customer_ID = '$id' AND customer_password = '$pass'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) == 1){
            $result = 1;
        }
        return $result;
    }

    //function update password in password.php
    public function update_password($id, $password){
        $result = 0;

        $pass = md5($password);
        $text = "UPDATE $this->table SET customer_password = '$pass' WHERE customer_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function customer_check_login(){
        $result = 0; 
        if(isset($_SESSION['customer_id'])){
            $result = 1;
        }
        return $result;
    }    

    public function customer_logout(){
        unset($_SESSION['customer_email']);
        unset($_SESSION['customer_name']);
        unset($_SESSION['customer_id']);
        $result = 1;
        return $result;
    }
}
?>