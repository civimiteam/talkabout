<?php
class Blog_Category{

	private $table = "T_BLOG_CATEGORY";
	private $join = "LEFT JOIN T_BLOG ON blogcat_ID = blog_categoryID";

//START FUNCTION FOR CLIENT PAGE
	public function get_list(){
		$result = 0;

		$text = "SELECT blogcat_ID, blogcat_name, blogcat_publish, blogcat_img, blogcat_imgThmb 
			FROM $this->table WHERE blogcat_publish = 'Publish' ORDER BY blogcat_sort ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	public function get_data(){
		$result = 0;

		$text = "SELECT COUNT(blog_ID) AS counter, blogcat_ID, blogcat_name, blogcat_publish, blogcat_img, 
			blogcat_imgThmb FROM $this->table $this->join GROUP BY blogcat_ID ORDER BY blogcat_sort ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		//$result = $text;
		return $result;
	}
	
	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE blogcat_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($name, $img, $img_thmb, $sort, $desc, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (blogcat_name, blogcat_img, blogcat_imgThmb, blogcat_sort, blogcat_desc, 
			blogcat_publish, blogcat_createDate) VALUES('$name', '$img', '$img_thmb', '$sort', '$desc', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		return $result;
	}

	public function update_data($id, $name, $sort, $desc, $publish){
		$result = 0;
	
		$text = "UPDATE $this->table SET blogcat_name = '$name', blogcat_sort = '$sort', blogcat_desc = '$desc', 
			blogcat_publish = '$publish' WHERE blogcat_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	
	public function update_data_image($id, $name, $img, $img_thmb, $sort, $desc, $publish){
		$result = 0;
		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET blogcat_name = '$name', blogcat_img = '$img', blogcat_imgThmb = '$img_thmb', 
			blogcat_sort = '$sort', blogcat_desc = '$desc', blogcat_publish = '$publish' WHERE blogcat_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;
		$this->remove_image($id);
		
		$text = "DELETE FROM $this->table WHERE blogcat_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT blogcat_ID, blogcat_img, blogcat_imgThmb FROM $this->table WHERE blogcat_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['blogcat_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['blogcat_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb); 
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>