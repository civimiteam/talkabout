<?php
class Shipping{

	private $table = "T_SHIPPING";
	private $itemPerPageAdmin = 30;

    //START FUNCTION FOR ADMIN PAGE
    public function get_by_id($id){
        $result = 0;    

        $text = "SELECT * FROM $this->table WHERE shipping_ID = '$id'";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
    
	public function get_data_by_page($page=1){
        $result = 0;    

        //get total data
        $text_total = "SELECT * FROM $this->table";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{ 
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

        $text = "SELECT * FROM $this->table ORDER BY shipping_city ASC LIMIT $limitBefore, $this->itemPerPageAdmin";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        
        if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
        return $result;
    }

    public function insert_data($shippingCity, $shippingCost1, $shippingCost2, $shippingCost3, $shippingCost4){
        $result = 0;

        $text = "INSERT INTO $this->table (shipping_city, shipping_cost_1, shipping_cost_2, shipping_cost_3, shipping_cost_4) VALUES ('$shippingCity', '$shippingCost1', '$shippingCost2', '$shippingCost3', '$shippingCost4')";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function delete_data($id){
        $result = 0;

        $text = "DELETE FROM $this->table WHERE shipping_ID='$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

    public function update_data($shippingID, $shippingCity, $shippingCost1, $shippingCost2, $shippingCost3, $shippingCost4){
        $result = 0;

        $text = "UPDATE $this->table SET shipping_city='$shippingCity', shipping_cost_1='$shippingCost1', shipping_cost_2='$shippingCost2', shipping_cost_3='$shippingCost3', shipping_cost_4='$shippingCost4' WHERE shipping_ID = '$shippingID'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }
    //END FUNCTION FOR ADMIN PAGE
}
?>