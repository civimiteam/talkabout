<?php
class Homepage{
	private $table = "T_HOMEPAGE";
//START FUNCTION FOR CLIENT PAGE
	public function get_index(){
		$result = 0;
		$text = "SELECT * FROM $this->table WHERE homepage_publish = 'Publish' ORDER BY homepage_createDate DESC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_data'] = $total_data;
        }
		return $result;
	}
//START FUNCTION FOR ADMIN PAGE
	public function get_data(){
		$result = 0;
		$text = "SELECT * FROM $this->table ORDER BY homepage_title ASC";
		$query = mysql_query($text);
		$total_data = mysql_num_rows($query);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_data'] = $total_data;
        }
		return $result;
	}
 
	public function get_data_detail($id){
		$result = 0;
		$text = "SELECT * FROM $this->table WHERE homepage_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}	
	public function insert_data($title, $img, $img_thmb, $link, $publish){
		$result = 0;
		$text = "INSERT INTO $this->table (homepage_title, homepage_img, homepage_imgThmb, homepage_link, homepage_publish, homepage_createDate) VALUES('$title', '$img', '$img_thmb', '$link', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		return $result;
	}
	public function update_data($id, $title, $link, $publish){
		$result = 0;
		$text = "UPDATE $this->table SET homepage_title = '$title', homepage_publish='$publish', homepage_link = '$link' WHERE homepage_ID = '$id'";
		$query = mysql_query($text);	
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	public function update_data_image($id, $title, $img, $img_thmb, $link, $publish){
		$result = 0;
		$this->remove_image($id); //remove image before
		$text = "UPDATE $this->table SET homepage_title = '$title', homepage_img = '$img', homepage_imgThmb = '$img_thmb', homepage_link = '$link', homepage_publish='$publish' WHERE homepage_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	public function delete_data($id){
		$result = 0;
		$this->remove_image($id); //remove image before
		$text = "DELETE FROM $this->table WHERE homepage_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}
	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT homepage_ID, homepage_img, homepage_imgThmb FROM $this->table WHERE homepage_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/".$row['homepage_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/".$row['homepage_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>