<?php
class Story{

	private $table = "T_STORY";
	private $itemPerPageAdmin = 10;
	private $itemPerPageClient = 6;
	private $join = "LEFT JOIN T_CATEGORY ON story_ref_categoryID = category_ID";

//START FUNCTION FOR CLIENT PAGE
	public function get_featured_story(){
        $result = 0;

        $text = "SELECT category_ID, category_title, story_ID, story_title, story_desc, story_publish, story_featured, story_img, story_imgThmb, story_createDate FROM $this->table $this->join WHERE story_featured = 'yes' AND story_publish = 'Publish' GROUP BY story_ID ORDER BY story_createDate DESC";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
	public function get_by_id($id){
		$result = 0;

		$text = "SELECT * FROM $this->table LEFT JOIN T_CATEGORY ON story_ref_categoryID = category_ID WHERE story_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}
	public function get_related($id){
        $result = 0;

        $text = "SELECT story_ID, story_title, story_desc, story_publish, story_featured, story_img, story_imgThmb, story_createDate FROM $this->table WHERE story_ID != '$id' AND story_publish = 'Publish' ORDER BY story_createDate DESC LIMIT 2";
        $query = mysql_query($text);
        if(mysql_num_rows($query) >= 1){
            $result = array();
            while($row = mysql_fetch_assoc($query)){
                $result[] = $row;
            }
        }
        return $result;
    }
	//function api get story in creative stories page
	public function get_story_by_category($page=1, $category_id){
		$result = 0;
		$cond = "";
		if($category_id != ""){
			$cond = "AND category_ID = '$category_id'";
		}
		//get total data
        $text_total = "SELECT story_ID FROM $this->table $this->join WHERE story_publish = 'Publish' $cond";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageClient);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageClient;
        }

		$text = "SELECT story_ID, story_title, category_ID, category_title, story_desc, story_publish, story_featured, story_img, story_imgThmb 
			FROM $this->table $this->join WHERE story_publish = 'Publish' $cond ORDER BY story_createDate DESC LIMIT 
			$limitBefore, $this->itemPerPageClient";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
		return $result;
	}
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	public function get_data_by_page($page=1){
		$result = 0;
		//get total data
        $text_total = "SELECT story_ID FROM $this->table ORDER BY story_createDate DESC";
        $query_total = mysql_query($text_total);
        $total_data = mysql_num_rows($query_total);
        if($total_data < 1){$total_data = 0;}

        //get total page
        $total_page = ceil($total_data / $this->itemPerPageAdmin);

        if($page <= 1 || $page == null){
            $limitBefore = 0;
        }else{
            $limitBefore = ($page-1) * $this->itemPerPageAdmin;
        }

		$text = "SELECT story_ID, story_title, category_title, story_publish, story_featured, story_img, story_imgThmb FROM $this->table 
			$this->join ORDER BY story_createDate DESC LIMIT $limitBefore, $this->itemPerPageAdmin";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		if(is_array($result)){
            $result[0]['total_page'] = $total_page;
            $result[0]['total_data_all'] = $total_data;
            $result[0]['total_data'] = count($result);
        }
		return $result;
	}

	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE story_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($category_id, $title, $desc, $img, $img_thmb, $publish, $writer, $photographer, $featured){
		$result = 0;

		$text = "INSERT INTO $this->table (story_ref_categoryID, story_title, story_desc, story_writer, story_photograph, story_img, story_imgThmb, story_publish, story_featured, story_createDate) VALUES('$category_id', '$title', '$desc', '$writer', '$photographer', '$img', '$img_thmb', '$publish', '$featured', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = mysql_insert_id();
		}
		return $result;
	}

	public function update_data($id, $category_id, $title, $desc, $publish, $writer, $photographer, $featured){
		$result = 0;
		
		$text = "UPDATE $this->table SET story_ref_categoryID = '$category_id', story_title = '$title', story_desc = '$desc', story_writer='$writer', 	story_photograph='$photographer', story_publish = '$publish', story_featured = '$featured' WHERE story_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function update_data_image($id, $category_id, $title, $desc, $img, $img_thmb, $publish, $writer, $photographer, $featured){
		$result = 0;
		$this->remove_image($id); //remove image before

		$text = "UPDATE $this->table SET story_ref_categoryID = '$category_id', story_title = '$title', story_desc = '$desc', story_writer='$writer', 	story_photograph='$photographer', story_img = '$img', story_imgThmb = '$img_thmb', story_publish = '$publish', story_featured = '$featured' WHERE story_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id){
		$result = 0;
		$this->remove_image($id); //remove image before
		$this->delete_data_tag($id);
		$this->remove_photo($id) ; //remove all story photo
		$this->delete_data_photo($id) ;

		$text = "DELETE FROM $this->table WHERE story_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data_tag($id){
		$result = 0;
		
		$text = "DELETE FROM AT_TAG WHERE at_typeID = '$id' AND at_type = 'story'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;

		$text = "SELECT story_ID, story_img, story_imgThmb FROM $this->table WHERE story_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/dev/unithree/".$row['story_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/dev/unithree/".$row['story_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}

	//delete data in T_STORY_IMAGE
    public function delete_data_photo($id){
        $result = 0;

        $text = "DELETE FROM T_STORY_IMAGE WHERE pi_ref_storyID = '$id'";
        $query = mysql_query($text);
        if(mysql_affected_rows() == 1){
            $result = 1;
        }
        return $result;
    }

	public function remove_photo($id){
        $result = 0;
        $flag_img = 0;
        $flag_img_thmb = 0;

        $text = "SELECT pi_img, pi_img_thmb FROM T_STORY_IMAGE WHERE pi_ref_storyID = '$id'";
        $query = mysql_query($text);
        while ($row = mysql_fetch_array($query, MYSQL_ASSOC)) 
        {
            $deleteImg = $_SERVER['DOCUMENT_ROOT']."/dev/unithree/".$row['pi_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $_SERVER['DOCUMENT_ROOT']."/dev/unithree/".$row['pi_img_thmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }

            if($flag_img == 1 && $flag_img_thmb ==1){
                $result = 1;
            }
        }
        return $result;
    }
//END FUNCTION FOR ADMIN PAGE
}
?>