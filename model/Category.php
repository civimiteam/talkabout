<?php
class Category{

	private $table = "T_CATEGORY";

//START FUNCTION FOR CLIENT PAGE
	//functio for get index in module/product/index.php, insert.php, edit.php
	public function get_index(){
		$result = 0;

		$text = "SELECT category_ID, category_name, category_slug, category_publish 
			FROM $this->table WHERE category_publish = 'Publish' ORDER BY category_sort ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	/*//function for get category by child in sub-category.php
	public function get_category_by_child($id){
		$result = 0;

		$text = "SELECT child.category_ID, child.category_name, child.category_gen, child.category_img, 
			child.category_imgThmb, parent.category_ID AS parent_category_ID, parent.category_name AS parent_category_name,
			parent.category_parentID AS parent_category_parentID FROM $this->table child LEFT JOIN T_CATEGORY parent ON 
			child.category_parentID = parent.category_ID WHERE child.category_publish = 'Publish' AND child.category_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			$loop = 0;
			while($row = mysql_fetch_assoc($query)){
				$result[$loop] = $row;
				$cond = "";
				if($row['category_gen'] == 2){
					$cond = "category_ID = '{$row['parent_category_ID']}'";
				}else{
					$cond = "category_ID = '{$row['parent_category_parentID']}'";
				}

                $text_parent = "SELECT category_ID, category_name FROM $this->table WHERE $cond";
                $query_parent = mysql_query($text_parent);
                if(mysql_num_rows($query_parent) >= 1){
                    while($row_parent = mysql_fetch_array($query_parent, MYSQL_ASSOC)){
                        $result[$loop]['parent'] = $row_parent;
                    }
                }
                $loop++;
			}
		}
		//$result = $text;
		return $result;
	}	

	//function for get category by parent in category.php
	public function get_category_by_parent($id){
		$result = 0;

		$text = "SELECT category_ID, category_name, category_img, category_imgThmb 
			FROM $this->table WHERE category_publish = 'Publish' AND category_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}*/
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	//function for get data in module/category/index.php
	public function get_data(){
		$result = 0;

		$text = "SELECT COUNT(product_ID) AS counter, category_ID, category_name, category_publish, category_img, category_imgThmb
			FROM $this->table LEFT JOIN T_PRODUCT ON category_ID = product_ref_categoryID 
			GROUP BY category_ID ORDER BY category_sort ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE category_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($name, $slug, $sort, $img, $img_thmb, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (category_name, category_slug, category_sort, category_img, category_imgThmb, category_publish, category_createDate) VALUES('$name', '$slug', '$sort', '$img', '$img_thmb', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		return $result;
	}

	public function update_data($id, $name, $slug, $sort, $img, $img_thmb, $publish, $path){
		$result = 0;
		$cond = "";
		if($img != ""){
			$this->remove_image($id, $path);
			$cond = "category_img = '$img', category_imgThmb = '$img_thmb',";
		}

		$text = "UPDATE $this->table SET $cond category_name = '$name', category_slug = '$slug', category_sort = '$sort', $cond category_publish = '$publish' WHERE category_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id, $path){
		$result = 0;
		$this->remove_image($id, $path);
		
		$text = "DELETE FROM $this->table WHERE category_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id, $path){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT category_ID, category_img, category_imgThmb FROM $this->table WHERE category_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $path.$row['category_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $path.$row['category_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb); 
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>