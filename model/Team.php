<?php
class Team{

	private $table = "T_TEAM";

//START FUNCTION FOR CLIENT PAGE
	public function get_team(){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE team_publish = 'Publish' ORDER BY team_createDate ASC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}
//END FUNCTION FOR CLIENT PAGE

//START FUNCTION FOR ADMIN PAGE
	public function get_data(){
		$result = 0;

		$text = "SELECT * FROM $this->table ORDER BY team_createDate DESC";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}
 
	public function get_data_detail($id){
		$result = 0;

		$text = "SELECT * FROM $this->table WHERE team_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) >= 1){
			$result = array();
			while($row = mysql_fetch_assoc($query)){
				$result[] = $row;
			}
		}
		return $result;
	}

	public function insert_data($name, $position, $desc, $facebook, $instagram, $twitter, $img, $img_thmb, $publish){
		$result = 0;

		$text = "INSERT INTO $this->table (team_name, team_position, team_desc, team_facebook, team_instagram, team_twitter, team_img, team_imgThmb, team_publish, team_createDate) VALUES('$name', '$position', '$desc', '$facebook', '$instagram', '$twitter', '$img', '$img_thmb', '$publish', NOW())";
		$query = mysql_query($text);
		if($query){
			$result = 1;
		}
		return $result;
	}

	public function update_data($id, $name, $position, $desc, $facebook, $instagram, $twitter, $img, $img_thmb, $publish, $path){
		$result = 0;
		$cond = "";
		if($img != ""){
			$this->remove_image($id, $path);
			$cond = "team_img = '$img', team_imgThmb = '$img_thmb',";
		}

		$text = "UPDATE $this->table SET team_name = '$name', $cond team_position = '$position', team_desc = '$desc', team_facebook = '$facebook', team_instagram = '$instagram', team_twitter = '$twitter', team_publish='$publish' WHERE team_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function delete_data($id, $path){
		$result = 0;
		$this->remove_image($id, $path); //remove image before

		$text = "DELETE FROM $this->table WHERE team_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_affected_rows() == 1){
			$result = 1;
		}
		return $result;
	}

	public function remove_image($id, $path){
		$result = 0;
		$flag_img = 0;
		$flag_img_thmb = 0;
		$text = "SELECT team_ID, team_img, team_imgThmb FROM $this->table WHERE team_ID = '$id'";
		$query = mysql_query($text);
		if(mysql_num_rows($query) == 1){
			$row = mysql_fetch_assoc($query);
            $deleteImg = $path.$row['team_img'];
            if (file_exists($deleteImg)) {
                unlink($deleteImg);
                $flag_img = 1;
            }
            $deleteImgThmb = $path.$row['team_imgThmb'];
            if (file_exists($deleteImgThmb)) {
                unlink($deleteImgThmb);
                $flag_img_thmb = 1;
            }
            if($flag_img == 1 && $flag_img_thmb ==1){
            	$result = 1;
            }
		}
		return $result;
	}
//END FUNCTION FOR ADMIN PAGE
}
?>