<?php 
//include("packages/require.php");
//include("controller/controller_home.php");
$curpage='home';
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title><?php echo $seo['title-home'];?></title>
    <meta name="keywords" content="<?php echo $seo['keyword-home'];?>">
    <meta name="description" content="<?php echo $seo['desc-home'];?>">
    <?php include("packages/head.php");?>
    <link rel="stylesheet" href="<?php echo $global['absolute-url'];?>packages/swiper/css/swiper.min.css"/>
    <script src="<?php echo $global['absolute-url'];?>packages/swiper/js/swiper.min.js"></script>
</head>
<body>
	<div class="device-max">
		<!-- START SECTION HEADER -->
		<?php include("parts/part-header.php");?>
		<!-- END SECTION HEADER -->
		<!-- START SECTION BODY -->	
		<div class="section-body up2">
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12">
						<div class="title center border-top border-bottom font16px padding-top-5 padding-bottom-5 MontserratRegular">
							DETAIL LOOKBOOK
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-xs-12 pad0">
						<div class="up4">
							<ol class="breadcrumb background-white MontserratLight">
								<li class="breadcrumb-item"><a href="index2.php">HOME</a></li>
								<li class="breadcrumb-item"><a href="lookbook.php">LOOKBOOK</a></li>
							  	<li class="breadcrumb-item active">QUIRKY PARTY</li>
							</ol>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12">
						<div>
							<a href="#">
								<img src="<?php echo $global['absolute-url'];?>img/Lookbook/02/LB-1_copy1.jpg" width="100%">
							</a>
						</div>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row">
					<div class="col-sm-6 col-xs-12 up3 margin-top-xs-35">
						<a href="#">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/02/LB-41.jpg" style="width: 100%">
						</a>
					</div>
					<div class="col-sm-6 col-xs-12 up3 margin-top-xs-20">
						<a href="#">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/02/LB-51.jpg" style="width: 100%">
						</a>
					</div>
				</div>
			</div>
			<div class="container" style="width: 100%">
				<div class="row margin-bottom-sm-60 padding-bottom-xs-15">
					<div class="col-sm-6 col-xs-12 up3 margin-top-xs-20">
						<a href="#">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/02/LB-31.jpg" style="width: 100%">
						</a>
					</div>
					<div class="col-sm-6 col-xs-12 up3 margin-top-xs-20">
						<a href="#">
							<img src="<?php echo $global['absolute-url'];?>img/Lookbook/03/64.jpg" style="width: 100%">
						</a>
					</div>
				</div>
			</div>	
		</div>
		<!-- END SECTION BODY -->
		<!-- START SECTION FOOTER -->
		<?php include("parts/part-footer.php");?>
		<!-- END SECTION FOOTER -->
	</div> 
    <script src="<?php echo $global['absolute-url'];?>js/global.js"></script>
    <script src="<?php echo $global['absolute-url'];?>js/home.js"></script>
</body>	
</html>