<?php
if(isset($_GET['action'])){

	function midtrans($method, $order_id, $action)
    {
    	require_once(dirname(__FILE__) . '/../Veritrans.php');
        $curl = curl_init();
        curl_setopt_array($curl, array(
          CURLOPT_URL => Veritrans_Config::getBaseUrl()."/".$order_id."/".$action,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => $method,
          CURLOPT_HTTPHEADER => array(
          	"Content-Type: application/json",
		    "Accept: application/json",
		    "Authorization: Basic ".$veritrans_server_key_base64
          ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            $result = 'error';
            return 'error';
        } else {
            return json_decode(trim($response), TRUE);
        }
    }
	
	if($_GET['action'] == "get_status" && $_GET['order_id'] != ""){
		$O_order_id = $_GET['order_id'];
		$result = midtrans("GET", $O_order_id, "status");
		var_dump($result);
	}

	else{
		echo "error authorized";
	}
}else{
	echo "error not action";
}
?>