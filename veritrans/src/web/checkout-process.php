<?php
require_once(dirname(__FILE__) . '/../../Veritrans.php');
//Set Your server key
Veritrans_Config::$serverKey = $veritrans_server_key;

// Uncomment for production environment
// Veritrans_Config::$isProduction = true;

// Uncomment to enable sanitization
// Veritrans_Config::$isSanitized = true;

// Uncomment to enable 3D-Secure
// Veritrans_Config::$is3ds = true;

// Required
$transaction_details = array(
  'order_id' => rand(),
  'gross_amount' => 550000, // no decimal allowed for creditcard
  );

// Optional
$item1_details = array(
    'id' => 'insta001',
    'price' => 150000,
    'quantity' => 2,
    'name' => "Kartu Nama"
    );

// Optional
$item2_details = array(
    'id' => 'insta002',
    'price' => 50000,
    'quantity' => 5,
    'name' => "Brochure"
    );

// Optional
$item_details = array ($item1_details, $item2_details);

// Optional
$billing_address = array(
    'first_name'    => "Cliff Dean Smith",
    'last_name'     => "Arsadi",
    'address'       => "RGTC",
    'city'          => "Jakarta Timur",
    'postal_code'   => "13910",
    'phone'         => "0852188184",
    'country_code'  => 'IDN'
    );

// Optional
$shipping_address = array(
    'first_name'    => "Cliff Dean Smith",
    'last_name'     => "Arsadi",
    'address'       => "RGTC",
    'city'          => "Jakarta Timur",
    'postal_code'   => "13910",
    'phone'         => "085212188184",
    'country_code'  => 'IDN'
    );

// Optional
$customer_details = array(
    'first_name'    => "Cliff Dean Smith",
    'last_name'     => "Arsadi",
    'email'         => "cliff.dean@eannovate.com",
    'phone'         => "085212188184",
    'billing_address'  => $billing_address,
    'shipping_address' => $shipping_address
    );

// Fill transaction details
$transaction = array(
    /*"vtweb" => array (
      "enabled_payments" => array("credit_card"),
      "credit_card_3d_secure" => true,
      //Set Redirection URL Manually
      "finish_redirect_url" => "https://www.eannovate.com/dev/instaprint/veritrans/src/notification-handler.php",
      "unfinish_redirect_url" => "https://www.example.co.id/payment/unfinish",
      "error_redirect_url" => "https://www.example.co.id/payment/error"
    ),*/
    'transaction_details' => $transaction_details,
    'customer_details' => $customer_details,
    'item_details' => $item_details,
    );

try {
  // Redirect to Veritrans VTWeb page
  header('Location: ' . Veritrans_VtWeb::getRedirectionUrl($transaction));
}
catch (Exception $e) {
  echo $e->getMessage();
  if(strpos ($e->getMessage(), "Access denied due to unauthorized")){
      echo "<code>";
      echo "<h4>Please set real server key from sandbox</h4>";
      echo "In file: " . __FILE__;
      echo "<br>";
      echo "<br>";
      echo htmlspecialchars('Veritrans_Config::$serverKey = \'$veritrans_server_key\';');
      die();
}

}
