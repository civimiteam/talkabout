<?php
  $base = $_SERVER['REQUEST_URI'];
?>
<center>
<table border="1">
  	<tr>
  		<th width="30">Id</th>
		<th width="150">Product Name</th>
		<th width="100">Price</th>
		<th width="75">Qty</th>
		<th width="100">Subtotal</th>
  	</tr>
  	<tr>
  		<td style="text-align: center;">1.</td>
  		<td>Kartu Nama</td>
  		<td style="text-align: right;">Rp 150.000</td>
  		<td style="text-align: center;">2</td>
  		<td style="text-align: right;">Rp 300.000</td>
  	</tr>
  	<tr>
  		<td style="text-align: center;">2.</td>
  		<td>Brochure</td>
  		<td style="text-align: right;">Rp 50.000</td>
  		<td style="text-align: center;">5</td>
  		<td style="text-align: right;">Rp 250.000</td>
  	</tr>
  	<tr>
  		<td colspan="4" style="text-align: right;">Amount</td>
  		<td style="text-align: right;">Rp 550.000</td>
  	</tr>
  	<tr>
  		<td colspan="5" style="text-align: center;">
  			<form action="<?php echo $base ?>checkout-process.php" method="GET">
				<input type="submit" value="Click to Pay">
			</form>
  		</td>
  	</tr>
</table>
</center>