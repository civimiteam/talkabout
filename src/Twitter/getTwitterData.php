<?php
session_start();

require("TwitterOAuth.php");
require("Twitter_key.php");

require_once("../../packages/front_config.php");

require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/Third_party.php");
$obj_tw = new Third_party();

require_once("../../../model/Setting.php");
$obj_set = new Setting();

if (!empty($_GET['oauth_verifier']) && !empty($_SESSION['oauth_token']) && !empty($_SESSION['oauth_token_secret'])) {
    // We've got everything we need
    $twitteroauth = new TwitterOAuth(YOUR_CONSUMER_KEY, YOUR_CONSUMER_SECRET, $_SESSION['oauth_token'], $_SESSION['oauth_token_secret']);
    // Let's request the access token
    $access_token = $twitteroauth->getAccessToken($_GET['oauth_verifier']);
    // Save it in a session var
    $_SESSION['access_token'] = $access_token;
    // Let's get the user's info
    $user_info = $twitteroauth->get('account/verify_credentials');
    // Print user's info
    if (isset($user_info->error)) {
        header("Location: {$global['base']}");  // Something's wrong, go back to square 1  
    } else {
        $obj_con->up();    
        $get_name = explode(" ", $user_info->name); // get first name, last name

        $tw_uid             = $user_info->id;
        $tw_username        = $user_info->screen_name;
        $tw_fullname        = $user_info->name;
        $tw_firstname       = $get_name[0];
        $tw_lastname        = $get_name[1];
        $tw_profile_picture = str_replace("_normal","",$user_info->profile_image_url);
        $tw_provider        = "Twitter";

        $check = $obj_tw->check_primary_username($tw_username); // check username
        //var_dump($check);
        if(is_array($check)){
            $result = $obj_tw->user_login($check[0]['User_email']);
            if(is_array($result)){
                $obj_tw->update_action_login($result[0]['User_ID']); //update for last login and num login
                //if auth_code null, update
                if($result[0]['User_auth_code'] == "" || $result[0]['User_auth_code'] == NULL){
                  $auth_code = $obj_tw->update_auth_code($result[0]['User_ID']);
                  //create cookie for login
                  create_cookie($result[0]['User_ID'], $result[0]['User_username'], $auth_code, $result[0]['User_email']);
                }else{
                  //create cookie for login
                  create_cookie($result[0]['User_ID'], $result[0]['User_username'], $result[0]['User_auth_code'], $result[0]['User_email']);
                }
                $obj_set->insert_data($result[0]['User_ID'], 1, 2, 3, 4, 5, 6, 7); //insert setting sort
                $location = $path['user-edit']; //redirect url
            }else{
                $location = $global['base']; 
            }
        }else{
            $N_fname    = $tw_firstname;
            $N_lname    = $tw_lastname;   
            $N_username = $tw_username;
            $N_picture  = $tw_profile_picture;
            $N_provider = $tw_provider;  

            $result_create = $obj_tw->insert_data_username($N_fname, $N_lname, $N_username, $N_provider, $N_picture, $N_picture);
            //var_dump($result_create);
            if($result_create){
                if(!isset($_SESSION['userData']['id'])){
                    $_SESSION['userData']['id'] = $result_create; //session id  
                }
                if(!isset($_SESSION['userData']['username'])){
                    $_SESSION['userData']['username'] = $N_username; //session username  
                }
                $obj_set->insert_data($result_create, 1, 2, 3, 4, 5, 6, 7); //insert setting sort
                $location = $path['form-personal'];
            }else if(!$result_create){
                $location = $global['base'];
            }
        }
        
        header("Location:".$location);
        $obj_con->down();
    }
} else {
    // Something's missing, go back to square 1
    header('Location: index.php');
}
?>
