<?php
session_start(); //start session

//include google api files
include("Google/Google_key.php");
require_once("Google/Google_Client.php");
require_once("Google/contrib/Google_Oauth2Service.php");
require_once("../packages/front_config.php");

require_once("../model/Connection.php");
$obj_con = new Connection(); 

require_once("../model/Third_party.php");
$obj_gp = new Third_party();

$gClient = new Google_Client();
$gClient->setApplicationName('Login to volia.id');
$gClient->setClientId($google_client_id);
$gClient->setClientSecret($google_client_secret);
$gClient->setRedirectUri($google_redirect_url);
$gClient->setDeveloperKey($google_developer_key);

$google_oauthV2 = new Google_Oauth2Service($gClient);

//If user wish to log out, we just unset Session variable
if (isset($_REQUEST['reset'])) 
{
  unset($_SESSION['token']);
  $gClient->revokeToken();
  header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL)); //redirect user back to page
}

//If code is empty, redirect user to google authentication page for code.
//Code is required to aquire Access Token from google
//Once we have access token, assign token to session variable
//and we can redirect user back to page and login.
if (isset($_GET['code'])) 
{ 
	$gClient->authenticate($_GET['code']);
	$_SESSION['token'] = $gClient->getAccessToken();
	header('Location: ' . filter_var($google_redirect_url, FILTER_SANITIZE_URL));
	return;
}

if (isset($_SESSION['token'])) 
{ 
	$gClient->setAccessToken($_SESSION['token']);
}

if ($gClient->getAccessToken()) 
{
	$obj_con->up();

	//For logged in user, get details from google using access token
	$user 				= $google_oauthV2->userinfo->get();
	$id 				= $user['id'];
	$birthday 			= $user['birthday'];
	$firstname 			= filter_var($user['given_name'], FILTER_SANITIZE_SPECIAL_CHARS);
	$lastname 			= filter_var($user['family_name'], FILTER_SANITIZE_SPECIAL_CHARS);
	$fullname 			= filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
	$email 				= filter_var($user['email'], FILTER_SANITIZE_EMAIL);
	$profile_image_url 	= filter_var($user['picture'], FILTER_VALIDATE_URL);
	$provider           = "Google"; // Provider Name
	
	$check = $obj_gp->check_primary_email($email); // check e-mail 
    if(is_array($check)){
	    $result = $obj_gp->user_login($check[0]['customer_email']);
	    if(is_array($result)){
	    	$location = $path['account']; //redirect url
	    }else{
       		$location = $path['home']; //redirect url
      	}
    }else{
	  	/* --- Set Variables --- */
	    $N_fname    = $firstname." ".$lastname;
      	$N_email    = $email;
      	$N_provider = $provider;

      	//START VARIABLE SEND E-MAIL
        $O_bodyCust = " <!DOCTYPE html>
	        <html>
	        <head>
	        <title></title>
	        </head>
	        <body>
	        <span style='font-size:12px;'><em>*This is a message from Volia</em></span>
	        <br/>
	        <br/><strong> Your account has been successfully created.</strong><br/>
	        Thank you for register as customer in our website.
	        <br/><br/>
	        <b>Name</b> : ".$N_fname."
	        <br/><br/>
	        <b>E-mail</b> : ".$N_email."
	        <br/><br/>      
	        <b>Register via</b> : ".$N_provider."
	        <br/><br/>
	        Please follow this link for view your account : 
	        <a href='".$path['account']."'>Your account</a>
	        <br/><br/>
	        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
	        </body>
	        </html>
        ";

        $O_bodyAdmin = " <!DOCTYPE html>
	        <html>
	        <head>
	        <title></title>
	        </head>
	        <body>
	        <span style='font-size:12px;'><em>*This is a message from Volia</em></span>
	        <br/>
	        <br/><strong> New account has been successfully created.</strong><br/>
	        <br/><br/>
	        <b>Name</b> : ".$N_fname."
	        <br/><br/>
	        <b>E-mail</b> : ".$N_email."
	        <br/><br/>      
	        <b>Register via</b> : ".$N_provider."
	        <br/><br/>
	        <em>*Do not reply to this e-mail.<br/><br/>Thank you!</em>
	        </body>
	        </html>
        ";

        $O_toCust = $N_email;
        $file_json = $global['root-url']."uploads/json/company_profile.json";
        $json = json_decode(file_get_contents($file_json),TRUE);
        $J_Company_email = $json['email1'];
        $O_toCompany = $J_Company_email;

        $O_subjectCust = "Volia Website account registration";
        $O_subjectAdmin = "New account registered";
        //END VARIABLE SEND E-MAIL
	    
		$result_create = $obj_gp->insert_data($N_fname, $N_email, $N_provider);
	    //var_dump($result_create);
	    if($result_create){
	    	$sentCust = smtpmailer($O_toCust, "info@volia.id", "Volia System", $O_bodyCust, $O_subjectCust); 
        	$sentAdmin = smtpmailer($O_toCompany, "info@volia.id", "Volia System", $O_bodyAdmin, $O_subjectAdmin);
	        $location = $path['account'];
	   	}else if(!$result_create){
	        $location = $path['home'];
	    }
    }

    $_SESSION['token'] 	= $gClient->getAccessToken();
	unset($_SESSION['token']);
    header("Location:".$location);
    $obj_con->down();
}
else 
{
	//For Guest user, get google login url
	$authUrl = $gClient->createAuthUrl();
	header("Location:".$authUrl);
}

?>