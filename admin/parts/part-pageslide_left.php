<a class="closedbar inner hidden-sm hidden-xs" href="#">
</a>
<nav id="pageslide-left" class="pageslide inner">
	<div class="navbar-content">
		<!-- start: SIDEBAR -->
		<div class="main-navigation left-wrapper transition-left">
			<div class="navigation-toggler hidden-sm hidden-xs">
				<a href="#main-navbar" class="sb-toggle-left">
				</a>
			</div>
			<div class="user-profile border-top padding-horizontal-10 block">
				<div class="inline-block hide">
					<img src="<?=$global['absolute-url-admin'];?>assets/images/avatar-1.jpg" alt="">
				</div>
				<div class="inline-block">
					<h5 class="no-margin"> Welcome </h5>
					<h4 class="no-margin"> <?=$_SESSION['admin_username'];?> </h4>
					
				</div>
			</div>
			<!-- start: MAIN NAVIGATION MENU -->
			<ul class="main-navigation-menu">
				<li <?php if($curpage == "homepage") { ?>class="active"<?php } ?>>
					<a href="<?=$path['homepage'];?>"><i class="fa fa-desktop"></i> <span class="title"> Homepage </span></a>
				</li>
				<li <?php if($curpage == "banner") { ?>class="active"<?php } ?>>
					<a href="<?=$path['banner'];?>"><i class="fa fa-bookmark"></i> <span class="title"> Banner </span></a>
				</li>
				<li <?php if($curpage == "category") { ?>class="active"<?php } ?>>
					<a href="<?=$path['category'];?>"><i class="fa fa-file-text-o"></i> <span class="title"> Categories </span></a>
				</li>
				<li <?php if($curpage == "product") { ?>class="active"<?php } ?>>
					<a href="javascript:void(0)"><i class="fa fa-folder"></i> <span class="title bold"> Products </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="<?=$path['product'];?>">
								<span class="title"><i class="fa fa-folder"></i>  All Products </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['product'];?>?page=1&tag=productTrending&cat=">
								<span class="title"><i class="fa fa-folder" style="color: #5CB85C;"></i>  Trending </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['product'];?>?page=1&tag=productBestSeller&cat=">
								<span class="title"><i class="fa fa-folder" style="color: #F0AD4E;"></i>  Best Seller </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['product'];?>?page=1&tag=productOutOfStock&cat=">
								<span class="title"><i class="fa fa-folder" style="color: #555555;"></i>  Out Of Stock </span>
							</a>
						</li>
					</ul>
				</li>
				<li <?php if($curpage == "coupon") { ?>class="active"<?php } ?>>
					<a href="<?=$path['coupon'];?>"><i class="fa fa-tag"></i> <span class="title"> Coupon</span></a>
				</li>
				<!--<li <?php if($curpage == "blogcat") { ?>class="active"<?php } ?>>
					<a href="<?=$path['blogcat'];?>"><i class="fa fa-file-text-o"></i> <span class="title"> Blog Category </span></a>
				</li>
				<li <?php if($curpage == "blog") { ?>class="active"<?php } ?>>
					<a href="<?=$path['blog'];?>"><i class="fa fa-pencil-square-o"></i> <span class="title bold"> Blog News </span></a>
				</li>!-->
				<li <?php if($curpage == "order") { ?>class="active"<?php } ?>>
					<a href="<?=$path['order'];?>"><i class="fa fa-archive"></i> <span class="title bold"> Order Management </span></a>
				</li>
				<li <?php if($curpage == "confirmation") { ?>class="active"<?php } ?>>
					<a href="<?=$path['confirmation'];?>"><i class="fa fa-check-square"></i> <span class="title"> Payment Confirmation </span></a>
				</li>
				<li <?php if($curpage == "customer") { ?>class="active"<?php } ?>>
					<a href="<?=$path['customer'];?>"><i class="fa fa-users"></i> <span class="title bold"> Customer </span></a>
				</li>
				<li <?php if($curpage == "content") { ?>class="active"<?php } ?>>
					<a href="javascript:void(0)"><i class="fa fa-book"></i> <span class="title"> Content </span><i class="icon-arrow"></i> </a>
					<ul class="sub-menu">
						<li>
							<a href="<?=$path['content'];?>">
								<span class="title"><i class="fa fa-book"></i>  About Us </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['content-shipping'];?>">
								<span class="title"><i class="fa fa-book"></i>  Shipping </span>
							</a> 
						</li>
						<li>
							<a href="<?=$path['content-payment'];?>">
								<span class="title"><i class="fa fa-book"></i>  Payment </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['content-how'];?>">
								<span class="title"><i class="fa fa-book"></i>  How To Order </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['content-faq'];?>">
								<span class="title"><i class="fa fa-book"></i>  FAQ </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['content-return'];?>">
								<span class="title"><i class="fa fa-book"></i>  Return Info </span>
							</a>
						</li>
						<li>
							<a href="<?=$path['content-terms'];?>">
								<span class="title"><i class="fa fa-book"></i>  Terms Condition </span>
							</a>
						</li>
					</ul>
				</li>
				<!--<li <?php if($curpage == "wishlist") { ?>class="active"<?php } ?>>
					<a href="<?=$path['wishlist'];?>"><i class="fa fa-archive"></i> <span class="title"> Wish List </span></a>
				</li>!-->

				<li <?php if($curpage == "team") { ?>class="active"<?php } ?>>
					<a href="<?=$path['team'];?>"><i class="fa fa-group"></i> <span class="title"> Team </span></a>
				</li>
				<li <?php if($curpage == "social") { ?>class="active"<?php } ?>>
					<a href="<?=$path['social'];?>"><i class="fa fa-link"></i> <span class="title"> Social </span></a>
				</li>
				<li <?php if($curpage == "newsletter") { ?>class="active"<?php } ?>>
					<a href="<?=$path['newsletter'];?>"><i class="fa fa-envelope-o"></i> <span class="title"> Newsletter </span></a>
				</li>
				<li <?php if($curpage == "profile") { ?>class="active"<?php } ?>>
					<a href="<?=$path['profile'];?>"><i class="fa fa-building-o"></i> <span class="title"> Company Profile </span></a>
				</li>
				<li <?php if($curpage == "user") { ?>class="active"<?php } ?>>
					<a href="<?=$path['user'];?>"><i class="fa fa-user"></i> <span class="title"> Users </span></a>
				</li>
				
			</ul>
			<!-- end: MAIN NAVIGATION MENU -->
		</div>
		<!-- end: SIDEBAR -->
	</div>
	<div class="slide-tools">
		<div class="col-xs-6 text-left no-padding">
			<a class="btn btn-sm status" href="#">
				Status <i class="fa fa-dot-circle-o text-green"></i> <span>Online</span>
			</a>
		</div>
		<div class="col-xs-6 text-right no-padding">
			<a class="btn btn-sm log-out text-right" href="<?=$path['logout'];?>">
				<i class="fa fa-power-off"></i> Log Out
			</a>
		</div>
	</div>
</nav>