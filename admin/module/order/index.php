<?php 
include("../../packages/require.php");
include("../../controller/controller_order.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
$curpage="order";
$page_name = "index.php";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['order'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Order History Management 
									<small>MANAGE orders and INPUT resi here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li class="active">
									Order History Management
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
							<div class="col-xs-12">
								<div class="alert alert-info">
									Check new and manage orders and finally input resi JNE. 
								</div>
							</div>
						</div>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading">
									<div style="text-align:left;">
										Total Order History : <span class="label label-info"><?=$total_data;?></span>
									</div>
									<div class="panel-tools">
										<div class="dropdown">
											<a data-toggle="dropdown" class="btn btn-xs dropdown-toggle btn-transparent-grey" aria-expanded="false">
												<i class="fa fa-cog"></i>
											</a>
											<ul class="dropdown-menu dropdown-light pull-right" role="menu" style="display: none;">
												<li>
													<a class="panel-collapse collapses" href="table_data.html#"><i class="fa fa-angle-up"></i> <span>Collapse</span> </a>
												</li>
												<li>
													<a class="panel-refresh" href="table_data.html#">
														<i class="fa fa-refresh"></i> <span>Refresh</span>
													</a>
												</li>
												<li>
													<a class="panel-config" href="table_data.html#panel-config" data-toggle="modal">
														<i class="fa fa-wrench"></i> <span>Configurations</span>
													</a>
												</li>
												<li>
													<a class="panel-expand" href="table_data.html#">
														<i class="fa fa-expand"></i> <span>Fullscreen</span>
													</a>
												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="panel-body">
									<div class="table-responsive">
										<table class="table table-striped table-bordered table-hover table-full-width">
											<tr>
												<th>#</th>
												<th>PO Name</th>
												<th>Order Date</th>
												<th>Buyer Name</th>
												<th>Total Amount</th>
												<th>Service</th>
												<th>Status</th>
												<th>Paid</th>												
												<th>Action</th>
											</tr>
											<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											<tr class="<?php orderStat($data['po_status']);?> ">
												<td><?=($O_page-1)*20+$num;?>.</td>
												<td><?=$data['po_name'];?></td>
												<td><?=date('F j, Y', strtotime($data['po_date']));?></td>
												<td><?=$data['customer_fname'];?></td>
												<td>Rp. <?=number_format(floatval($data['po_totalAmount']),0,',','.');?></td>
												<td> <?=$data['po_service'];?></td>
												<td> <?=convert_status($data['po_status']);?></td>
												<td>
													<?php 
														if($data['po_isPaid'] == 1){
															echo "<span class='label label-success'>PAID</span>";
														} else {
															echo "<span class='label label-danger'>NOT PAID</span>";															
														} 
													?>
												</td>
												
												<td>
													<div class="btn-group text-center">
														<a href="<?=$path['order-edit'].$data['po_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="View">View</a>														
														<a class="btn btn-xs btn-azure" data-poStatus="<?=$data['po_status'];?>" data-custEmail="<?=$data['customer_email'];?>" data-custName="<?=$data['customer_fname'];?>" data-poName="<?=$data['po_name'];?>" data-poid="<?=$data['po_ID'];?>" data-poresi="<?=$data['po_resi'];?>" onclick="transferPOid(this);" href="#panel-resi" data-toggle="modal" data-target="#panel-resi">Resi</a>
														
													</div>
												</td>
											</tr>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="8" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</table>
									</div>
									<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(5*($batch-1));
												$nextLimit = 5 * $batch;
												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if($O_page > 1){
													echo "<li><a href='".$page_name."?page=".($O_page-1)."'><i class='fa fa-chevron-left'></i> Prev</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ 
												?>
												<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
												<?php 
												}
												if($total_page > 1 && $O_page != $total_page){
													echo "<li><a href='".$page_name."?page=".($O_page+1)."'>Next <i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
									<!-- end pagination -->
								</div>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
				<!-- start: PANEL RESI MODAL FORM -->
				<div class="modal fade" id="panel-resi" tabindex="-1" role="dialog" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
									&times;
								</button>
								<h4 class="modal-title">Input Resi</h4>
							</div>
							<form name="addResi" action="index.php?action=resi" enctype="multipart/form-data" method="post" onsubmit="return validateResi();" >
								<div class="modal-body">
									<div class="row">
										<div class="col-sm-4 col-xs-12 up1"><strong>No Resi / Connote  <span class="symbol required"></span></strong> :</div>
										<div class="col-sm-8 col-xs-12 up1">
											<input id="PO_resi_modal" type="text" name="PO_resi"class="form-control" placeholder="no resi" disabled="true" />
											<div id="error-resi" class="is-error"></div>
											<input id="PO_ID_modal" type="hidden" name="PO_ID" />
											<input id="PO_name_modal" type="hidden" name="PO_name" />
											<input id="Cust_name_modal" type="hidden" name="Cust_name" />
											<input id="Cust_email_modal" type="hidden" name="Cust_email" />
										</div>										
									</div>
									<div class="row">
									<div id="text-info" class="col-sm-12 col-xs-12 up1">
											<div class="text-danger" style="text-align: right;"><strong>Input resi hanya bisa dilakukan setelah pembayaran dikonfirmasi.</strong></div> 
									</div>
									</div>
								</div>
								<div class="modal-footer f5-bg">
									<div class="btn-group">
										<button type="reset" class="btn btn-default" data-dismiss="modal">
											<i class="fa fa-times"></i> Cancel
										</button>
										<button id="btn-submit" type="submit" class="btn btn-success">
											<i class="fa fa-check fa fa-white"></i> Confirm Resi
										</button>
									</div>
								</div>
							</form>
						</div>
						<!-- /.modal-content -->
					</div>
					<!-- /.modal-dialog -->
				</div>
				<!-- /.modal -->
				<!-- end: SPANEL RESI MODAL FORM -->
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<script type="text/javascript">
		function transferPOid(x){
			var currentPOid = $(x).attr("data-poid");
			var currentPOresi = $(x).attr("data-poresi");
			var currentPOName = $(x).attr("data-poName");
			var currentPOStatus = $(x).attr("data-poStatus");
			var currentCustName = $(x).attr("data-custName");
			var currentCustEmail = $(x).attr("data-custEmail");
			$('#PO_ID_modal').val(currentPOid);
			$('#PO_resi_modal').val(currentPOresi);
			$('#PO_name_modal').val(currentPOName);
			$('#Cust_name_modal').val(currentCustName);
			$('#Cust_email_modal').val(currentCustEmail);			
			
				
			if(currentPOStatus == 3 || currentPOStatus == 4 ) {	
			$('#PO_resi_modal').attr("disabled", false);
			$('#btn-submit').show();
			$('#text-info').hide();
			} else { 
			$('#PO_resi_modal').attr("disabled", true);
			$('#btn-submit').hide();
			$('#text-info').show();
			}
			     
		}
			function validateResi(){
                var resi = $("#PO_resi_modal").val();
                
                if(resi != ""){
                    $("#error-resi").html("");
                    $("#error-resi").hide();
                    $("#PO_resi_modal").removeClass("input-error");
                } else {
                    $("#error-resi").show();
                    $("#error-resi").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#PO_resi_modal").addClass("input-error");
                    return false;
                }
            }
			<?php if($message != "") { ?>
            //use session here for alert success/failed
            var alertText = "<?=$message;?>"; //teks for alert
            
                <?php if($alert != "success"){ ?>
                    //error alert
                    errorAlert(alertText);
                <?php } else { ?>
                    //success alert
                    successAlert(alertText); 
                <?php } ?>
             
            <?php } ?>
			//errorAlert(alertText); 
			//function confirmation delete
			function confirmDelete(num){
				swal({
					title: "Are you sure?",
					text: "You will not be able to recover this file!",
					type: "warning",
					showCancelButton: true,
					confirmButtonColor: "#DD6B55",
					confirmButtonText: "Delete ! ",
					cancelButtonText: "Cancel !",
					closeOnConfirm: false,
					closeOnCancel: true
				},
				function (isConfirm) {
					if (isConfirm) {
						window.location.href = "index.php?action=delete&id="+num;
					} else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
	</html>