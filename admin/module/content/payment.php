<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_content_payment.php");
$curpage="content";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['content'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Content Management 
									<small>Manage / editing your content website here</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li class="active">
									Content Management
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Payment Method</span></h4>
								</div>
								<form name="updateContent" action="payment.php?action=update" enctype="multipart/form-data" method="post" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left">Payment Info</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<textarea name="content" class="tinymce form-control" rows="5"><?=$J_content;?></textarea>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													<a href="payment.php" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<script src="<?=$global['absolute-url-admin'];?>packages/tinymce/tinymce.min.js"></script>
	<script>
        tinymce.init({
            selector: ".tinymce",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
            relative_urls: false,
            forced_root_block : false,
            height : 200
        });
    </script>
	<script type="text/javascript">
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>
		</script>
	</body>
	<!-- end: BODY -->
	</html>