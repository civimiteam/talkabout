<?php
$file_json = $global['root-url']."uploads/json/content_about_us.json";
if(!isset($_GET['action'])){
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_brief = $json['brief'];
    // $J_visi = $json['visi'];
    $J_img = $json['img'];
    $J_img_thmb = $json['img_thmb'];

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']); 
    } else {
        $alert = "";
    }
} else if(isset($_GET['action'])){
    if($_GET['action'] == "update"){
        $N_brief = $_POST['brief'];
        // $N_visi = $_POST['visi'];
        $O_img = $_POST['old_img'];
        $O_imgThmb = $_POST['old_img_thmb'];
        $imageName = $_FILES["image"]["name"];

        if($imageName != null && $imageName !=''){

            $ran = rand();
            $timestamp = time();
            $Photo_ImgLink = "uploads/about/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "uploads/about-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);

            if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
                require_once("../../packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0) {
                            //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                } else {
                    $file = $_FILES["image"]["tmp_name"];
                    //Resizing images
                    if (true !== ($pic_error = @resampimage(1300, 600, "$file", "../../../$Photo_ImgLink", 0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }

                    if (true !== ($pic_error1 = @resampimagethmb("$file", "../../../$ThmbPhoto_ImgLink", 0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $data['img'] = $Photo_ImgLink;
                    $data['img_thmb'] = $ThmbPhoto_ImgLink;
                }
            } else if ($_FILES["image"]["name"] != "") {
                $message .= "Something is incorrent with photo you upload, please make sure that the format is (jpg, png, gif) and size less than 9.5MB.<br /";
            }

        }
        else if($imageName == null || $imageName == ''){
        // create Record attribute node
            $data['img'] = $O_img;
            $data['img_thmb'] = $O_imgThmb;
        }

        
        $data['brief'] = $N_brief;
        // $data['visi'] = $N_visi;
        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Content About Us has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Content About Us failed to update!";
            $_SESSION['alert'] = "error";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
    }
}
?> 