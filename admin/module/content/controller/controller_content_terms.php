<?php
$file_json = $global['root-url']."uploads/json/content_terms.json";

if(!isset($_GET['action'])){
    $J_content = "";
    $json = "";

    if(file_exists($file_json)){
        $json = json_decode(file_get_contents($file_json),TRUE);
        if(is_array($json)){
            $J_content = $json['content'];
        }
    }else{
        file_put_contents($file_json, '');
    }

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']); 
    } else {
        $alert = "";
    }
    
} else if(isset($_GET['action'])){

    if($_GET['action'] == "update"){
        $N_content = $_POST['content'];
        $data['content'] = $N_content;

        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        
        if($result == 1){
            $message = "Content Terms Condition has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Content Terms Condition failed to update!";
            $_SESSION['alert'] = "error";
        }
       
        $_SESSION['status'] = $message;
        header("Location:terms.php");
    }
}
?> 