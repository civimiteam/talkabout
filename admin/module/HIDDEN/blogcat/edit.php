<?php 
	include("../../packages/require.php");
	include("../../packages/categoryFunction.php");//RECURSIVE CATEGORY
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_blog_category_detail.php");
	$curpage="blogcat";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['blogcat'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Blog Category Management 
									<small>Editing your blog category product here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li><a href="<?=$path['blogcat'];?>">Blog Category Management</a></li>
								<!-- BREADCRUMB RECURSIVE !-->
								<li class="active"><?=$datas[0]['blogcat_name'];?></li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Category Name <span class="text-bold">(<?=$datas[0]['blogcat_name'];?>)</span></h4>
								</div>
								<form name="editCategory" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Category Name <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-title" name="name" type="text" class="form-control" placeholder="category name" value="<?=$datas[0]['blogcat_name'];?>" />
													<div id="error-title" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Sort # <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-sort" name="sort" type="number" class="form-control" placeholder="#" value="<?=$datas[0]['blogcat_sort'];?>" />
													<div id="error-sort" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Description :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<textarea name="desc" class="form-control" cols="5" rows="5" placeholder="description about category"><?=correctDisplay($datas[0]['blogcat_desc']);?></textarea>
												</div>
											</div>
											<div class="row">       
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Image Upload <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<a class="fancybox" href="<?=$global['absolute-url'].$datas[0]['blogcat_img'];?>">
														<img style="width: 40px;" class="img-circle" src="<?=$global['absolute-url'].$datas[0]['blogcat_imgThmb'];?>">
													</a>
													<input name="image" type="file" class="text up05" maxlength="256" placeholder="homepage image" />
													<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> <?=$infoSize['blogcat'];?></span></small>
												</div>
											</div>
	                                        <div class="row">       
	                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong> :</div>
	                                            <div class="col-sm-6 col-xs-12 up1">
	                                                <select id="input-publish" name="publish" class="form-control" >
	                                                    <option <?php if($datas[0]['blogcat_publish'] == "Publish"){echo "selected";}?> value="Publish">Publish</option>
	                                                    <option <?php if($datas[0]['blogcat_publish'] == "Not Publish"){echo "selected";}?> value="Not Publish">Not Publish</option>
	                                                </select>
	                                                <div id="error-publish" class="is-error"></div>
	                                            </div>
	                                        </div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['blogcat_ID'];?>">
											<div class="col-sm-2 col-xs-12">
												<div class="link-delete">
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['blogcat_ID'];?>', '<?=$datas[0]['blogcat_name'];?>');">
														Delete
													</a>
												</div>
											</div>
											<div class="col-sm-8 col-xs-12 text-right">
												<div class="btn-group hidden-xs">
													<a href="<?=$path['blogcat'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
												<div class="visible-xs text-right">
													<div class="btn-group">
														<a href="<?=$path['blogcat'];?>" type="reset" class="btn btn-default">
															<i class="fa fa-times"></i> Cancel
														</a>
														<button type="submit" class="btn btn-success">
															<i class="fa fa-check fa fa-white"></i> Update
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">
		function validateForm(){
            var title = $("#input-title").val();
            var sort = $("#input-sort").val();
            var numFormat = /^[0-9]+$/;
            
            if(title != ""){
                $("#error-title").html("");
                $("#error-title").hide();
                $("#input-title").removeClass("input-error");
            } else {
                $("#error-title").show();
                $("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-title").addClass("input-error");
                return false;
            }
            if(sort != ""){
                if(sort.match(numFormat)){
                    $("#error-sort").html("");
                    $("#error-sort").hide();
                    $("#input-sort").removeClass("input-error");
                } else {
                    $("#error-sort").show();
                    $("#error-sort").html("<i class='fa fa-warning'></i> This field must contain number only.");
                    $("#input-sort").addClass("input-error");
                    return false;
                }
            } else {
                $("#error-sort").show();
                $("#error-sort").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-sort").addClass("input-error");
                return false;
            }
        }

		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0
			});
		});
		
		//use session here for alert success/failed
		var alertText = "success/error text"; //teks for alert
		//success alert
		//successAlert(alertText); 
		//error alert
		//errorAlert(alertText); 
		//function confirmation delete
		function confirmDelete(num, text){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num+"&title="+text;
				} else {
	                   //nothing
	               }
	           });
		}
	</script>
</body>
<!-- end: BODY -->
</html>