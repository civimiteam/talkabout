<?php 

include("../../packages/require.php");

include("../../controller/controller_order_edit.php");

include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php



$curpage="order";

?>

<!DOCTYPE html>

<html lang="en">

<!-- start: HEAD -->

<head>

	<title><?=$title['order'];?></title>

	<?php include("../../packages/module-head.php");?>

	<!-- Add fancyBox -->

	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />

</head>

<!-- end: HEAD -->

<!-- start: BODY --> 

<body>

	<!-- start: SLIDING BAR (SB) -->

	<?php include("../../parts/part-sliding_bar.php");?>

	<!-- end: SLIDING BAR -->



	<div class="main-wrapper">



		<!-- start: TOPBAR -->

		<?php include("../../parts/part-top_bar.php");?>

		<!-- end: TOPBAR -->



		<!-- start: PAGESLIDE LEFT -->

		<?php include("../../parts/part-pageslide_left.php");?>

		<!-- end: PAGESLIDE LEFT -->



		<!-- start: PAGESLIDE RIGHT -->

		<?php include("../../parts/part-pageslide_right.php");?>

		<!-- end: PAGESLIDE RIGHT -->



		<!-- start: MAIN CONTAINER -->

		<div class="main-container inner">

			<!-- start: PAGE -->

			<div class="main-content">



				<div class="container">



					<!-- start: PAGE HEADER -->

					<div class="toolbar row">

						<div class="col-sm-6">

							<div class="page-header">

								<h1>

									Order History Management 

									<small>Editing your customer order here.</small>

								</h1>

							</div>

						</div>

						<div class="col-sm-6 col-xs-12"></div>

					</div>

					<!-- end: PAGE HEADER -->



					<!-- start: BREADCRUMB -->

					<div class="row">

						<div class="col-md-12">

							<ol class="breadcrumb">

								<li>

									<a href="<?=$path['order'];?>">

										Order History Management

									</a>

								</li>

								<li class="active">

									<?=$datas[0]['customer_fname'];?>'s PO #<?=$O_id;?> Detail

								</li>

							</ol>

						</div>

					</div>

					<!-- end: BREADCRUMB -->



					<!-- start: PAGE CONTENT -->

					<?php if(is_array($datas)){?>

					<div class="row">

						<div class="col-md-12">

							<div class="panel panel-white">

								<div class="panel-heading border-light">

									<div class="row">

										<div class="col-sm-3 col-xs-12 hidden-xs">

											<a href="<?=$path['order'];?>" type="reset" class="btn btn-default">

												<i class="fa fa-arrow-circle-left"></i> Back

											</a>

										</div>

										<div class="col-sm-6 col-xs-12 text-center">

											<h4 class="panel-title bold"><?=$datas[0]['customer_fname'];?>'s Order History Detail</h3>

										</div>

									</div>

								</div>

								<form name="editCustomer" action="edit.php?action=edit" enctype="multipart/form-data" method="post" >

									<div class="panel-body">

										<div class="form-body">

											<div class="row up2">

						                        <div class="col-xs-12 text-center">

						                            <h4 class="hidden-xs"><span class="label label-danger"><?=$datas[0]['po_name'];?></span>, Date <span class="label label-inverse"><?=$datas[0]['po_date'];?></span></h4>

						                            <h5 class="visible-xs"><span class="label label-danger"><?=$datas[0]['po_name'];?></span>, Date <span class="label label-inverse"><?=$datas[0]['po_date'];?></span></h5>

						                        </div>

						                    </div>



						                    <div class="row up2">

                            					<div class="col-xs-12">

                            						<h5 class="up15 bold">Status: <?=convert_status($datas[0]['po_status']);?>  </h5>      

                                					<?php if($datas[0]['po_resi'] != null && $datas[0]['po_resi'] != "" && $datas[0]['po_resi'] != "0") { ?>

                                					<h5 class="up15 bold">

                                                    	No Resi: <span class='label label-inverse'><?=$datas[0]['po_resi'];?></span>

                                                    </h5>

                                                    <?php } ?>

                                                    <h5 class="up15 bold">

                                                    	Payment Status: 

                                                    	<?php 

														if($datas[0]['po_isPaid'] == 0){

															echo "<span class='label label-danger'>NOT PAID</span>";

														} else if($datas[0]['po_isPaid'] == 1){

															echo "<span class='label label-success'>PAID</span>";

														} 

													?>

                                                    </h5>

                                                </div>

                    						</div>



                    						<div class="row up3">

                    							<div class="col-sm-6 col-xs-12">

                    								<div class="thumbnail">

	                    								<div class="well bold">BILLING INFO</div>

	                    								<div style="padding: 0 20px 20px 20px;">

		                    								<div><span class="bold">Full Name</span> : <?=$datas[0]['customer_fname'];?></div>

		                    								<div class="up1"><span class="bold">Phone</span> : <?=$datas[0]['customer_phone'];?></div>

		                    								<div class="up1"><span class="bold">E-Mail</span> : <?=$datas[0]['customer_email'];?></div>

	                    								</div>

                    								</div>

                    							</div>

                    							<div class="col-sm-6 col-xs-12">

                    								<div class="thumbnail">

	                    								<div class="well bold">SHIP TO</div>

	                    								<address style="padding: 0 20px 20px 20px;margin: 0;">

	                                                        <?=$datas[0]['po_addressStreet1'];?><br>

	                                                        <?php if($datas[0]['po_addressStreet2'] != ""){echo $datas[0]['po_addressStreet2']."<br>";}?>

	                                                        <?=$datas[0]['po_addressCity'].". ".$datas[0]['po_addressState'].".";?><br>

	                                                        <?=$datas[0]['po_addressCountry'].". ".$datas[0]['po_addressZip'].".";?>

	                                                  	</address>

                                                  	</div>

                    							</div>

                    						</div>

                    						

                    						<?php if($datas[0]['counter_product'] != 0){ ?>

											<div style="text-align:right;margin: 30px 0 10px 0;">

												Total Product : <span class="label label-info"><?=$datas[0]['counter_product'];?></span>

											</div>

											<div class="table-responsive">

												<table class="table table-striped table-bordered">

												  	<tr>

												  		<th>#</th>

												  		<th>Product ID</th>

												  		<th>Product Name</th>

												  		<th>QTY</th>

												  		<th>Price</th>

												  		<th>Line Total</th>

												  	</tr>

												  	<?php $total_weight="";$num=1; for($i=0; $i<$datas[0]['counter_product']; $i++){?>

												  	<tr class="info">

												  		<td><?=$num;?></td>

												  		<td>#<?=$datas[0][$i]['atpp_productID'];?></td>

												  		<td><?=$datas[0][$i]['atpp_productName'];?></td>

												  		<td><?=$datas[0][$i]['atpp_qty'];?></td>

												  		<td>Rp <?=number_format(floatval($datas[0][$i]['atpp_price']),0,',','.');?></td>

												  		<td class="bold">Rp <?=number_format(floatval($datas[0][$i]['atpp_qty'] * $datas[0][$i]['atpp_price']),0,',','.');?></td>

												  		<?php $total_weight += ($datas[0][$i]['product_weight'] * $datas[0][$i]['atpp_qty']);?>

												  	</tr>

												  	<?php $num++;}?>

												  	<tr>

												  		<td colspan="5" class="bold text-right">Sub Amount</td>

												  		<td class="bold">Rp <?=number_format(floatval($datas[0]['po_amount']),0,',','.');?></td>

												  	</tr>

												  	<tr>

												  		<td colspan="5" class="bold text-right">Shipping <?php if($total_weight > 0){echo "(".$total_weight." Kg)";}?></td>

												  		<td class="bold">Rp <?=number_format(floatval($datas[0]['po_shippingCost']),0,',','.');?></td>

												  	</tr>

												  	<tr class="success">

												  		<td colspan="5" class="bold text-right">Total Amount</td>

												  		<td class="bold">Rp <?=number_format(floatval($datas[0]['po_totalAmount']),0,',','.');?></td>

												  	</tr>

												  	<?php if($datas[0]['po_isPaid'] == 1 && $datas[0]['confirm_amount'] > 0){ ?>

												  	<tr class="warning">

												  		<td colspan="5" class="bold text-right">Amount Paid</td>

												  		<td class="bold">Rp <?=number_format(floatval($datas[0]['confirm_amount']),0,',','.');?></td>

												  	</tr>

												  	<?php } ?>

												</table>

												<?php if($datas[0]['po_note']!= null){?>

							                    <div class="row up2">

							                        <div class="col-sm-4 col-xs-12 hidden-xs" align="right"><strong>Note</strong></div>

							                        <div class="col-sm-8 col-xs-12 pad0">

							                            <textarea rows="5" class="form-control" disabled><?php echo $datas[0]['po_note'];?></textarea>

							                        </div>

							                    </div>

							                    <?php } ?>

											</div>

											<?php } ?>

										</div>

									</div>

									<div class="panel-footer up3">

										<div class="row">

											<input type="hidden" id="po_id" name="PO_ID" value="<?=$datas[0]['po_ID'];?>">

											<input type="hidden" id="po_name" name="pO_name" value="<?=$datas[0]['po_name'];?>">

											<input type="hidden" id="po_paid" name="pO_isPaid" value="<?=$datas[0]['po_isPaid'];?>">

											<input type="hidden" id="customer_id" name="customer_ID" value="<?=$datas[0]['customer_ID'];?>">

											<div class="col-xs-12 pad0 text-right">

												<div class="btn-group hidden-xs">

													<a href="#panel-note" class="btn btn-default" data-toggle="modal" data-target="#panel-note">

														<?php if($datas[0]['po_note'] == null){ ?>

														<i class="fa fa-plus"></i> Add Note

														<?php } else { ?>

														<i class="fa fa-edit"></i> Edit Note

														<?php } ?>

													</a>

													<!--jika po_status = 5 /completed maka button PAID-NOT PAID akan di hidden -->

													<?php if($datas[0]['po_status'] != 5 ){ ?>

														<?php if($datas[0]['po_isPaid'] == 1){ ?>

														<a href="javascript:void(0)" class="btn btn-warning" onclick="setNotPaid();">

															<i class="fa fa-thumbs-o-down fa fa-white"></i> Set as Not PAID

														</a>

														<?php } else { ?>

														<a href="javascript:void(0)" class="btn btn-warning" onclick="setPaid();">

															<i class="fa fa-thumbs-o-up fa fa-white"></i> Set as PAID

														</a>

														<?php } } ?>



													<!--Cancel order hanya bisa dilakukan pada status awaiting payment (1) / verify payment(2) -->

													<?php if($datas[0]['po_status'] == 2 or $datas[0]['po_status'] == 1 ){ ?>

													<a href="javascript:void(0)" class="btn btn-danger" onclick="cancelOrder();">

														<i class="fa fa-remove fa fa-white"></i> Cancel Order

													</a>

													<?php } ?>

													<!--Complete order hanya bisa dilakukan pada status shipped (4) -->

													<?php if($datas[0]['po_status'] == 4 ){ ?>

													<a href="#panel-resi" class="btn btn-success" data-toggle="modal" data-target="#panel-resi" data-custEmail="<?=$datas[0]['customer_email'];?>" data-custName="<?=$datas[0]['customer_fname'];?>" data-poName="<?=$datas[0]['po_name'];?>" data-poid="<?=$datas[0]['po_ID'];?>" data-poresi="<?=$datas[0]['po_resi'];?>" onclick="transferPOid(this);">

														<i class="fa fa-check fa fa-white"></i> Complete

													</a>

													<?php } ?>

													

												</div>

												<div class="btn-group-vertical visible-xs">

													<a href="#panel-note" class="btn btn-default" data-toggle="modal" data-target="#panel-note">

														<?php if($datas[0]['po_note'] == null){ ?>

														<i class="fa fa-plus"></i> Add Note

														<?php } else { ?>

														<i class="fa fa-edit"></i> Edit Note

														<?php } ?>

													</a>



													<!--jika po_status = 5 /completed maka button PAID-NOT PAID akan di hidden -->

													<?php if($datas[0]['po_status'] != 5 ){ ?> 

														<?php if($datas[0]['po_isPaid'] == 1){ ?>

														<a href="javascript:void(0)" class="btn btn-warning" onclick="setNotPaid();">

															<i class="fa fa-thumbs-o-down fa fa-white"></i> Set as Not PAID

														</a>

														<?php } else { ?>

														<a href="javascript:void(0)" class="btn btn-warning" onclick="setPaid();">

															<i class="fa fa-thumbs-o-up fa fa-white"></i> Set as PAID

														</a>

														<?php } } ?>

													

													<!--Cancel order hanya bisa dilakukan pada status awaiting payment (1) / verify payment(2) -->

													<?php if($datas[0]['po_status'] == 2 or $datas[0]['po_status'] == 1){ ?>

													<a href="javascript:void(0)" class="btn btn-danger" onclick="cancelOrder();">

														<i class="fa fa-remove fa fa-white"></i> Cancel Order

													</a>

													<?php } ?>

													<!--Complete order hanya bisa dilakukan pada status shipped (4) -->

													<?php if($datas[0]['po_status'] == 4 ){ ?>

													<a href="#panel-resi" class="btn btn-success" data-toggle="modal" data-target="#panel-resi">

														<i class="fa fa-check fa fa-white"></i> Complete

													</a>

													<?php } ?>

												</div>

											</div>

										</div>

									</div>

								</form>

							</div>

						</div>

					</div>

					<?php }else{?>

					<div class="row">

						<div class="col-md-12">

							<div class="panel panel-white">

								<div class="panel-heading border-light">

									<h4 class="panel-title">There is no data order in this page!</h4>

								</div>

							</div>

						</div>

					</div>

					<?php } ?>

					<!-- end: PAGE CONTENT-->



				</div>

				<div class="subviews">

					<div class="subviews-container"></div>

				</div>



			</div>

			<!-- end: PAGE -->



			<!-- start: PANEL RESI MODAL FORM -->

				<div class="modal fade" id="panel-resi" tabindex="-1" role="dialog" aria-hidden="true">

					<div class="modal-dialog">

						<div class="modal-content">

							<div class="modal-header">

								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">

									&times;

								</button>

								<h4 class="modal-title">Verifikasi Resi</h4>

							</div>

							<form name="addResi" action="edit.php?action=resi" enctype="multipart/form-data" method="post" onsubmit="return validateResi();" >

								<div class="modal-body">

									<div class="row">

										<div class="col-sm-4 col-xs-12 up1"><strong>No Resi / Connote  <span class="symbol required"></span></strong> :</div>

										<div class="col-sm-8 col-xs-12 up1">

											<input id="PO_resi_modal" type="text" name="PO_resi" class="form-control" placeholder="no resi" />

											<div id="error-resi" class="is-error"></div>

											<input type="hidden" name="PO_ID" value="<?=$datas[0]['po_ID'];?>" />

											<input type="hidden" name="PO_name" value="<?=$datas[0]['po_name'];?>" />

											<input type="hidden" name="Cust_name" value="<?=$datas[0]['customer_fname'];?>" />

											<input type="hidden" name="Cust_email" value="<?=$datas[0]['customer_email'];?>" />

										</div>

									</div>

								</div>

								<div class="modal-footer f5-bg">

									<div class="btn-group">

										<button type="reset" class="btn btn-default" data-dismiss="modal">

											<i class="fa fa-times"></i> Cancel

										</button>

										<button type="submit" class="btn btn-success">

											<i class="fa fa-check fa fa-white"></i> Confirm Resi

										</button>

									</div>

								</div>

							</form>

						</div>

						<!-- /.modal-content -->

					</div>

					<!-- /.modal-dialog -->

				</div>

				<!-- /.modal -->

				<!-- end: SPANEL RESI MODAL FORM -->



				<!-- start: PANEL NOTE MODAL FORM -->

				<div class="modal fade" id="panel-note" tabindex="-1" role="dialog" aria-hidden="true">

					<div class="modal-dialog">

						<div class="modal-content">

							<div class="modal-header">

								<button type="button" class="close" data-dismiss="modal" aria-hidden="true">

									&times;

								</button>

								<h4 class="modal-title">Add Note</h4>

							</div>

							<form name="addResi" action="edit.php?action=addNote" enctype="multipart/form-data" method="post" onsubmit="return validateNote();" >

								<div class="modal-body">

									<div class="row">

										<div class="col-sm-4 col-xs-12 up1"><strong>Note  <span class="symbol required"></span></strong> :</div>

										<div class="col-sm-8 col-xs-12 up1">

											<textarea id="input-note" type="text" name="PO_note" rows="5" class="form-control" placeholder="note here.." ><?php echo $datas[0]['po_note'];?></textarea>

											<div id="error-note" class="is-error"></div>

											<input type="hidden" name="PO_ID" value="<?=$datas[0]['po_ID'];?>" />

											<input type="hidden" name="customer_ID" value="<?=$datas[0]['customer_ID'];?>" />

										</div>

									</div>

								</div>

								<div class="modal-footer f5-bg">

									<div class="btn-group">

										<button type="reset" class="btn btn-default" data-dismiss="modal">

											<i class="fa fa-times"></i> Cancel

										</button>

										<button type="submit" class="btn btn-success">

											<i class="fa fa-check fa fa-white"></i> Save Note

										</button>

									</div>

								</div>

							</form>

						</div>

						<!-- /.modal-content -->

					</div>

					<!-- /.modal-dialog -->

				</div>

				<!-- /.modal -->

				<!-- end: SPANEL NOTE MODAL FORM -->

		</div>

		<!-- end: MAIN CONTAINER -->



		<!-- start: FOOTER -->

		<?php include("../../parts/part-footer.php");?>

		<!-- end: FOOTER -->



		<!-- start: SUBVIEW SAMPLE CONTENTS -->

		<?php include("../../parts/part-sample_content.php");?>

		<!-- end: SUBVIEW SAMPLE CONTENTS -->



	</div>



	<?php include("../../packages/footer-js.php");?>

	<!-- Add fancyBox -->

	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>

	<script type="text/javascript">



		function transferPOid(x){

			var currentPOid = $(x).attr("data-poid");

			var currentPOresi = $(x).attr("data-poresi");

			var currentPOName = $(x).attr("data-poName");

			var currentCustName = $(x).attr("data-custName");

			var currentCustEmail = $(x).attr("data-custEmail");

			$('#PO_ID_modal').val(currentPOid);

			$('#PO_resi_modal').val(currentPOresi);

			$('#PO_name_modal').val(currentPOName);

			$('#Cust_name_modal').val(currentCustName);

			$('#Cust_email_modal').val(currentCustEmail);			

			

				

			<?php if($datas[0]['po_status'] == 3 or $datas[0]['po_status'] == 4 ) {	?>		

			$('#PO_resi_modal').attr("disabled", false);

			$('#btn-submit').attr("disabled", false);

			<?php } else { ?>

			$('#PO_resi_modal').attr("disabled", true);

			<?php } ?>			     

		}



		function validateResi(){

                var resi = $("#PO_resi_modal").val();

                

                if(resi != ""){

                    $("#error-resi").html("");

                    $("#error-resi").hide();

                    $("#PO_resi_modal").removeClass("input-error");

                } else {

                    $("#error-resi").show();

                    $("#error-resi").html("<i class='fa fa-warning'></i> This field is required.");

                    $("#PO_resi_modal").addClass("input-error");

                    return false;

                }

            }

        function validateNote(){

                var note = $("#input-note").val();

                

                if(note != ""){

                    $("#error-note").html("");

                    $("#error-note").hide();

                    $("#input-note").removeClass("input-error");

                } else {

                    $("#error-note").show();

                    $("#error-note").html("<i class='fa fa-warning'></i> This field is required.");

                    $("#input-note").addClass("input-error");

                    return false;

                }

            }

		//function confirmation cancel order

		function cancelOrder(){

			var po_id = $("#po_id").val();

			var po_name = $("#po_name").val();

			swal({

				title: "Are you sure?",

				text: "you want to cancel order "+po_name+"",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Yes, Cancel Order ! ",

				cancelButtonText: "Cancel !",

				closeOnConfirm: false,

				closeOnCancel: true

			},

			function (isConfirm) {

				if (isConfirm) {

					window.location.href = "edit.php?action=cancel&po_id="+po_id;

				} else {

	                   //nothing

	               }

	           });

		}

		//function confirmation set as paid

		function setPaid(){

			var po_id = $("#po_id").val();

			var po_name = $("#po_name").val();

			var po_paid = $("#po_paid").val();

			var cust_id = $("#customer_id").val();

			swal({

				title: "Are you sure ?",

				text: "you want to change the payment status for the order "+po_name+" as PAID",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#ec971f",

				confirmButtonText: "Yes, Set as Paid ! ",

				cancelButtonText: "Cancel !",

				closeOnConfirm: false,

				closeOnCancel: true

			},

			function (isConfirm) {

				if (isConfirm) {

					window.location.href = "edit.php?action=paid&po_id="+po_id+"&po_isPaid="+po_paid+"&customer_id="+cust_id;

				} else {

	                   //nothing

	               }

	           });

		}

		//function confirmation set as paid

		function setNotPaid(){

			var po_id = $("#po_id").val();

			var po_name = $("#po_name").val();

			var po_paid = $("#po_paid").val();

			var cust_id = $("#customer_id").val();

			swal({

				title: "Are you sure ? ",

				text: "you want to change the payment status for the order "+po_name+" as NOT PAID",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#ec971f",

				confirmButtonText: "Yes, Set as NOT Paid ! ",

				cancelButtonText: "Cancel !",

				closeOnConfirm: false,

				closeOnCancel: true

			},

			function (isConfirm) {

				if (isConfirm) {

					window.location.href = "edit.php?action=paid&po_id="+po_id+"&po_isPaid="+po_paid+"&customer_id="+cust_id;

				} else {

	                   //nothing

	               }

	           });

		}

		$(document).ready(function() {

			$(".fancybox").fancybox({

				padding : 0

			});

		});

		<?php if($message != "") { ?>

            //use session here for alert success/failed

            var alertText = "<?=$message;?>"; //teks for alert

            

                <?php if($alert != "success"){ ?>

                    //error alert

                    errorAlert(alertText);

                <?php } else { ?>

                    //success alert

                    successAlert(alertText); 

                <?php } ?>

             

            <?php } ?> 



		//function confirmation delete

		function confirmDelete(num){

			swal({

				title: "Are you sure?",

				text: "You will not be able to recover this file!",

				type: "warning",

				showCancelButton: true,

				confirmButtonColor: "#DD6B55",

				confirmButtonText: "Delete ! ",

				cancelButtonText: "Cancel !",

				closeOnConfirm: false,

				closeOnCancel: true

			},

			function (isConfirm) {

				if (isConfirm) {

					window.location.href = "index.php?action=delete&id="+num;

				} else {

	                   //nothing

	               }

	           });

		}

		</script>



	</body>

	<!-- end: BODY -->

	</html>