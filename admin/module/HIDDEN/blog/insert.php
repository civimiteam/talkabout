<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_blog.php");
	$curpage="blog";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['blog'];?></title>
		<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
 		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Blog News Management 
										<small>Adding and Editing your blog news here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="<?=$path['blog'];?>">
											Blog News Management
										</a>
									</li>
									<li class="active">
										Add New Blog News
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-left">
										<h4 class="modal-title form-title">Add More Blog News</h4>
									</div>
									<div class="panel-body">
										<form name="addBlog" action="insert.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
											<div class="panel-body">
												<div class="form-body">
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Category <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<select id="input-category" name="category" class="form-control">
																<option value="">Choose Category</option>
																<?php if(is_array($data_categorys)){ foreach($data_categorys as $data){ ?>
																<option value="<?=$data['blogcat_ID'];?>"><?=$data['blogcat_name'];?></option>
																<?php } } ?>
															</select>
															<div id="error-category" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Title <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-title" name="title" type="text" class="form-control" placeholder="blog title"/>
                                                			<div id="error-title" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Content <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-8 col-xs-12 up1">
															<textarea id="input-desc" name="desc" class="tinymce form-control" rows="5" placeholder="blog content"></textarea>
                                                			<div id="error-desc" class="is-error"></div>
														</div>
													</div>
													<div class="row">       
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Image <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-photo" name="image" type="file" class="file" style="display: inline-block;" required/>
															<div id="error-photo" class="is-error"></div>
															<small><span class="help-block up1"><i class="fa fa-info-circle"></i> <?php echo $infoSize['blog'];?> </span></small>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label">Author :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-writer" name="writer" type="text" class="form-control" placeholder="blog author"/>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label">Author link :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-writerlink" name="writer_link" type="text" class="form-control" placeholder="blog author link"/>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label">Video url :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-video" name="video" type="text" class="form-control" placeholder="blog video url"/>
														</div>
													</div>
					                                <div class="row">
					                                    <div class="col-sm-3 col-xs-12 up1 form-label">Publish :</div>
					                                    <div class="col-sm-6 col-xs-12 up1">
					                                    	<select name="publish" class="form-control">
					                                    		<option value="Publish">Publish</option>
					                                    		<option value="Not Publish">Not Publish</option>
					                                    	</select>
					                                    </div>
					                                </div>
												</div>
											</div>
											<div class="panel-footer">
												<div class="row">
													<div class="col-xs-12 pad0 text-right">
														<div class="btn-group text-right">
															<a href="<?=$path['blog'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Create
															</button>
														</div>
													</div>
												</div>
											</div>
										</form>

									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>

		<?php include("../../packages/footer-js.php");?>
		<script src="../../packages/tinymce/tinymce.min.js"></script>
  		<script>
        tinymce.init({
            selector: ".tinymce",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image",
            relative_urls: false,
            forced_root_block : false,
            height : 200
        });
    	</script>
		<script type="text/javascript">
			function validateForm(){
				var category = $("#input-category").val();
                var title = $("#input-title").val();
                var desc = tinyMCE.get('input-desc').getContent();
                
                if(category != ""){
					$("#error-category").html("");
					$("#error-category").hide();
					$("#input-category").removeClass("input-error");
				} else {
					$("#error-category").show();
					$("#error-category").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-category").addClass("input-error");
					return false;
				}
                if(title != ""){
                    $("#error-title").html("");
                    $("#error-title").hide();
                    $("#input-title").removeClass("input-error");
                } else {
                    $("#error-title").show();
                    $("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-title").addClass("input-error");
                    return false;
                }
                if(desc != ""){
                    $("#error-desc").html("");
                    $("#error-desc").hide();
                } else {
                    $("#error-desc").show();
                    $("#error-desc").html("<i class='fa fa-warning'></i> This field is required.");
                    return false;
                }
                
            }

			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>
		</script>
	</body>
	<!-- end: BODY -->
</html>