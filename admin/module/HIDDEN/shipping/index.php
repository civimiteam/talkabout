<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_shipping.php");
	$curpage="shipping";
	$page_name = "index.php";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['shipping'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Shipping Management 
										<small>Control your shipping method and price here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Shipping Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
										<div class="btn-group">
											<a class="btn btn-dark-yellow" href="#panel-setting" data-toggle="modal" data-target="#panel-setting">
												<i class="fa fa-wrench"></i> Settings
											</a>
											<a class="btn btn-light-azure" href="#panel-add" data-toggle="modal" data-target="#panel-add">
												<i class="fa fa-plus"></i> Add New Data
											</a>
										</div>
									</div>
									<div class="panel-body">
										<div style="text-align:right;margin: 10px 0;">
											Total Shipping City : <span class="label label-info"><?=$total_data;?></span>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th>#</th>
											  		<th>City</th>
											  		<th><?=$O_shippingName1;?></th>
											  		<?php if($O_shippingName2 != null){?>
											  		<th><?=$O_shippingName2;?></th>
											  		<?php } ?>
											  		<?php if($O_shippingName3 != null){?>
											  		<th><?=$O_shippingName3;?></th>
											  		<?php } ?>
											  		<?php if($O_shippingName4 != null){?>
											  		<th><?=$O_shippingName4;?></th>
											  		<?php } ?>
											  		<th>Action</th>
											  	</tr>
											  	<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											  	<tr>
											  		<td><?=($O_page-1)*30+$num;?></td>
											  		<td><?=$data['shipping_city'];?></td>
											  		<td><?=$data['shipping_cost_1'];?></td>
											  		<?php if($O_shippingName2 != null){?>
											  		<td><?=$data['shipping_cost_2'];?></td>
											  		<?php } ?>
											  		<?php if($O_shippingName3 != null){?>
											  		<td><?=$data['shipping_cost_3'];?></td>
											  		<?php } ?>
											  		<?php if($O_shippingName4 != null){?>
											  		<td><?=$data['shipping_cost_4'];?></td>
											  		<?php } ?>
											  		<td>
											  			<div class="text-center">
															<a href="<?=$path['shipping-edit'].$data['shipping_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
															<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['shipping_ID'];?>');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
														</div>
											  		</td>
											  	</tr>
											  	<?php $num++;} } else { ?>
											  	<tr class="warning">
											  		<td colspan="7" class="bold">There is no data right now!</td>
											  	</tr>
											  	<?php } ?>
											</table>
										</div>
									<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatch($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(5*($batch-1));
												$nextLimit = 5 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if($O_page > 1){
													echo "<li><a href='".$page_name."?page=".($O_page-1)."'><i class='fa fa-chevron-left'></i> Prev</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ 
												?>
												<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
												<?php 
												}
												if($total_page > 1 && $O_page != $total_page){
													echo "<li><a href='".$page_name."?page=".($O_page+1)."'>Next <i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
									<!-- end pagination -->
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Add New Shipping</h4>
								</div>
								<form name="addShipping" action="index.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>City <span class="symbol required"></span></strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-city" name="Shipping_city" type="text" class="form-control" placeholder="shipping image title" />
		                                    	<div id="error-city" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName1;?> <span class="symbol required"></span></strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-cost1" name="Shipping_cost_1" type="text" class="form-control" placeholder="ongkir <?=$O_shippingName1;?>" />
		                                    	<div id="error-cost1" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <?php if($O_shippingName2 != null){?>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName2;?> <span class="symbol required"></span></strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-cost2" name="Shipping_cost_2" type="text" class="form-control" placeholder="ongkir <?=$O_shippingName2;?>" />
		                                    	<div id="error-cost2" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <?php } else { ?>
		                                <input name="Shipping_cost_2" type="hidden" value="0" class="form-control" />
		                                <?php } ?>
										<?php if($O_shippingName3 != null){?>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName3;?> <span class="symbol required"></span></strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-cost3" name="Shipping_cost_3" type="text" class="form-control"  placeholder="ongkir <?=$O_shippingName3;?>" />
		                                    	<div id="error-cost3" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <?php } else { ?>
		                                <input name="Shipping_cost_3" type="hidden" value="0" class="form-control" />
		                                <?php } ?>
										<?php if($O_shippingName4 != null){?>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName4;?> <span class="symbol required"></span></strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-cost4" name="Shipping_cost_4" type="text" class="form-control" placeholder="ongkir <?=$O_shippingName4;?>" />
		                                    	<div id="error-cost4" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <?php } else { ?>
		                                <input name="Shipping_cost_4" type="hidden" value="0" class="form-control" />
		                                <?php } ?>
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: PANEL ADD MODAL FORM -->

					<!-- start: PANEL setting MODAL FORM -->
					<div class="modal fade" id="panel-setting" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Change Shipping Method Names</h4>
								</div>
								<form name="addShipping" action="index.php?action=setName" enctype="multipart/form-data" method="post" onsubmit="return validateMethod();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Shipping Method 1</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-method" name="Shipping_name_1" type="text" class="form-control" value="<?=$O_shippingName1;?>" placeholder="shipping method 1" />
		                                    	<div id="error-method" class="is-error"></div>
		                                    </div>
		                                </div>
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Shipping Method 2</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="Shipping_name_2" type="text" class="form-control" value="<?=$O_shippingName2;?>" placeholder="shipping method 2" />
		                                    </div>
		                                </div>
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Shipping Method 3</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="Shipping_name_3" type="text" class="form-control" value="<?=$O_shippingName3;?>" placeholder="shipping method 3" />
		                                    </div>
		                                </div>
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Shipping Method 4</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="Shipping_name_4" type="text" class="form-control" value="<?=$O_shippingName4;?>" placeholder="shipping method 4" />
		                                    </div>
		                                </div>
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Save
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: PANEL setting MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});
			});
		</script>
		<script type="text/javascript">
			function validateMethod(){
				var method = $("#input-method").val();
                if(method != ""){
                    $("#error-method").html("");
                    $("#error-method").hide();
                    $("#input-method").removeClass("input-error");
                } else {
                    $("#error-method").show();
                    $("#error-method").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-method").addClass("input-error");
                    return false;
                }
			}
			function validateForm(){

                var city = $("#input-city").val();
                if(city != ""){
                    $("#error-city").html("");
                    $("#error-city").hide();
                    $("#input-city").removeClass("input-error");
                } else {
                    $("#error-city").show();
                    $("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-city").addClass("input-error");
                    return false;
                }
                var cost1 = $("#input-cost1").val();
                if(cost1 != ""){
                    $("#error-cost1").html("");
                    $("#error-cost1").hide();
                    $("#input-cost1").removeClass("input-error");
                } else {
                    $("#error-cost1").show();
                    $("#error-cost1").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost1").addClass("input-error");
                    return false;
                }

                <?php if($O_shippingName2 != null){?>
                var cost2 = $("#input-cost2").val();
                if(cost2 != ""){
                    $("#error-cost2").html("");
                    $("#error-cost2").hide();
                    $("#input-cost2").removeClass("input-error");
                } else {
                    $("#error-cost2").show();
                    $("#error-cost2").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost2").addClass("input-error");
                    return false;
                }
                <?php } ?>

                <?php if($O_shippingName3 != null){?>
                var cost3 = $("#input-cost3").val();
                if(cost3 != ""){
                    $("#error-cost3").html("");
                    $("#error-cost3").hide();
                    $("#input-cost3").removeClass("input-error");
                } else {
                    $("#error-cost3").show();
                    $("#error-cost3").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost3").addClass("input-error");
                    return false;
                }
                <?php } ?>

                <?php if($O_shippingName4 != null){?>
                var cost4 = $("#input-cost4").val();
                if(cost4 != ""){
                    $("#error-cost4").html("");
                    $("#error-cost4").hide();
                    $("#input-cost4").removeClass("input-error");
                } else {
                    $("#error-cost4").show();
                    $("#error-cost4").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost4").addClass("input-error");
                    return false;
                }
                <?php } ?>

            }
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>