<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_shipping_detail.php");
$curpage="shipping";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['shipping'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Shipping Management 
									<small>Control your shipping method and price here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['shipping'];?>">
										Shipping Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['shipping_city'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Shipping <span class="text-bold">(<?=$datas[0]['shipping_city'];?>)</span></h4>
								</div>
								<form name="editShipping" action="edit.php?action=update" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
			                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>City <span class="symbol required"></span></strong> :</div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<input id="input-city" name="Shipping_city" type="text" class="form-control" value="<?=$datas[0]['shipping_city'];?>" placeholder="shipping image title" />
		                                    		<div id="error-city" class="is-error"></div>
			                                    </div>
			                                </div>
			                                <div class="row">
			                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName1;?> <span class="symbol required"></span></strong> :</div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<input id="input-cost1" name="Shipping_cost_1" type="text" class="form-control" value="<?=$datas[0]['shipping_cost_1'];?>" placeholder="ongkir <?=$O_shippingName1;?>" />
		                                    		<div id="error-cost1" class="is-error"></div>
			                                    </div>
			                                </div>
			                                <?php if($O_shippingName2 != null){?>
			                                <div class="row">
			                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName2;?> <span class="symbol required"></span></strong> :</div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<input id="input-cost2" name="Shipping_cost_2" type="text" class="form-control" value="<?=$datas[0]['shipping_cost_2'];?>" placeholder="ongkir <?=$O_shippingName2;?>" />
		                                    		<div id="error-cost2" class="is-error"></div>
			                                    </div>
			                                </div>
			                                <?php } else { ?>
			                                <input name="Shipping_cost_2" type="hidden" value="0" class="form-control" />
			                                <?php } ?>
											<?php if($O_shippingName3 != null){?>
			                                <div class="row">
			                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName3;?> <span class="symbol required"></span></strong> :</div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<input id="input-cost3" name="Shipping_cost_3" type="text" class="form-control" value="<?=$datas[0]['shipping_cost_3'];?>" placeholder="ongkir <?=$O_shippingName3;?>" />
		                                    		<div id="error-cost3" class="is-error"></div>
			                                    </div>
			                                </div>
			                                <?php } else { ?>
			                                <input name="Shipping_cost_3" type="hidden" value="0" class="form-control" />
			                                <?php } ?>
											<?php if($O_shippingName4 != null){?>
			                                <div class="row">
			                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong><?=$O_shippingName4;?> <span class="symbol required"></span></strong> :</div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<input id="input-cost4" name="Shipping_cost_4" type="text" class="form-control" value="<?=$datas[0]['shipping_cost_4'];?>" placeholder="ongkir <?=$O_shippingName4;?>" />
		                                    		<div id="error-cost4" class="is-error"></div>
			                                    </div>
			                                </div>
			                                <?php } else { ?>
			                                <input name="Shipping_cost_4" type="hidden" value="0" class="form-control" />
			                                <?php } ?>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="Shipping_ID" value="<?=$datas[0]['shipping_ID'];?>">
											<div class="col-sm-2 col-xs-12">
												<div class="link-delete">
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['shipping_ID'];?>');">
														Delete
													</a>
												</div>
											</div>
											<div class="col-sm-8 col-xs-12 text-right">
												<div class="btn-group hidden-xs">
													<a href="<?=$path['shipping'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
												<div class="visible-xs text-right">
													<div class="btn-group">
														<a href="<?=$path['shipping'];?>" type="reset" class="btn btn-default">
															<i class="fa fa-times"></i> Cancel
														</a>
														<button type="submit" class="btn btn-success">
															<i class="fa fa-check fa fa-white"></i> Update
														</button>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>
	<script type="text/javascript">
		function validateForm(){

                var city = $("#input-city").val();
                if(city != ""){
                    $("#error-city").html("");
                    $("#error-city").hide();
                    $("#input-city").removeClass("input-error");
                } else {
                    $("#error-city").show();
                    $("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-city").addClass("input-error");
                    return false;
                }
                var cost1 = $("#input-cost1").val();
                if(cost1 != ""){
                    $("#error-cost1").html("");
                    $("#error-cost1").hide();
                    $("#input-cost1").removeClass("input-error");
                } else {
                    $("#error-cost1").show();
                    $("#error-cost1").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost1").addClass("input-error");
                    return false;
                }

                <?php if($O_shippingName2 != null){?>
                var cost2 = $("#input-cost2").val();
                if(cost2 != ""){
                    $("#error-cost2").html("");
                    $("#error-cost2").hide();
                    $("#input-cost2").removeClass("input-error");
                } else {
                    $("#error-cost2").show();
                    $("#error-cost2").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost2").addClass("input-error");
                    return false;
                }
                <?php } ?>

                <?php if($O_shippingName3 != null){?>
                var cost3 = $("#input-cost3").val();
                if(cost3 != ""){
                    $("#error-cost3").html("");
                    $("#error-cost3").hide();
                    $("#input-cost3").removeClass("input-error");
                } else {
                    $("#error-cost3").show();
                    $("#error-cost3").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost3").addClass("input-error");
                    return false;
                }
                <?php } ?>

                <?php if($O_shippingName4 != null){?>
                var cost4 = $("#input-cost4").val();
                if(cost4 != ""){
                    $("#error-cost4").html("");
                    $("#error-cost4").hide();
                    $("#input-cost4").removeClass("input-error");
                } else {
                    $("#error-cost4").show();
                    $("#error-cost4").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-cost4").addClass("input-error");
                    return false;
                }
                <?php } ?>

            }
		//use session here for alert success/failed
		var alertText = "success/error text"; //teks for alert

		//success alert
		//successAlert(alertText); 

		//error alert
		//errorAlert(alertText); 

		//function confirmation delete
		function confirmDelete(num){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num;
				} else {
	                   //nothing
	               }
	           });
		}
		</script>

	</body>
	<!-- end: BODY -->
	</html>