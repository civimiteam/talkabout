<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("../../controller/controller_customer_detail.php");
$curpage="customer";
$page_name = "edit.php";
$customer_page = "id=$O_id";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['customer'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Customer Management 
									<small>Editing your customer order here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['customer'];?>">
										Customer Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['customer_fname'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->

					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Customer <span class="text-bold">(<?=$datas[0]['customer_fname'];?>)</span></h4>
								</div>
								<form name="editCustomer" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Full Name <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-name" name="name" type="text" class="form-control" placeholder="customer name" value="<?=$datas[0]['customer_fname'];?>" />
		                                    		<div id="error-name" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Phone <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-phone" name="phone" type="text" class="form-control" placeholder="customer phone" value="<?=$datas[0]['customer_phone'];?>" />
		                                    		<div id="error-phone" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Fax </div>
												<div class="col-sm-6 col-xs-12 up1">
													<input name="fax" type="text" class="form-control" placeholder="customer fax" value="<?=$datas[0]['customer_fax'];?>" />
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">E-Mail </div>
												<div class="col-sm-6 col-xs-12 up1">
													<input name="email" type="email" class="form-control" placeholder="customer email" value="<?=$datas[0]['customer_email'];?>" disabled/>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Note </div>
												<div class="col-sm-6 col-xs-12 up1">
													<textarea name="note" type="text" rows="3" class="form-control" placeholder="customer note"><?=correctDisplay($datas[0]['customer_note']);?></textarea>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Member Since </div>
												<div class="col-sm-6 col-xs-12 up1">
													<input name="since" type="text" class="form-control" placeholder="customer member since" value="<?php if ($datas[0]['customer_since'] > '00:00:00') {echo date('F j, Y', strtotime($datas[0]['customer_since']));}else{echo "not recorded";}?>" disabled/>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Total Purchase Amount </div>
												<div class="col-sm-6 col-xs-12 up1">
													<input name="total" type="text" class="form-control" placeholder="customer total purchase" value="Rp <?=number_format(floatval($datas[0]['customer_balance']), 0, ',', '.');?>" disabled/>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Last Purchase Date </div>
												<div class="col-sm-6 col-xs-12 up1">
													<input name="last" type="text" class="form-control" placeholder="customer last purchase" value="<?php if ($data_pos[0]['po_date'] > '00:00:00') {echo date('F j, Y', strtotime($data_pos[0]['po_date']));}else{echo "not yet";}?>" disabled/>
												</div>
											</div>

											<div style="text-align: center;padding: 10px 0;border-top: solid 1px #ddd;border-bottom: solid 1px #ddd;margin: 30px 0;">
												<input type="hidden" name="id" value="<?=$datas[0]['customer_ID'];?>">
												<a href="<?=$path['customer-address'].$datas[0]['customer_ID']."&name=".$datas[0]['customer_fname'];?>" class="btn btn-warning">Address Book</a>
												<a href="<?=$path['customer-wishlist'].$datas[0]['customer_ID']."&name=".$datas[0]['customer_fname'];?>" class="btn btn-info">Customer Wishlist</a>
											</div>

											<div class="table-responsive">
												<table class="table table-striped table-bordered">
												  	<tr>
												  		<th>#</th>
												  		<th>PO Name</th>
												  		<th>Order Date</th>
												  		<th>Total Amount</th>
												  		<th>Status</th>
												  		<th>Action</th>
												  	</tr>
												  	<?php if(is_array($data_pos)) { $num=1;foreach($data_pos as $data_po) { ?>
												  	<tr>
												  		<td><?=($O_page-1)*10+$num;?></td>
												  		<td><?=$data_po['po_name'];?></td>
												  		<td><?=date('F j, Y', strtotime($data_po['po_date']));?></td>
												  		<td>Rp <?=number_format(floatval($data_po['po_totalAmount']), 0, '.', ',');?></td>
												  		<td>
												  			<?php if($data_po['po_status'] == "IN PROCESS"){?>
												  				<span class="label label-info"><?=$data_po['po_status'];?></span>
												  			<?php }else{?>
												  				<span class="label label-success"><?=$data_po['po_status'];?></span>
												  			<?php } ?>
												  		</td>
												  		<td>
												  			<div class="text-center">
																<a href="<?=$path['customer-order'].$data_po['po_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="View order"><i class="fa fa-search"></i> View Order</a>
															</div>
												  		</td>
												  	</tr>
												  	<?php $num++;} } else { ?>
												  	<tr class="warning">
												  		<td colspan="6" class="bold">There is no data right now!</td>
												  	</tr>
												  	<?php } ?>
												</table>
											</div>

											<!-- start pagination -->
											<div class="part-pagination">
												<ul class="pagination pagination-blue margin-bottom-10">
						                            <?php
						                            $batch = getBatch($O_page);
						                            if($batch < 1){$batch = 1;}
						                            $prevLimit = 1 +(10*($batch-1));
						                            $nextLimit = 10 * $batch;

						                            if($nextLimit > $total_page){
						                                $nextLimit = $total_page;
						                            }
						                            if ($total_page > 1 && $O_page > 1) {
						                                echo "<li><a href='".$page_name."?page=1&".$customer_page."'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
						                            }
						                            if($batch > 1 && $O_page > 10){
						                                echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$customer_page."><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
						                            }
						                            for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
						                                <li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon."&".$customer_page;?>" ><?php echo $mon;?></a></li>
						                                <?php if($mon == $nextLimit && $mon < $total_page){
						                                    if($mon >= $total_page){ 
						                                        $mon -=1;
						                                    }
						                                    echo "<li><a href='".$page_name."?page=".($mon+1)."&".$customer_page."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
						                                }                               
						                            }
						                            if ($total_page > 1 &&  ($O_page != $total_page)) {
						                                echo "<li><a href='".$page_name."?page=".$total_page."&".$customer_page."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
						                            }
						                            ?>
						                        </ul>
											</div>
											<!-- end pagination -->

										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['customer'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['customer'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Update
															</button>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data customer in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">
		function validateForm(){
				var name = $("#input-name").val();
				var phone = $("#input-phone").val();
				
				if(name != ""){
					$("#error-name").html("");
					$("#error-name").hide();
					$("#input-name").removeClass("input-error");
				} else {
					$("#error-name").show();
					$("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-name").addClass("input-error");
					return false;
				}
				if(phone != ""){
					$("#error-phone").html("");
					$("#error-phone").hide();
					$("#input-phone").removeClass("input-error");
				} else {
					$("#error-phone").show();
					$("#error-phone").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-phone").addClass("input-error");
					return false;
				}
			}
		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0
			});
		});
		
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
			<?php if($alert != "success"){ ?>
				//error alert
				errorAlert(alertText);
			<?php } else { ?>
				//success alert
				successAlert(alertText); 
			<?php } ?>
			 
		<?php } ?>
		
		//function confirmation delete
		function confirmDelete(num){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num;
				} else {
	                   //nothing
	               }
	           });
		}
		</script>

	</body>
	<!-- end: BODY -->
	</html>