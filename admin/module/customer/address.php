<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	require_once("../../../packages/api_config.php");
	include("../../controller/controller_customer_address.php");
	$curpage="customer";
	function get_data_city(){
		$curl = curl_init();
		curl_setopt_array($curl, array(
			CURLOPT_URL => "http://api.rajaongkir.com/starter/city?province=6",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"key: 2a5596f91813755069cbb8e872eb79ca"
			),
		));
			 
		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			echo "cURL Error #:" . $err;
		} else { 
			$data_ongkir = json_decode($response, true);
		}
		for ($i=0; $i < count($data_ongkir['rajaongkir']['results']); $i++) { 
		    echo "<option value='".$data_ongkir['rajaongkir']['results'][$i]['city_id']."'>".ucfirst($data_ongkir['rajaongkir']['results'][$i]['city_name'])."</option>";
		} 
	}
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['customer'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR --> 

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Customer Management 
										<small>Adding and Editing your customer address here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
									<a href="<?=$path['customer'];?>">
											Customer Management
										</a>
									</li>
									<li>
										<a href="<?=$path['customer-edit'].$O_id;?>">
											<?=$O_name;?>
										</a>
									</li>
									<li class="active">
										<?=$O_name."'s Address";?>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light">
										<div class="row">
											<div class="col-sm-6 col-xs-12 hidden-xs">
												<h4 class="panel-title bold"><?=$O_name."' Address";?></h4>
											</div>
											<div class="col-sm-6 col-xs-12 text-right">
												<a class="btn btn-light-azure" href="#panel-add" data-toggle="modal" data-target="#panel-add">
													<i class="fa fa-plus"></i> Add New Address
												</a>
											</div>
										</div>
									</div>
									<div class="panel-body">
										<div class="row">
											<?php if(is_array($datas)) { $num = 1; foreach($datas as $data){?>
											<div class="col-md-4 col-sm-6 col-xs-12 pad0">
												<div class="row">
													<div class="col-xs-2 text-right"><span><?=$num;?>.</span></div>
													<div class="col-xs-10">
														<div class="thumbnail" style="min-height: 200px;"> 
		                    								<address style="padding: 15px;margin: 0;">
		                    									<strong style="text-transform:uppercase;"><?=$data['address_name'];?></strong><br>
		                                                        <?=$data['address_street1'];?><br>
		                                                        <?php if($data['address_street2'] != ""){echo $data['address_street2']."<br>";}?>
		                                                        <?=$data['address_city'].". ".$data['address_state'].".";?><br>
		                                                        <?=$data['address_country'].". ".$data['address_zip'].".";?>                      
		                                                  	</address>
		                                                  	<span class="hide id<?=$num;?>"><?=$data['address_ID'];?></span>
					                                        <span class="hide name<?=$num;?>"><?=$data['address_name'];?></span>
					                                        <span class="hide street1<?=$num;?>"><?=$data['address_street1'];?></span>
					                                        <span class="hide street2<?=$num;?>"><?=$data['address_street2'];?></span>
					                                        <span class="hide cityid<?=$num;?>"><?=$data['address_cityID'];?></span>
					                                        <span class="hide city<?=$num;?>"><?=$data['address_city'];?></span>
					                                        <span class="hide stateid<?=$num;?>"><?=$data['address_stateID'];?></span>
					                                        <span class="hide state<?=$num;?>"><?=$data['address_state'];?></span>
					                                        <span class="hide zip<?=$num;?>"><?=$data['address_zip'];?></span>
					                                        <span class="hide country<?=$num;?>"><?=$data['address_country'];?></span>
		                                                  	<div style="padding: 0 15px 15px 15px;">
		                                                  		<a href="#panel-edit" data-toggle="modal" data-target="#panel-edit" onclick="copyAddress('<?=$num;?>')" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
																<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['address_ID'];?>', '<?=$O_id;?>', '<?=$data['address_street1'];?>', '<?=$O_name;?>' );" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
		                                                  	</div>
	                                                  	</div>
													</div>
												</div>
											</div>
											<?php $num++;} } else { ?>
											<tr class="warning">
												<td colspan="6" class="bold">There is no data right now!</td>
											</tr>
											<?php } ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Add New Address</h4>
								</div>
								<form name="addAddress" action="address.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Address Name<span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-name" name="name" type="text" class="form-control" placeholder="exp: house, office, school, etc" />
		                                    	<div id="error-name" class="is-error"></div>
		                                    </div>
		                                </div>
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Full Address<span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-address1" name="address1" type="text" class="form-control" placeholder="address" />
		                                    	<div id="error-address1" class="is-error"></div>
		                                    </div>
		                                </div>
										<div class="row" style="display:none;">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label">Address Line 2  :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="address2" type="text" class="form-control" placeholder="address line 2 (optional)" />
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>State/Province <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<select id="input-state" name="stateid" class="form-control">
		                                    		<option value="">--Choose Province--</option>
		                                    	</select>
		                                    	<input id="input-statename" name="state" type="hidden" />
		                                    	<div id="error-state" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>City <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<select id="input-city" name="cityid" class="form-control">
		                                    		<option value="">--Choose City--</option>
		                                    	</select>
		                                    	<input id="input-cityname" name="city" type="hidden" />
		                                    	<div id="error-city" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Zip Code <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-zip" name="zip" type="text" class="form-control" placeholder="zip code" />
		                                    	<div id="error-zip" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Country <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="input-country" name="country" type="text" class="form-control disable-state" value="Indonesia" placeholder="country, ex:indonesia" />
		                                    	<div id="error-country" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="cust_id" type="hidden" value="<?=$O_id;?>" />
		                                    	<input type="hidden" name="cust_name" value="<?=$O_name;?>" />
		                                    </div>
		                                </div>
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">

											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL ADD MODAL FORM  -->

					<!-- start: PANEL EDIT MODAL FORM -->
					<div class="modal fade" id="panel-edit" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Edit Address <br/>(<span id="text_address"></span>)</h4>
								</div>
								<form name="addAddress" action="address.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateFormEdit();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Address Name<span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="einput-name" name="Address_name" type="text" class="form-control" placeholder="exp: house, office, school, etc" />
		                                    	<div id="error-ename" class="is-error"></div>
		                                    </div>
		                                </div>
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Full Address <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="einput-address1" name="Address_line1" type="text" class="form-control" placeholder="full address" />
		                                    	<div id="error-eaddress1" class="is-error"></div>
		                                    </div>
		                                </div>
										<div class="row" style="display:none;">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label">Address Line 2  </div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="Address_line2" type="text" class="form-control" placeholder="address line 2 (optional)" />
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>State/Province <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<select id="einput-state" name="Address_stateid" class="form-control">
		                                    		<option value="">--Choose Province--</option>
		                                    	</select>
		                                    	<input id="einput-statename" name="Address_state" type="hidden" />
		                                    	<div id="error-estate" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>City <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<select id="einput-city" name="Address_cityid" class="form-control">
		                                    		<option value="">--Choose City--</option>
		                                    	</select>
		                                    	<input id="einput-cityname" name="Address_city" type="hidden" />
		                                    	<div id="error-ecity" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Zip Code <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="einput-zip" name="Address_zip" type="text" class="form-control" placeholder="zip code" />
		                                    	<div id="error-ezip" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Country <span class="symbol required"></span></strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="einput-country" name="Address_country" type="text" class="form-control disable-state" placeholder="country, ex:indonesia" value="Indonesia" />
		                                    	<div id="error-ecountry" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="Address_id" type="hidden"/>
		                                    	<input name="Address_cust_id" type="hidden" value="<?=$O_id;?>" />
		                                    	<input type="hidden" name="cust_name" value="<?=$O_name;?>" />
		                                    </div>
		                                </div>
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Update
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL EDIT MODAL FORM  -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});
			});
			$("#input-city").on("change", function () {
		        var txt = $(this).find(':checked').text();
		        $("#input-cityname").val(txt);
		    });
			$("#einput-city").on("change", function () {
		        var txt = $(this).find(':checked').text();
		        $("#einput-cityname").val(txt);
		    });
		    $("#input-state").on("change", function () {
		        var txt = $(this).find(':checked').text();
			    var province_id =  $("#input-state").val();
				if(province_id != ""){
					$("#input-city").html("<option value=''>Loading...</option>");
					loadCity(province_id);
			        $("#input-statename").val(txt);
				} else {
					$("#input-city").html("<option value=''>--Choose City--</option>");
				}
		    });
			$("#einput-state").on("change", function () {
		        var txt = $(this).find(':checked').text();
		        $("#einput-statename").val(txt);
		    });
		</script>
		<script type="text/javascript">
			loadProvince();
			function loadProvince(){
				$.ajax({
					url:'<?=$api['ongkir-province'];?>',
					dataType:'json',
					success:function(response){
						$("#input-state").html("");
						$("#einput-state").html("");
						province = '';
						$("#input-state").append("<option value=''>--Choose Province--</option>");
						$("#einput-state").append("<option value=''>--Choose Province--</option>");
						$.each(response['rajaongkir']['results'], function(i,n){
							province = '<option value="'+n['province_id']+'">'+n['province']+'</option>';
							province = province + '';
							$("#input-state").append(province);
							$("#einput-state").append(province);
						});
					},
					error:function(){
						$("#input-state").html('ERROR');
					}
				});
			}
			function loadCity(provinces){
				$.ajax({
					url:'<?=$api['ongkir-city'];?>&province_id='+provinces,
					dataType:'json',
					success:function(response){
						$("#input-city").html("");
						var province = '';
						$("#input-city").append("<option value=''>--Choose City--</option>");
						$.each(response['rajaongkir']['results'], function(i,n){
							province = '<option value="'+n['city_id']+'">'+n['city_name']+'</option>';
							province = province + '';
							$("#input-city").append(province);
						});
					},
					error:function(){
						$("#input-city").html('ERROR');
					}
				});
			}
			function loadCityEdit(provinces,city){
				$.ajax({
					url:'<?=$api['ongkir-city'];?>&province_id='+provinces,
					dataType:'json',
					success:function(response){
						$("#einput-city").html("");
						var province = '';
						$("#einput-city").append("<option value=''>--Choose City--</option>");
						$.each(response['rajaongkir']['results'], function(i,n){
							province = '<option value="'+n['city_id']+'">'+n['city_name']+'</option>';
							province = province + '';
							$("#einput-city").append(province);
						});
		        		$("#einput-city").val(city);
					},
					error:function(){
						$("#einput-city").html('ERROR');
					}
				});
			}
			function validateForm(){
                var name = $("#input-name").val();
                var address1 = $("#input-address1").val();
                var state = $("#input-state").val();
                var city = $("#input-city").val();
                var zip = $("#input-zip").val();
                var country = $("#input-country").val();

                if(name != ""){
                    $("#error-name").html("");
                    $("#error-name").hide();
                    $("#input-name").removeClass("input-error");
                } else {
                    $("#error-name").show();
                    $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-name").addClass("input-error");
                    return false;
                }
                if(address1 != ""){
                    $("#error-address1").html("");
                    $("#error-address1").hide();
                    $("#input-address1").removeClass("input-error");
                } else {
                    $("#error-address1").show();
                    $("#error-address1").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-address1").addClass("input-error");
                    return false;
                }
                if(state != ""){
                    $("#error-state").html("");
                    $("#error-state").hide();
                    $("#input-state").removeClass("input-error");
                } else {
                    $("#error-state").show();
                    $("#error-state").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-state").addClass("input-error");
                    return false;
                }
                if(city != ""){
                    $("#error-city").html("");
                    $("#error-city").hide();
                    $("#input-city").removeClass("input-error");
                } else {
                    $("#error-city").show();
                    $("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-city").addClass("input-error");
                    return false;
                }
                if(zip != ""){
                    $("#error-zip").html("");
                    $("#error-zip").hide();
                    $("#input-zip").removeClass("input-error");
                } else {
                    $("#error-zip").show();
                    $("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-zip").addClass("input-error");
                    return false;
                }
                if(country != ""){
                    $("#error-country").html("");
                    $("#error-country").hide();
                    $("#input-country").removeClass("input-error");
                } else {
                    $("#error-country").show();
                    $("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-country").addClass("input-error");
                    return false;
                }
            }
            function validateFormEdit(){
                var name = $("#einput-name").val();
                var address1 = $("#einput-address1").val();
                var state = $("#einput-state").val();
                var city = $("#einput-city").val();
                var zip = $("#einput-zip").val();
                var country = $("#einput-country").val();

                if(name != ""){
                    $("#error-ename").html("");
                    $("#error-ename").hide();
                    $("#einput-name").removeClass("input-error");
                } else {
                    $("#error-ename").show();
                    $("#error-ename").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#einput-name").addClass("input-error");
                    return false;
                }
                if(address1 != ""){
                    $("#error-eaddress1").html("");
                    $("#error-eaddress1").hide();
                    $("#einput-address1").removeClass("input-error");
                } else {
                    $("#error-eaddress1").show();
                    $("#error-eaddress1").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#einput-address1").addClass("input-error");
                    return false;
                }
                if(state != ""){
                    $("#error-estate").html("");
                    $("#error-estate").hide();
                    $("#einput-state").removeClass("input-error");
                } else {
                    $("#error-estate").show();
                    $("#error-estate").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#einput-state").addClass("input-error");
                    return false;
                }
                if(city != ""){
                    $("#error-ecity").html("");
                    $("#error-ecity").hide();
                    $("#einput-city").removeClass("input-error");
                } else {
                    $("#error-ecity").show();
                    $("#error-ecity").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#einput-city").addClass("input-error");
                    return false;
                }
                if(zip != ""){
                    $("#error-ezip").html("");
                    $("#error-ezip").hide();
                    $("#einput-zip").removeClass("input-error");
                } else {
                    $("#error-zip").show();
                    $("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#einput-zip").addClass("input-error");
                    return false;
                }
                if(country != ""){
                    $("#error-ecountry").html("");
                    $("#error-ecountry").hide();
                    $("#einput-country").removeClass("input-error");
                } else {
                    $("#error-ecountry").show();
                    $("#error-ecountry").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#einput-country").addClass("input-error");
                    return false;
                }
            }
			<?php if($message != "") { ?>
				//use session here for alert success/failed
				var alertText = "<?=$message;?>"; //teks for alert
				
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
				 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num, cust, text, name){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "address.php?action=delete&id="+num+"&cust_id="+cust+"&title="+text+"&name="+name;
	                } else {
	                    //nothing
	                }
	            });
			}

			function copyAddress(para) {
		        var stateid = $('.stateid' + para).text();
		        var state = $('.state' + para).text();
		        var id = $('.id' + para).text();
		        var name = $('.name' + para).text();
		        var street1 = $('.street1' + para).text();
		        var street2 = $('.street2' + para).text();
		        var cityid = $('.cityid' + para).text();
		        var city = $('.city' + para).text();
		        var country = $('.country' + para).text();
		        var zip = $('.zip' + para).text();
		        loadCityEdit(stateid,cityid);

		        $("input[name='Address_id']").val(id);
		        $("input[name='Address_name']").val(name);
		        $("input[name='Address_line1']").val(street1);
		        $("input[name='Address_line2']").val(street2);
		        $("#einput-state").val(stateid);
		        $("#einput-statename").val(state);
		        $("input[name='Address_country']").val(country);
		        $("input[name='Address_zip']").val(zip);
		       	$("#text_address").text(street1)
		        $("#einput-cityname").val(city);
		    }
		</script>
	</body>
	<!-- end: BODY -->
</html>