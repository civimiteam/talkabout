<?php
ob_start("ob_gzhandler");
session_start();
require_once('../../packages/check_input.php');

if($_GET['action'] == 'export'){
    require_once($global['root-url']."model/Connection.php");
    $obj_con = new Connection();

    require_once($global['root-url']."model/Newsletter.php");
    $obj_newsletter = new Newsletter(); 

    $obj_con->up();

    $O_now = nowDate();
    $data_inactives = $obj_newsletter->get_data_all_by_status(0);
    $total_inactive = is_array($data_inactives) ? count($data_inactives) : 0;

    $data_actives = $obj_newsletter->get_data_all_by_status(1);
    $total_active = is_array($data_actives) ? count($data_actives) : 0;

    $total_data = $total_inactive + $total_active;
    header("Content-type: application/octet-stream");
    header("Content-Disposition: attachment; filename=Newsletter-".date("dMY", strtotime($O_now)).".xls");
    header("Pragma: no-cache");
    header("Expires: 0");
    $obj_con->down();
}
?>

<!DOCTYPE html>
<html lang="en">
    <!-- start: HEAD -->
    <head>
    </head>
    <!-- end: HEAD -->
    <!-- start: BODY -->
    <body>
        <div>
            <!-- start: MAIN CONTAINER -->
            <div>
                <!-- start: PAGE -->
                <div>
                    <div>
                        <!-- start: PAGE CONTENT -->
                        <div>
                            <div>
                                <div>
                                    <div>
                                        <div style="text-align: left; margin: 10px 0;">
                                            Total Newsletter Email : <span><?=$total_data;?></span>
                                        </div>

                                        <?php if(is_array($data_inactives)) { ?>
                                        <div style="padding: 10px 0 5px 0; border-top: solid 1px #ddd;">
                                            <div style="text-align: left"><h1>Inactive Subscribers</h1></div>
                                            <div style="text-align: left; margin: 10px 0;">
                                                Total Inactive Subscribers : <span><?=$total_inactive;?></span>
                                            </div>
                                        </div>
                                        <div>
                                            <table border="1">
                                                <tr>
                                                    <th style="width:20%;text-align: center">#</th>
                                                    <th style="width:150%;text-align: left">E-Mail</th>
                                                    <th style="width:125%;text-align: center;">Status</th>
                                                </tr>
                                                <?php $num=1; foreach($data_inactives as $data_inactive) { ?>
                                                <tr>
                                                    <td style="text-align: center;"><?=$num;?>.</td>
                                                    <td><?=$data_inactive['mail_email'];?></td>
                                                    <td style="text-align: center;">Inactive</td>
                                                </tr>
                                                <?php $num++; } ?>
                                            </table>
                                        </div>
                                        <?php } ?>

                                        <?php if(is_array($data_actives)) { ?>
                                        <div style="padding: 10px 0 5px 0; border-top: solid 1px #ddd; margin:30px 0 0 0;">
                                            <div style="text-align: left"><h1>Active Subscribers</h1></div>
                                            <div style="text-align: left; margin: 10px 0;">
                                                Total Active Subscribers : <span><?=$total_active;?></span>
                                            </div>
                                        </div>
                                        <div>
                                            <table border="1">
                                                <tr>
                                                    <th style="width:20%;text-align: center">#</th>
                                                    <th style="width:150%;text-align: left">E-Mail</th>
                                                    <th style="width:125%;text-align: center;">Status</th>
                                                </tr>
                                                <?php $num=1; foreach($data_actives as $data_active) { ?>
                                                <tr>
                                                    <td style="text-align: center;"><?=$num;?>.</td>
                                                    <td><?=$data_active['mail_email'];?></td>
                                                    <td style="text-align: center;">Active</td>
                                                </tr>
                                                <?php $num++; } ?>
                                            </table>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- end: PAGE CONTENT-->
                    </div>
                </div>
                <!-- end: PAGE -->
            </div>
            <!-- end: MAIN CONTAINER -->
        </div>
    </body>
    <!-- end: BODY -->
</html>