<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_newsletter_inactive.php");
	$curpage="newsletter";
	$page_name = "inactive.php";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['newsletter'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->
			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->
			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">
						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Newsletter Management 
										<small>View and check subscriber newsletter here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="<?=$path['newsletter'];?>">
											Newsletter Management
										</a>
									</li>
									<li class="active">
										Inactive Subscribers
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<?php if(is_array($datas)) { ?>
										<div style="padding: 10px 0 5px 0;">
											<div class="text-center"><h4 class="bold">Inactive Subscribers</h4></div>
											<div style="text-align:right;margin: 10px 0;">
												Total Inactive Subscribers : <span class="label label-info"><?=$total_data;?></span>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th>#</th>
											  		<th>E-Mail</th>
											  		<th>Status</th>
											  		<th>Action</th>
											  	</tr>
											  	<?php $num=1; foreach($datas as $data) { ?>
											  	<tr class="warning">
											  		<td><?=($O_page-1)*10+$num;?>.</td>
											  		<td><?=$data['mail_email'];?></td>
											  		<td>INACTIVE</td>
											  		<td><a href="javascript:void(0)" data-status="<?=$data['mail_status'];?>" data-email="<?=$data['mail_email'];?>" data-id="<?=$data['mail_ID'];?>" onclick="confirmActivate(this);" class="btn btn-xs btn-info tooltips" data-placement="top" data-original-title="Deactivate">Activate</a></td>
											  	</tr>
											  	<?php $num++; } ?>
											</table>
										</div>
											<!-- start pagination -->
											<div class="part-pagination part-pagination-customer" style="">
												<ul class="pagination pagination-blue margin-bottom-10">
													<?php
													$batch = getBatch($O_page);
													if($batch < 1){$batch = 1;}
													$prevLimit = 1 +(5*($batch-1));
													$nextLimit = 5 * $batch;
													if($nextLimit > $total_page){
														$nextLimit = $total_page;
													}
													if($O_page > 1){
														echo "<li><a href='".$page_name."?page=".($O_page-1)."'><i class='fa fa-chevron-left'></i> Prev</a></li>";
													}
													for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ 
													?>
													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
													<?php 
													}
													if($total_page > 1 && $O_page != $total_page){
														echo "<li><a href='".$page_name."?page=".($O_page+1)."'>Next <i class='fa fa-chevron-right'></i></a></li>";
													}
													?>
												</ul>
											</div>
											<!-- end pagination -->
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->
			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>
		<?php include("../../packages/footer-js.php");?>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>
			//function newsletter deactivate
			function confirmActivate(x){
				var mail_id = $(x).attr("data-id");
				var mail_email = $(x).attr("data-email");
				var mail_status = $(x).attr("data-status");
				swal({
	                title: "Are you sure?",
	                text: "Are you sure you want to set email: "+mail_email+" as ACTIVE",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#49afcd",
	                confirmButtonText: "Activate ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "inactive.php?action=update&id="+mail_id+"&status="+mail_status;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>