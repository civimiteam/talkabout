<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_newsletter.php");
	$curpage="newsletter";
	$page_name = "index.php";
?>

<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['newsletter'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Newsletter Management 
										<small>View and check subscriber newsletter here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Newsletter Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<div class="panel-heading border-light text-right">
											<a target="_blank" class="btn btn-success" href="<?=$path['newsletter-export_all'];?>">
												<i class="fa fa-download"></i> Export to Excel
											</a>
										</div>
										<div style="text-align:right;margin: 10px 0;">
											Total Newsletter Email : <span class="label label-info"><?=$total_data;?></span>
										</div>

										<?php if(is_array($data_inactives)) { ?>
										<div style="padding: 10px 0 5px 0;border-top: solid 1px #ddd;">
											<div class="text-center"><h4 class="bold">Inactive Subscribers</h4></div>
											<div style="text-align:right;margin: 10px 0;">
												Total Inactive Subscribers : <span class="label label-info"><?=$total_inactive;?></span>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th>#</th>
											  		<th>E-Mail</th>
											  		<th>Status</th>
											  		<th>Action</th>
											  	</tr>
											  	<?php $num=1; foreach($data_inactives as $data_inactive) { ?>
											  	<tr class="warning">
											  		<td><?=$num;?>.</td>
											  		<td><?=$data_inactive['mail_email'];?></td>
											  		<td>INACTIVE</td>
											  		<td><a href="javascript:void(0)" data-status="<?=$data_inactive['mail_status'];?>" data-email="<?=$data_inactive['mail_email'];?>" data-id="<?=$data_inactive['mail_ID'];?>" onclick="confirmActivate(this);" class="btn btn-xs btn-info tooltips" data-placement="top" data-original-title="Activate">Activate</a></td>
											  	</tr>
											  	<?php $num++; } ?>
											</table>
										</div>
										<?php if($total_inactive >= 10 ){ ?>
										<div style="text-align:right;margin: 10px 0;">
											<a href="<?=$path['newsletter-inactive'];?>" class="btn btn-blue btn-sm">
												See All <i class="fa fa-arrow-circle-right"></i>
											</a>
										</div>
										<?php }} ?>

										<?php if(is_array($data_actives)) { ?>
										<div style="padding: 10px 0 5px 0;border-top: solid 1px #ddd;margin:30px 0 0 0;">
											<div class="text-center"><h4 class="bold">Active Subscribers</h4></div>
											<div style="text-align:right;margin: 10px 0;">
												Total Active Subscribers : <span class="label label-info"><?=$total_active;?></span>
											</div>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th>#</th>
											  		<th>E-Mail</th>
											  		<th>Status</th>
											  		<th>Action</th>
											  	</tr>
											  	<?php $num=1; foreach($data_actives as $data_active) { ?>
											  	<tr class="info">
											  		<td><?=$num;?>.</td>
											  		<td><?=$data_active['mail_email'];?></td>
											  		<td>ACTIVE</td>
											  		<td><a href="javascript:void(0)" data-status="<?=$data_active['mail_status'];?>" data-email="<?=$data_active['mail_email'];?>" data-id="<?=$data_active['mail_ID'];?>" onclick="confirmDeactivate(this);" class="btn btn-xs btn-warning tooltips" data-placement="top" data-original-title="Deactivate">Deactivate</a></td>
											  	</tr>
											  	<?php $num++; } ?>
											</table>
										</div>
										<?php if($total_active >= 10 ){ ?>
										<div style="text-align:right;margin: 10px 0;">
											<a href="<?=$path['newsletter-active'];?>" class="btn btn-blue btn-sm">
												See All <i class="fa fa-arrow-circle-right"></i>
											</a>
										</div>
										<?php }} ?>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>

		<?php include("../../packages/footer-js.php");?>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			<?php } ?>

			//function newsletter activate
			function confirmActivate(x){
				var mail_id = $(x).attr("data-id");
				var mail_email = $(x).attr("data-email");
				var mail_status = $(x).attr("data-status");

				swal({
	                title: "Are you sure?",
	                text: "Are you sure you want to set email: "+mail_email+" as ACTIVE",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#49afcd",
	                confirmButtonText: "Activate ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },

	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=update&id="+mail_id+"&status="+mail_status;
	                } else {
	                    //nothing
	                }
	            });
			}

			//function newsletter deactivate
			function confirmDeactivate(x){
				var mail_id = $(x).attr("data-id");
				var mail_email = $(x).attr("data-email");
				var mail_status = $(x).attr("data-status");

				swal({
	                title: "Are you sure?",
	                text: "Are you sure you want to set email: "+mail_email+" as INACTIVE",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#f89406",
	                confirmButtonText: "Deactivate ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },

	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=update&id="+mail_id+"&status="+mail_status;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>