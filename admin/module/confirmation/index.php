<?php 	
	include("../../packages/require.php");	
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("../../controller/controller_confirmation.php");	
	$curpage="confirmation";
	$page_name = "index.php";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['confirmation'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->
			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->
			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">
						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Confirmation Management 
										<small>View and check status payment confirmation here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Payment Confirmation Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-xs-12">
								<div class="alert alert-info">
									This module only to CONFIRM payment that are submitted by Customer through Confirm Payment Page.
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-body">
										<div style="text-align:right;margin: 10px 0;">
											Total Data : <span class="label label-info"><?=$total_data;?></span>
										</div>
										<div class="table-responsive">
											<table class="table table-striped table-bordered">
											  	<tr>
											  		<th class="text-center">#</th>
											  		<th>Conf Name</th>
											  		<th>Conf Date</th>
											  		<th>Customer Name</th>
											  		<th>PO Name</th>
											  		<th>Amount Due</th>
											  		<th>Amount Paid</th>
											  		<th>Status</th>
											  		<th>Action</th>
											  	</tr>
											  	<?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
											  	<tr class="<?=rowColor($data['confirm_status']);?>">
											  		<td class="text-center"><?=($O_page-1)*10+$num;?>.</td>
											  		<td><?=$data['confirm_name']."-".$data['confirm_status'];?></td>
											  		<td><?=date('F j, Y', strtotime($data['confirm_date']));?></td>
											  		<td><?=$data['customer_fname'];?></td>
											  		<td><?=$data['po_name'];?></td>
											  		<td>Rp. <?=number_format(floatval($data['po_amount']),0,',','.');?></td>
											  		<td>Rp. <?=number_format(floatval($data['confirm_amount']),0,',','.');?></td>
											  		<td><?=statConfirm($data['confirm_status']);?></td>
											  		<td>
											  			<div class="btn-group-vertical text-center">
															<a href="<?=$path['order-edit'].$data['confirm_poID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="View Order"><i class="fa fa-eye"></i> Order</a>
															<a class="btn btn-xs btn-azure" 
															data-confName="<?=$data['confirm_name'];?>" 
															data-poName="<?=$data['po_name'];?>" 
															data-custName="<?=$data['customer_fname'];?>" 
															data-noRek="<?=$data['confirm_account'];?>" 
															data-amountDue="<?=$data['po_amount'];?>" 
															data-amountPaid="<?=$data['confirm_amount'];?>" 
															data-confDate="<?=date('Y-m-d', strtotime($data['confirm_date']));?>" 
															data-confStat="<?=$data['confirm_status'];?>" 
															data-confNote="<?=$data['confirm_note'];?>" 
															data-confBank="<?=$data['confirm_bank'];?>" 
															data-confAcc="<?=$data['confirm_accountName'];?>" 
															data-confID="<?=$data['confirm_ID'];?>" 
															data-confPoID="<?=$data['confirm_poID'];?>" 
															data-confEmail="<?=$data['customer_email'];?>" 
															onclick="transferData(this);" href="#panel-stat" data-toggle="modal" data-target="#panel-stat"><i class="fa fa-cog"></i> Status</a>
														</div>
											  		</td>
											  	</tr>
											  	<?php $num++;} } else { ?>
											  	<tr class="warning">
											  		<td colspan="8" class="bold">There is no data right now!</td>
											  	</tr>
											  	<?php } ?>
											</table>
										</div>
											<!-- start pagination -->
											<div class="part-pagination part-pagination-customer" style="">
												<ul class="pagination pagination-blue margin-bottom-10">
													<?php
													$batch = getBatch($O_page);
													if($batch < 1){$batch = 1;}
													$prevLimit = 1 +(5*($batch-1));
													$nextLimit = 5 * $batch;
													if($nextLimit > $total_page){
														$nextLimit = $total_page;
													}
													if($O_page > 1){
														echo "<li><a href='".$page_name."?page=".($O_page-1)."'><i class='fa fa-chevron-left'></i> Prev</a></li>";
													}
													for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ 
													?>
													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon;?>" ><?php echo $mon;?></a></li>
													<?php 
													}
													if($total_page > 1 && $O_page != $total_page){
														echo "<li><a href='".$page_name."?page=".($O_page+1)."'>Next <i class='fa fa-chevron-right'></i></a></li>";
													}
													?>
												</ul>
											</div>
											<!-- end pagination -->
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-stat" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Status Confirmation <span id="label-confName" class="label label-info"></span></h4>
								</div>
								<form name="updateConfirmation" action="index.php?action=update" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Date *</strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_date" name="conf_date" type="date" class="form-control" />
		                                    	<div id="error-conf_date" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Confirmation Name *</strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_name" name="conf_name" type="text" class="form-control" placeholder="confirmation name" />
		                                    	<div id="error-conf_name" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>PO Name </strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_poName" name="conf_poName" type="text" class="form-control disable-state" placeholder="po name"/>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Customer Name </strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_custName" name="conf_custName" type="text" class="form-control disable-state" placeholder="customer name"/>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Amount Due </strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_amountDue" name="conf_amountDue" type="text" class="form-control disable-state" placeholder="amount due"  />
		                                    	<div id="error-conf_amountDue" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Amount Paid *</strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_amountPaid" name="conf_amountPaid" type="text" class="form-control" placeholder="amount paid" />
		                                    	<div id="error-conf_amountPaid" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>From Bank *</strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_bank" name="conf_bank" type="text" class="form-control" placeholder="bank name" />
		                                    	<div id="error-conf_bank" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>No Rekening *</strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_noRek" name="conf_noRek" type="text" class="form-control" placeholder="no rek" />
		                                    	<div id="error-conf_noRek" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Rekening A/N *</strong></div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input id="conf_rekName" name="conf_rekName" type="text" class="form-control" placeholder="rek account name" />
		                                    	<div id="error-conf_rekName" class="is-error"></div>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label">Note  </div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<textarea id="conf_note" name="conf_note" rows="3" class="form-control" placeholder="confirmation note" ></textarea>
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Status *</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<select id="conf_stat" name="conf_stat" class="form-control" onchange="checkStatus();">
		                                    		<option value="" class="statusVal">Choose Status</option>
		                                    		<option value="2" class="statusVal">CONFIRMED</option>		                                         
		                                            <option value="1" class="statusVal">REJECTED</option>
		                                            <option value="0" class="statusVal">PENDING</option>                    
		                                        </select>
		                                        <textarea id="conf_reason" name="conf_reason" rows="3" class="form-control hide" placeholder="rejected reason" style="margin:10px 0 0 0;" ></textarea>
		                                        <div id="error-conf_stat" class="is-error"></div>
		                                    </div>                                 			
		                                                                        
		                                </div>
									</div>
									<div class="modal-footer f5-bg">
										<input id="conf_poName2" name="conf_poName2" type="hidden" />
		                                <input id="conf_custName2" name="conf_custName2" type="hidden"  />
		                                <input id="conf_ID" name="conf_ID" type="hidden" />
		                                <input id="conf_poID" name="conf_poID" type="hidden" />
		                                <input id="conf_Email" name="cust_Email" type="hidden" />
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Save
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->
			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>
		<?php include("../../packages/footer-js.php");?>
		<script type="text/javascript">
			function checkStatus(){
				var stat = $("#conf_stat").val();
				if(stat != 1){
                	$("#conf_reason").addClass("hide");
                	$("#conf_reason").val("");
                } else {
                	$("#conf_reason").removeClass("hide");
                }
			}
			function validateForm(){
				var conf_date = $("#conf_date").val();
				var conf_name = $("#conf_name").val();
				var conf_amountDue = $("#conf_amountDue").val();
				var conf_amountPaid = $("#conf_amountPaid").val();
				var conf_bank = $("#conf_bank").val();
				var conf_noRek = $("#conf_noRek").val();
				var conf_rekName = $("#conf_rekName").val();
				var conf_stat = $("#conf_stat").val();
				if(conf_stat != ""){
					$("#error-conf_stat").html("");
					$("#error-conf_stat").hide();
					$("#input-conf_stat").removeClass("input-error");
				} else {
					$("#error-conf_stat").show();
					$("#error-conf_stat").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_stat").addClass("input-error");
					return false;
				}
				
				if(conf_date != ""){
					$("#error-conf_date").html("");
					$("#error-conf_date").hide();
					$("#input-conf_date").removeClass("input-error");
				} else {
					$("#error-conf_date").show();
					$("#error-conf_date").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_date").addClass("input-error");
					return false;
				}
				if(conf_name != ""){
					$("#error-conf_name").html("");
					$("#error-conf_name").hide();
					$("#input-conf_name").removeClass("input-error");
				} else {
					$("#error-conf_name").show();
					$("#error-conf_name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_name").addClass("input-error");
					return false;
				}
				if(conf_amountDue != ""){
					$("#error-conf_amountDue").html("");
					$("#error-conf_amountDue").hide();
					$("#input-conf_amountDue").removeClass("input-error");
				} else {
					$("#error-conf_amountDue").show();
					$("#error-conf_amountDue").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_amountDue").addClass("input-error");
					return false;
				}
				if(conf_amountPaid != ""){
					$("#error-conf_amountPaid").html("");
					$("#error-conf_amountPaid").hide();
					$("#input-conf_amountPaid").removeClass("input-error");
				} else {
					$("#error-conf_amountPaid").show();
					$("#error-conf_amountPaid").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_amountPaid").addClass("input-error");
					return false;
				}
				if(conf_bank != ""){
					$("#error-conf_bank").html("");
					$("#error-conf_bank").hide();
					$("#input-conf_bank").removeClass("input-error");
				} else {
					$("#error-conf_bank").show();
					$("#error-conf_bank").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_bank").addClass("input-error");
					return false;
				}
				if(conf_noRek != ""){
					$("#error-conf_noRek").html("");
					$("#error-conf_noRek").hide();
					$("#input-conf_noRek").removeClass("input-error");
				} else {
					$("#error-conf_noRek").show();
					$("#error-conf_noRek").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_noRek").addClass("input-error");
					return false;
				}
				if(conf_rekName != ""){
					$("#error-conf_rekName").html("");
					$("#error-conf_rekName").hide();
					$("#input-conf_rekName").removeClass("input-error");
				} else {
					$("#error-conf_rekName").show();
					$("#error-conf_rekName").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-conf_rekName").addClass("input-error");
					return false;
				}
			}
			function transferData(x){
				var conf_name = $(x).attr("data-confName");
				var conf_poName = $(x).attr("data-poName");
				var conf_custName = $(x).attr("data-custName");
				var conf_noRek = $(x).attr("data-noRek");
				var conf_amountDue = $(x).attr("data-amountDue");
				var conf_amountPaid = $(x).attr("data-amountPaid");
				var conf_date = $(x).attr("data-confDate");
				var conf_stat = $(x).attr("data-confStat");
				var conf_note = $(x).attr("data-confNote");
				var conf_bank = $(x).attr("data-confBank");
				var conf_acc = $(x).attr("data-confAcc");
				var conf_ID = $(x).attr("data-confID");
				var conf_poID = $(x).attr("data-confPoID");
				var conf_Email = $(x).attr("data-confEmail");
				$("#conf_date").val(conf_date);
                $('option[value*="' + conf_stat + '"].statusVal').attr("selected", true);
                if(conf_stat != 1){
                	$("#conf_reason").addClass("hide");
                	$("#conf_reason").val("");
                } else {
                	$("#conf_reason").removeClass("hide");
                }
                $("#conf_name").val(conf_name);
                $("#conf_poName").val(conf_poName);
                $("#conf_custName").val(conf_custName);
                $("#conf_poName2").val(conf_poName);
                $("#conf_custName2").val(conf_custName);
                $("#conf_amountDue").val(conf_amountDue);
                $("#conf_amountPaid").val(conf_amountPaid);
                $("#conf_bank").val(conf_bank);
                $("#conf_note").text(conf_note);
                $("#conf_rekName").val(conf_acc);
                $("#conf_noRek").val(conf_noRek);
                $("#conf_ID").val(conf_ID);
                $("#conf_poID").val(conf_poID);
                $("#label-confName").text(conf_name);
                $("#conf_Email").val(conf_Email);
				  
			}
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>
			//function confirmation delete
			function confirmDelete(num, text){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num+"&title="+text;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>