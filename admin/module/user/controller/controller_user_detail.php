<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/User.php");
$obj_user = new User(); 

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();

    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_user->get_data_detail($O_id);
    //var_dump($datas);

    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "edit"){
        $obj_con->up();

        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_email = mysql_real_escape_string(check_input($_POST['email']));
        $N_username = mysql_real_escape_string(check_input($_POST['username']));
        $N_password = mysql_real_escape_string(check_input($_POST['password']));
        $N_newpassword = mysql_real_escape_string(check_input($_POST['new_password']));
        if($N_newpassword != ""){
            $R_password = $N_newpassword;
        } else {
            $R_password = $N_password;
        }

        $result = $obj_user->update_data($N_id, $N_email, $N_username, $R_password);
        if($result <= 0){
            $message = "User <i><b>'" . $N_username . "'</b></i> failed to be updated.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "User <i><b>'" . $N_username . "'</b></i> is successfully updated.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>