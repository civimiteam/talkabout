<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_user_detail.php");
	$curpage="user";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['user'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->

	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					<div class="container">
						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										User Management 
										<small>Editing your carousel image here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="<?=$path['user'];?>">
											User Management
										</a>
									</li>
									<li class="active">
										<?=$datas[0]['user_name'];?>
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<?php if(is_array($datas)){?>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light">
										<h4 class="panel-title">User <span class="text-bold">(<?=$datas[0]['user_name'];?>)</span></h4>
									</div>
									<form name="editUser" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
										<div class="panel-body">
											<div class="form-body">
												<div class="row">
				                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Email <span class="symbol required"></span></strong></div>
				                                    <div class="col-sm-6 col-xs-12 up1">
				                                    	<input id="input-email" name="email" type="email" class="form-control" placeholder="user email" value="<?=$datas[0]['user_email'];?>" />
		                                    			<div id="error-email" class="is-error"></div>
				                                    </div>
				                                </div>
				                                <div class="row">
				                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Username <span class="symbol required"></span></strong></div>
				                                    <div class="col-sm-6 col-xs-12 up1">
				                                    	<input id="input-username" name="username" type="text" class="form-control" placeholder="username" value="<?=$datas[0]['user_name'];?>" />
		                                    			<div id="error-username" class="is-error"></div>
				                                    </div>
				                                </div>
				                                <div class="row">
				                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Current Password <span class="symbol required"></span></strong></div>
				                                    <div class="col-sm-6 col-xs-12 up1">
				                                    	<input id="input-password" name="password" type="password" class="form-control" placeholder="user password" value="<?=$datas[0]['user_password'];?>"/>
				                                    	<input id="input-oldpassword" type="hidden" value="<?=$datas[0]['user_password'];?>">
		                                    			<div id="error-password" class="is-error"></div>
				                                    </div>
				                                </div>
				                                <div class="row">
				                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>New Password <span class="symbol required"></span></strong></div>
				                                    <div class="col-sm-6 col-xs-12 up1">
				                                    	<input id="input-new_password" name="new_password" type="password" class="form-control" placeholder="new password" />
		                                    			<div id="error-new_password" class="is-error"></div>
				                                    </div>
				                                </div>
				                                <div class="row">
				                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Re-Enter New Password <span class="symbol required"></span></strong></div>
				                                    <div class="col-sm-6 col-xs-12 up1">
				                                    	<input id="input-renew_password" name="renew_password" type="password" class="form-control" placeholder="re enter new password" />
		                                    			<div id="error-renew_password" class="is-error"></div>
				                                    </div>
				                                </div>
											</div>
										</div>
										<div class="panel-footer">
											<div class="row">
												<input type="hidden" name="id" value="<?=$datas[0]['user_ID'];?>">
												<div class="col-sm-2 col-xs-12 pad0">
													<div class="link-delete">
														<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['user_ID'];?>', '<?=$datas[0]['user_name'];?>');">
															Delete
														</a>
													</div>
												</div>
												<div class="col-sm-8 col-xs-12 pad0 text-right">
													<div class="btn-group text-right hidden-xs">
														<a href="<?=$path['user'];?>" type="reset" class="btn btn-default">
															<i class="fa fa-times"></i> Cancel
														</a>
														<button type="submit" class="btn btn-success">
															<i class="fa fa-check fa fa-white"></i> Update
														</button>
													</div>
													<div class="btn-group visible-xs">
														<div class="row">
															<div class="col-xs-12 text-right">
																<a href="<?=$path['user'];?>" type="reset" class="btn btn-default">
																	<i class="fa fa-times"></i> Cancel
																</a>
																<button type="submit" class="btn btn-success">
																	<i class="fa fa-check fa fa-white"></i> Update
																</button>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</form>
								</div>
							</div>
						</div>
						<?php }else{?>
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light">
										<h4 class="panel-title">There is no data in this page!</h4>
									</div>
								</div>
							</div>
						</div>
						<?php }?>
						<!-- end: PAGE CONTENT-->
					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>
		<?php include("../../packages/footer-js.php");?>
		<script type="text/javascript">
			function validateForm(){
				var email = $("#input-email").val();
				var username = $("#input-username").val();
				var password = $("#input-password").val();
				var oldpassword = $("#input-oldpassword").val();
				var new_password = $("#input-new_password").val();
				var renew_password = $("#input-renew_password").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;

				if(email != ""){
					if(email.match(mailformat)){
						$("#error-email").html("");
						$("#error-email").hide();
						$("#input-email").removeClass("input-error");
					} else {
						$("#error-email").show();
						$("#error-email").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email").show();
					$("#error-email").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email").addClass("input-error");
					return false;
				}
				if(username != ""){
					$("#error-username").html("");
					$("#error-username").hide();
					$("#input-username").removeClass("input-error");
				} else {
					$("#error-username").show();
					$("#error-username").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-username").addClass("input-error");
					return false;
				}
				if(password != ""){
					if(password == oldpassword){
						$("#error-password").html("");
						$("#error-password").hide();
						$("#input-password").removeClass("input-error");
					} else {
						$("#error-password").show();
						$("#error-password").html("<i class='fa fa-warning'></i> Your current password is not correct.");
						$("#input-password").addClass("input-error");
						return false;
					}
				} else {
					$("#error-password").show();
					$("#error-password").html("<i class='fa fa-warning'></i> Current password is required.");
					$("#input-password").addClass("input-error");
					return false;
				}
				if(new_password == "" && renew_password == ""){
					$("#error-new_password").html("");
					$("#error-new_password").hide();
					$("#input-new_password").removeClass("input-error");
					$("#error-renew_password").html("");
					$("#error-renew_password").hide();
					$("#input-renew_password").removeClass("input-error");
				}
				else if(new_password != "" && renew_password == ""){
					$("#error-new_password").html("");
					$("#error-new_password").hide();
					$("#input-new_password").removeClass("input-error");
					$("#error-renew_password").show();
					$("#error-renew_password").html("<i class='fa fa-warning'></i> Please re-enter your new password.");
					$("#input-renew_password").addClass("input-error");
					return false;
				} 
				else if(new_password == "" && renew_password != ""){
					$("#error-renew_password").html("");
					$("#error-renew_password").hide();
					$("#input-renew_password").removeClass("input-error");
					$("#error-new_password").show();
					$("#error-new_password").html("<i class='fa fa-warning'></i> Please enter your new password.");
					$("#input-new_password").addClass("input-error");
					return false;
				} 
				else if(new_password != "" && renew_password != ""){
					if(new_password == renew_password){
						$("#error-new_password").html("");
						$("#error-new_password").hide();
						$("#input-new_password").removeClass("input-error");
						$("#error-renew_password").html("");
						$("#error-renew_password").hide();
						$("#input-renew_password").removeClass("input-error");
					} else {
						$("#error-new_password").html("");
						$("#error-new_password").hide();
						$("#input-new_password").removeClass("input-error");
						$("#error-renew_password").show();
						$("#error-renew_password").html("<i class='fa fa-warning'></i> Your new password do not match.");
						$("#input-renew_password").addClass("input-error");
						return false;
					}
				}
			}

			//function confirmation delete
			function confirmDelete(num, text){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num+"&title="+text;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>