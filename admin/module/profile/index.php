<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_profile.php");
$curpage="profile";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['profile'];?></title>
	<?php include("../../packages/module-head.php");?>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper">
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?> 
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Profile Management 
									<small>Editing you company profile here</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['profile'];?>">
										Profile Management
									</a>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Profile Management</span></h4>
								</div>
								<form name="updateProfile" action="index.php?action=update" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-offset-1 col-sm-10 col-xs-12 pad0">
													<div class="row">
														<div class="col-xs-12 up1">
															<div class="form-label bold text-left">Company Name <span class="symbol required"></span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-company_name" name="company_name" type="text" class="form-control" placeholder="company name" value="<?=$J_company;?>" />
		                                    				<div id="error-company_name" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up3">
															<div class="form-label bold text-left">Address</span></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<input id="input-address1" name="address1" type="text" class="form-control"  placeholder="address " value="<?=$J_address1;?>" />
		                                    				<div id="error-address1" class="is-error"></div>
														</div>
													</div>
													<!--
													<div class="row">
														<div class="col-xs-12 up1">
															<input name="address2" type="text" class="form-control" placeholder="address 2" value="<?=$J_address2;?>" />
														</div>
													</div>
													-->
													<div class="row">
														<div class="col-xs-12 up1">
															<input id="input-city" name="city" type="text" class="form-control" placeholder="city " value="<?=$J_city;?>" />
		                                    				<div id="error-city" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-province" name="province" type="text" class="form-control" placeholder="province " value="<?=$J_province;?>" />
		                                    				<div id="error-province" class="is-error"></div>
														</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-zip" name="zip" type="text" class="form-control" placeholder="zip code " value="<?=$J_zip;?>" />
		                                    				<div id="error-zip" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-country" name="country" type="text" class="form-control" placeholder="country " value="<?=$J_country;?>" />
		                                    				<div id="error-country" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up3">
															<div class="form-label bold text-left">Email Address 1<span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-email1" name="email1" type="text" class="form-control" placeholder="email address 1*" value="<?=$J_email1;?>" />
		                                    				<div id="error-email1" class="is-error"></div>
															<small><span class="help-block up05"><i class="fa fa-info-circle"></i> E-mail to receive contact form and order</span></small>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Email Address 2</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input name="email2" type="email" class="form-control" placeholder="email address 2" value="<?=$J_email2;?>" />
		                                    				<div id="error-email2" class="is-error"></div>
															<small><span class="help-block up05"><i class="fa fa-info-circle"></i> E-mail will be displayed in footer</span></small>
														</div>
													</div>
													<!--
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Pin BB</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input name="bbm" type="text" class="form-control" placeholder="pin bb" value="<?=$J_bbm;?>" />
														</div>
													</div>
													-->
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Phone Number <span class="symbol required"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input id="input-phone1" name="phone1" type="text" class="form-control" placeholder="phone number *" value="<?=$J_phone1;?>" />
		                                    				<div id="error-phone1" class="is-error"></div>
														</div>
													</div>
													<!--
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Phone Number 2</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input name="phone2" type="text" class="form-control" placeholder="phone number 2" value="<?=$J_phone2;?>" />
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Mobile / Fax</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input name="fax" type="text" class="form-control" placeholder="mobile / fax" value="<?=$J_fax;?>" />
														</div>
													</div>
													-->
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Website</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<input name="website" type="text" class="form-control" placeholder="url website" value="<?=$J_website;?>" />
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Company Short Description</div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-8 col-xs-12 up1">
															<textarea name="short-desc" class="form-control" rows="4" placeholder="Company Short Description" maxlength="175"><?=correctDisplay($J_short_desc);?></textarea>
															<small><span class="help-block up05"><i class="fa fa-info-circle"></i> displayed at the footer(left side) <span style="color: red;">with max 175 chars.</span></span></small>
														</div>
													</div>
													<!--
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Current Promo</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<input name="promo" type="text" class="form-control" placeholder="current promo" maxlength="100" value="<?=$J_promo;?>" />
															<small><span class="help-block up05"><i class="fa fa-info-circle"></i> max length 100 chars</span></small>
														</div>
													</div>
													-->
													<div class="row">
														<div class="col-xs-12 up15">
															<div class="form-label bold text-left">Bank Account Info</div>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-12 up1">
															<textarea name="bank" class="tinymce form-control" rows="5" placeholder="bank account info"><?=correctDisplay($J_bank);?></textarea>
															<small><span class="help-block up05"><i class="fa fa-info-circle"></i> shown at checkout</span></small>
														</div>
													</div>
													
												</div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<div class="col-sm-offset-1 col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													<a href="<?=$path['profile'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<script src="<?=$global['absolute-url-admin'];?>packages/tinymce/tinymce.min.js"></script>
	<script>
        tinymce.init({
            selector: ".tinymce",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
            relative_urls: false,
            forced_root_block : false,
            height : 200
        });
    	</script>
	<script type="text/javascript">
			function validateForm(){
				var company_name = $("#input-company_name").val();
				// var address1 = $("#input-address1").val();
				// var province = $("#input-province").val();
				// var city = $("#input-city").val();
				// var zip = $("#input-zip").val();
				// var country = $("#input-country").val();
				var email1 = $("#input-email1").val();
				var phone1 = $("#input-phone1").val();
				var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
				
				if(company_name != ""){
					$("#error-company_name").html("");
					$("#error-company_name").hide();
					$("#input-company_name").removeClass("input-error");
				} else {
					$("#error-company_name").show();
					$("#error-company_name").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-company_name").addClass("input-error");
					return false;
				}
				// if(address1 != ""){
				// 	$("#error-address1").html("");
				// 	$("#error-address1").hide();
				// 	$("#input-address1").removeClass("input-error");
				// } else {
				// 	$("#error-address1").show();
				// 	$("#error-address1").html("<i class='fa fa-warning'></i> This field is required.");
				// 	$("#input-address1").addClass("input-error");
				// 	return false;
				// }
				// if(city != ""){
				// 	$("#error-city").html("");
				// 	$("#error-city").hide();
				// 	$("#input-city").removeClass("input-error");
				// } else {
				// 	$("#error-city").show();
				// 	$("#error-city").html("<i class='fa fa-warning'></i> This field is required.");
				// 	$("#input-city").addClass("input-error");
				// 	return false;
				// }
				// if(province != ""){
				// 	$("#error-province").html("");
				// 	$("#error-province").hide();
				// 	$("#input-province").removeClass("input-error");
				// } else {
				// 	$("#error-province").show();
				// 	$("#error-province").html("<i class='fa fa-warning'></i> This field is required.");
				// 	$("#input-province").addClass("input-error");
				// 	return false;
				// }
				// if(zip != ""){
				// 	$("#error-zip").html("");
				// 	$("#error-zip").hide();
				// 	$("#input-zip").removeClass("input-error");
				// } else {
				// 	$("#error-zip").show();
				// 	$("#error-zip").html("<i class='fa fa-warning'></i> This field is required.");
				// 	$("#input-zip").addClass("input-error");
				// 	return false;
				// }
				// if(country != ""){
				// 	$("#error-country").html("");
				// 	$("#error-country").hide();
				// 	$("#input-country").removeClass("input-error");
				// } else {
				// 	$("#error-country").show();
				// 	$("#error-country").html("<i class='fa fa-warning'></i> This field is required.");
				// 	$("#input-country").addClass("input-error");
				// 	return false;
				// }
				if(email1 != ""){
					if(email1.match(mailformat)){
						$("#error-email1").html("");
						$("#error-email1").hide();
						$("#input-email1").removeClass("input-error");
					} else {
						$("#error-email1").show();
						$("#error-email1").html("<i class='fa fa-warning'></i> Your email address must be in the format of name@domain.com");
						$("#input-email1").addClass("input-error");
						return false;
					}
				} else {
					$("#error-email1").show();
					$("#error-email1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-email1").addClass("input-error");
					return false;
				}
				if(phone1 != ""){
					$("#error-phone1").html("");
					$("#error-phone1").hide();
					$("#input-phone1").removeClass("input-error");
				} else {
					$("#error-phone1").show();
					$("#error-phone1").html("<i class='fa fa-warning'></i> This field is required.");
					$("#input-phone1").addClass("input-error");
					return false;
				}
			}
		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>
		</script>
	</body>
	<!-- end: BODY -->
	</html>