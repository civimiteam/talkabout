<?php 
$file_json = $global['root-url']."uploads/json/company_profile.json";

if(!isset($_GET['action'])){
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_company = $json['company_name'];
    $J_address1 = $json['address1'];
    $J_city = $json['city'];
    $J_province = $json['province'];
    $J_zip = $json['zip'];
    $J_country = $json['country'];
    $J_email1 = $json['email1'];
    $J_email2 = $json['email2'];
    $J_phone1 = $json['phone1'];
    $J_website = $json['website'];
    $J_short_desc = $json['short-desc'];
    $J_bank = $json['bank'];
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
} else if(isset($_GET['action'])){
    if($_GET['action'] == "update"){
        $N_company = (check_input($_POST['company_name']));
        $N_address1 = (check_input($_POST['address1']));
        $N_city = (check_input($_POST['city']));
        $N_province = (check_input($_POST['province']));
        $N_zip = (check_input($_POST['zip']));
        $N_country = (check_input($_POST['country']));
        $N_email1 = (check_input($_POST['email1']));
        $N_email2 = (check_input($_POST['email2']));
        $N_phone1 = (check_input($_POST['phone1']));
        $N_website = (check_input($_POST['website']));
        $N_short_desc = (check_input($_POST['short-desc']));
        $N_bank = $_POST['bank'];
        
        $data['company_name'] = $N_company;
        $data['address1'] = $N_address1;
        $data['city'] = $N_city;
        $data['province'] = $N_province;
        $data['zip'] = $N_zip;
        $data['country'] = $N_country;
        $data['email1'] = $N_email1;
        $data['email2'] = $N_email2;
        $data['phone1'] = $N_phone1;
        $data['website'] = $N_website;
        $data['short-desc'] = $N_short_desc;
        $data['bank'] = $N_bank;
        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Company profile has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Company profile failed to update!";
            $_SESSION['alert'] = "error";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
    }
}
?> 