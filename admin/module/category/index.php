<?php 
	include("../../packages/require.php");
    include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_category.php");
	$curpage="category";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['category'];?></title>
		<?php include("../../packages/module-head.php");?>
        <!-- Add fancyBox -->
        <link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->
		<div class="main-wrapper">
			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->
			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->
			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->
			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">
						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Category Management 
										<small>Adding and Editing your category product here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->
						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Category Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->
						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
										<a class="btn btn-light-azure" href="#panel-add" data-toggle="modal" data-target="#panel-add">
											<i class="fa fa-plus"></i> Add New Data
										</a>
									</div>
									<div class="panel-body">
                                        <div style="text-align:right;margin: 10px 0;">
                                            Total Data : <span class="label label-info"><?=$total_data;?></span>
                                        </div>
                                        <div class="table-responsive">
                                            <table class="table table-striped table-bordered">
                                                <tr>
                                                    <th class="text-center">#</th>
                                                    <th>Title</th>
                                                    <th># Product</th>
                                                    <th>Image</th>
                                                    <th>Publish</th>
                                                    <th>Action</th>
                                                </tr>
                                                <?php if(is_array($datas)) { $num=1;foreach($datas as $data) { ?>
                                                <tr>
                                                    <td class="text-center"><?=$num;?>.</td>
                                                    <td><?=correctDisplay($data['category_name']);?></td>
                                                    <td><?=$data['counter'];?></td>
                                                    <td>
                                                        <a class="fancybox" href="<?=$global['absolute-url'].$data['category_img'];?>">
                                                            <img style="width: 40px;" class="img-circle" src="<?=$global['absolute-url'].$data['category_imgThmb'];?>">
                                                        </a>
                                                    </td>
                                                    <td><?=$data['category_publish'];?></td>
                                                    <td>
                                                        <div class="text-center">
                                                            <a href="<?=$path['category-edit'].$data['category_ID'];?>" class="btn btn-xs btn-blue tooltips" data-placement="top" data-original-title="Edit"><i class="fa fa-edit"></i></a>
                                                            <a href="javascript:void(0)" onclick="confirmDelete('<?=$data['category_ID'];?>', '<?=$data['category_name'];?>');" class="btn btn-xs btn-red tooltips" data-placement="top" data-original-title="Remove"><i class="fa fa-trash-o fa fa-white"></i></a>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php $num++;} } else { ?>
                                                <tr class="warning">
                                                    <td colspan="5" class="bold">There is no data right now!</td>
                                                </tr>
                                                <?php } ?>
                                            </table>
                                        </div>
                                    </div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->
					</div> 
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>
					<!-- start: PANEL ADD MODAL FORM -->
                    <div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header form-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title form-title">Add New Category</h4>
                                </div>
                                <form name="addCategory" action="index.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
                                    <div class="modal-body">
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Category Name <span class="symbol required"></span></strong> :</div>
                                            <div class="col-sm-8 col-xs-12 up1">
                                                <input id="input-title" name="name" type="text" class="form-control" placeholder="category name" />
                                                <div id="error-title" class="is-error"></div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Sort # <span class="symbol required"></span></strong> :</div>
                                            <div class="col-sm-8 col-xs-12 up1">
                                                <input id="input-sort" name="sort" type="number" class="form-control" placeholder="#" />
                                                <div id="error-sort" class="is-error"></div>
                                            </div>
                                        </div>
                                        <div class="row">       
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong> :</div>
                                            <div class="col-sm-8 col-xs-12 up1">
                                                <select id="input-publish" name="publish" class="form-control" >
                                                    <option value="Publish">Publish</option>
                                                    <option value="Not Publish">Not Publish</option>
                                                </select>
                                                <div id="error-publish" class="is-error"></div>
                                            </div>
                                        </div>
                                        <div class="row">       
                                            <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Image Upload <span class="symbol required"></span></strong> :</div>
                                            <div class="col-sm-8 col-xs-12 up1">
                                                <input id="input-image" name="image" type="file" class="text" placeholder="category image" required />
                                                <div id="error-image" class="is-error"></div>
                                                <small><span class="help-block"><i class="fa fa-info-circle"></i> <?php echo $infoSize['category'];?></span></small>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer f5-bg">
                                        <div class="btn-group">
                                            <button type="reset" class="btn btn-default" data-dismiss="modal">
                                                <i class="fa fa-times"></i> Cancel
                                            </button>
                                            <button type="submit" class="btn btn-success">
                                                <i class="fa fa-check fa fa-white"></i> Create
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <!-- /.modal-content -->
                        </div>
                        <!-- /.modal-dialog -->
                    </div>
                    <!-- /.modal -->
                    <!-- end: SPANEL CONFIGURATION MODAL FORM -->
				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->
			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->
			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>
		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
        <script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
        <script type="text/javascript">
            $(document).ready(function() {
                $(".fancybox").fancybox({
                    padding : 0
                });
            });
        </script>
        <script type="text/javascript">
            function validateForm(){
                var title = $("#input-title").val();
                var sort = $("#input-sort").val();
                var numFormat = /^[0-9]+$/;
                var image = $("#input-image").val();
                
                if(title != ""){
                    $("#error-title").html("");
                    $("#error-title").hide();
                    $("#input-title").removeClass("input-error");
                } else {
                    $("#error-title").show();
                    $("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-title").addClass("input-error");
                    return false;
                }
                if(sort != ""){
                    if(sort.match(numFormat)){
                        $("#error-sort").html("");
                        $("#error-sort").hide();
                        $("#input-sort").removeClass("input-error");
                    } else {
                        $("#error-sort").show();
                        $("#error-sort").html("<i class='fa fa-warning'></i> This field must contain number only.");
                        $("#input-sort").addClass("input-error");
                        return false;
                    }
                } else {
                    $("#error-sort").show();
                    $("#error-sort").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-sort").addClass("input-error");
                    return false;
                }
                if(image != ""){
                    $("#error-image").html("");
                    $("#error-image").hide();
                    $("#input-image").removeClass("input-error");
                } else {
                    $("#error-image").show();
                    $("#error-image").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-image").addClass("input-error");
                    return false;
                }
            }

            <?php if($message != "") { ?>
            //use session here for alert success/failed
            var alertText = "<?=$message;?>"; //teks for alert
            
                <?php if($alert != "success"){ ?>
                    //error alert
                    errorAlert(alertText);
                <?php } else { ?>
                    //success alert
                    successAlert(alertText); 
                <?php } ?>
             
            <?php } ?>
            
            //function confirmation delete
            function confirmDelete(num, text){
                swal({
                    title: "Are you sure?",
                    text: "You will not be able to recover this file!",
                    type: "warning",
                    showCancelButton: true,
                    confirmButtonColor: "#DD6B55",
                    confirmButtonText: "Delete ! ",
                    cancelButtonText: "Cancel !",
                    closeOnConfirm: false,
                    closeOnCancel: true
                },
                function (isConfirm) {
                    if (isConfirm) {
                        window.location.href = "index.php?action=delete&id="+num+"&title="+text;
                    } else {
                        //nothing
                    }
                });
            }
        </script>
	</body>
	<!-- end: BODY -->
</html>