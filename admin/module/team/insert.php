<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_team.php");
	$curpage = "team";
	$index = $path['team'];
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['team'];?></title>
		<?php include("../../packages/module-head.php");?>
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-9">
								<div class="page-header">
									<h1>
										Team Management
										<small>Adding and Editing your team here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-3 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li>
										<a href="<?=$index;?>">
											Team Management
										</a>
									</li>
									<li class="active">
										Add New Team
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-left">
										<h4 class="modal-title form-title">Add New Team</h4>
									</div>
									<div class="panel-body">
										<form name="addTeam" action="insert.php?action=add" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
											<div class="panel-body">
												<div class="form-body">
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Name <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-name" name="name" type="text" class="form-control" placeholder="name" maxlength="150" />
		                                                	<div id="error-name" class="is-error"></div>
		                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Team name max 150 chars.</span></small>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Position <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-position" name="position" type="text" class="form-control" placeholder="position" maxlength="150" />
		                                                	<div id="error-position" class="is-error"></div>
		                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Position max 150 chars.</span></small>
														</div>
													</div>
													<div class="row up1">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Description<span class="symbol required"></span></strong> :</div>
														<div class="col-sm-7 col-xs-12 up1">
															<textarea id="input-desc" name="desc" class="tinymce form-control" rows="5" placeholder="description (english)"></textarea>
															<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Description of the team member.</span></small>
		                                                	<div id="error-desc" class="is-error"></div>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Facebook Link <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-facebook" name="facebook" type="text" class="form-control" placeholder="facebook" maxlength="150" />
		                                                	<div id="error-facebook" class="is-error"></div>
		                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Facebook Link max 150 chars.</span></small>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Instagram Link <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-instagram" name="instagram" type="text" class="form-control" placeholder="instagram" maxlength="150" />
		                                                	<div id="error-instagram" class="is-error"></div>
		                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Instagram Link max 150 chars.</span></small>
														</div>
													</div>
													<div class="row">
														<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Twtter Link <span class="symbol required"></span></strong> :</div>
														<div class="col-sm-6 col-xs-12 up1">
															<input id="input-twitter" name="twitter" type="text" class="form-control" placeholder="twitter" maxlength="150" />
		                                                	<div id="error-twitter" class="is-error"></div>
		                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Twtter Link max 150 chars.</span></small>
														</div>
													</div>
													<div class="row">       
							                            <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong> :</div>
							                            <div class="col-sm-6 col-xs-12 up1">
							                                <select id="input-publish" name="publish" class="form-control">
							                                    <option value="Publish">Publish</option>
							                                    <option value="Not Publish">Not Publish</option>
							                                </select>
							                                <div id="error-publish" class="is-error"></div>
							                            </div>
							                        </div>
							                        <hr/>
							                        <div class="row">       
							                            <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Photo(s) </strong>:</div>
							                            <div class="col-sm-7 col-xs-12 up1">
							                                <div id="photoField">
							                                	<input id="input-photo" name="photo[]" type="file" class="file" style="display: inline-block;"/>
							                                    <span><strong><span class="symbol required"></span>Primary</strong></span>
							                                </div>
		                                                	<div id="error-photo" class="is-error"></div>
							            					<small><span class="help-block up1"><i class="fa fa-info-circle"></i> <?php echo $infoSize['team'];?></span></small>
							                            </div>
							                        </div>
					                                <br>
													<div class="panel-footer">
														<div class="row">
															<input type="hidden" name="type" value="<?=$O_type;?>">
															<div class="col-xs-12 pad0 text-right">
																<div class="btn-group text-right">
																	<a href="<?=$index;?>" type="reset" class="btn btn-default">
																		<i class="fa fa-times"></i> Cancel
																	</a>
																	<button type="submit" class="btn btn-success">
																		<i class="fa fa-check fa fa-white"></i> Create
																	</button>
																</div>
															</div>
														</div>
													</div>
												</div>
											</div>
										</form>
									</div>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->
		</div>

		<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<?php include("../../packages/footer-js.php");?>
		<script src="<?=$global['absolute-url-admin'];?>packages/tinymce/tinymce.min.js"></script>
		<!-- for select2 in brand  -->
		<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
		<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
		<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
		<script>
	        tinymce.init({
	            selector: ".tinymce",
	            plugins: [
	            "advlist autolink lists link image charmap print preview anchor",
	            "searchreplace visualblocks code fullscreen",
	            "insertdatetime media table contextmenu paste",
	            "textcolor"
	            ],
	            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
	            relative_urls: false,
	            forced_root_block : false,
	            height : 200
	        });
    	</script>
		<script type="text/javascript">
			function validateForm(){
				var name = $("#input-name").val();
				var position = $("#input-position").val();
                var desc = tinyMCE.get('input-desc').getContent();
                var photo = $("#input-photo").val();
                
                if(name != ""){
                    $("#error-name").html("");
                    $("#error-name").hide();
                    $("#input-name").removeClass("input-error");
                } else {
                    $("#error-name").show();
                    $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-name").addClass("input-error");
                    return false;
                }
                if(position != ""){
                    $("#error-position").html("");
                    $("#error-position").hide();
                    $("#input-position").removeClass("input-error");
                } else {
                    $("#error-position").show();
                    $("#error-position").html("<i class='fa fa-warning'></i> This field is required.");
                    $("#input-position").addClass("input-error");
                    return false;
                }
                if(desc != ""){
                    $("#error-desc").html("");
                    $("#error-desc").hide();
                } else {
                    $("#error-desc").show();
                    $("#error-desc").html("<i class='fa fa-warning'></i> This field is required.");
                    return false;
                }
                if(photo != ""){
                    $("#error-photo").html("");
                    $("#error-photo").hide();
                    $("#input-photo").removeClass("input-error");
                } else {
                    $("#error-photo").show();
                    $("#error-photo").html("<i class='fa fa-warning'></i> Please insert primary photo.");
                    $("#input-photo").addClass("input-error");
                    return false;
                }
            }
		</script>
	</body>
	<!-- end: BODY -->
</html>