<?php 
	include("../../packages/require.php");
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_team_detail.php");
	$curpage = "team";
	$index = $path['team'];
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['team'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/css/lightbox.css">
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-9">
							<div class="page-header">
								<h1>
									Team Management
									<small>Editing your team data here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-3 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$index;?>">
										Team Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['team_name'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->	

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Team <span class="text-bold">(<?=$datas[0]['team_name'];?>)</span></h4>
								</div>
								<form name="editTeam" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();">
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Name <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-name" name="name" type="text" class="form-control" placeholder="name" value="<?=$datas[0]['team_name'];?>" maxlength="150" />
                                                	<div id="error-name" class="is-error"></div>
                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Team name max 150 chars.</span></small>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Position <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-position" name="position" type="text" class="form-control" placeholder="position" value="<?=$datas[0]['team_position'];?>" maxlength="150" />
                                                	<div id="error-position" class="is-error"></div>
                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Position max 150 chars.</span></small>
												</div>
											</div>
											<div class="row up1">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Description <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-7 col-xs-12 up1">
													<textarea id="input-desc" name="desc" class="tinymce form-control" rows="5" placeholder="description (english)"><?=correctDisplay($datas[0]['team_desc']);?></textarea>
													<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Description of the team member.</span></small>
                                                	<div id="error-desc" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Facebook Link <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-facebook" name="facebook" type="text" class="form-control" placeholder="facebook" value="<?=$datas[0]['team_facebook'];?>" maxlength="150" />
                                                	<div id="error-facebook" class="is-error"></div>
                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Facebook Link max 150 chars.</span></small>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Instagram Link <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-instagram" name="instagram" type="text" class="form-control" placeholder="instagram" value="<?=$datas[0]['team_instagram'];?>" maxlength="150" />
                                                	<div id="error-instagram" class="is-error"></div>
                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Instagram Link max 150 chars.</span></small>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Twitter Link <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-twitter" name="twitter" type="text" class="form-control" placeholder="twitter" value="<?=$datas[0]['team_twitter'];?>" maxlength="150" />
                                                	<div id="error-twitter" class="is-error"></div>
                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Twitter Link max 150 chars.</span></small>
												</div>
											</div>
			                                <div class="row">       
			                                    <div class="col-sm-3 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong></div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<select id="input-publish" name="publish" class="form-control" >
			                                    		<option value="Publish" <?php if($datas[0]['team_publish'] == "Publish"){ echo "selected"; }?>>Publish</option>
			                                    		<option value="Not Publish" <?php if($datas[0]['team_publish'] != "Publish"){ echo "selected"; }?>>Not Publish</option>
			                                    	</select>
			                                    	<div id="error-publish" class="is-error"></div>
			                                    </div>
			                                </div>
											<div class="row">       
												<div class="col-sm-3 col-xs-12 up1 form-label"><strong>Image Upload <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<a class="fancybox" href="<?=$global['absolute-url'].$datas[0]['team_img'];?>">
														<img style="width: 40px;" class="img-circle" src="<?=$global['absolute-url'].$datas[0]['team_imgThmb'];?>">
													</a>
													<input name="image" type="file" class="text up05" maxlength="256" placeholder="team image" />
												</div>
											</div>
											<div class="row">
												<div class="col-sm-3 hidden-xs up1"></div>
												<div class="col-sm-6 col-xs-12 up1"><small><span class="help-block"><i class="fa fa-info-circle"></i> <?php echo $infoSize['team'];?></span></small></div>
											</div>
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['team_ID'];?>">
											<div class="col-sm-2 col-xs-12 pad0">
												<div class="link-delete">
													<!-- <a href="javascript:void(0)" onclick="javascript:alert(1);try{confirmDelete('1', 'asd')}catch(e){alert(e)}">
														Delete
													</a> -->
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['team_ID'];?>', '<?=$datas[0]['team_name'];?>');">
														Delete
													</a> 
												</div>
											</div>
											<div class="col-sm-8 col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['team'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['team'];?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Update
															</button>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<?php include("../../packages/footer-js.php");?>
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/js/pages-gallery.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>packages/tinymce/tinymce.min.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
        tinymce.init({
            selector: ".tinymce",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
            relative_urls: false,
            forced_root_block : false,
            height : 200
        });
    </script>
	<script type="text/javascript">
		function validateForm(){
			var name = $("#input-name").val();
			var position = $("#input-position").val();
            var desc = tinyMCE.get('input-desc').getContent();
            
            if(name != ""){
                $("#error-name").html("");
                $("#error-name").hide();
                $("#input-name").removeClass("input-error");
            } else {
                $("#error-name").show();
                $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-name").addClass("input-error");
                return false;
            }
            if(position != ""){
                $("#error-position").html("");
                $("#error-position").hide();
                $("#input-position").removeClass("input-error");
            } else {
                $("#error-position").show();
                $("#error-position").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-position").addClass("input-error");
                return false;
            }
            if(desc != ""){
                $("#error-desc").html("");
                $("#error-desc").hide();
            } else {
                $("#error-desc").show();
                $("#error-desc").html("<i class='fa fa-warning'></i> This field is required.");
                return false;
            }
        }

		//function confirmation delete
		function confirmDelete(num, text){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num+"&title="+text;
				} else {
	                   //nothing
	               }
	           });
		}
	</script>
</body>
<!-- end: BODY -->
</html>