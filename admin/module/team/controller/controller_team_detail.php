<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/Team.php");
$obj_team = new Team();

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();

    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_team->get_data_detail($O_id);
    //var_dump($datas);

    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "edit"){
        $obj_con->up();

        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_position = mysql_real_escape_string(check_input($_POST['position']));
        $N_desc = mysql_real_escape_string(check_input($_POST['desc']));
        $N_facebook = mysql_real_escape_string(check_input($_POST['facebook']));
        $N_instagram = mysql_real_escape_string(check_input($_POST['instagram']));
        $N_twitter = mysql_real_escape_string(check_input($_POST['twitter']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));
        $Photo_ImgLink = "";
        $ThmbPhoto_ImgLink = "";

        if($_FILES["image"]["name"]!=null){
            $ran = rand();
            $timestamp = time();
            $Photo_ImgLink = "uploads/team/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "uploads/team-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                //require_once("../../packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                    //save image in server
                    $file_loc = $global['root-url'].$Photo_ImgLink; 
                    $file_locThmb = $global['root-url'].$ThmbPhoto_ImgLink;

                    //Resizing images
                    if(move_uploaded_file($file, $file_loc))
                    {
                        $image = new SimpleImage();
                        $image->load($file_loc);
                        $image->resize(200,200);
                        $image->save($file_locThmb);
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message = "Something is wrong with your uploaded team image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
                $_SESSION['alert'] = "error";
            }
        }

        $result = $obj_team->update_data($N_id, $N_name, $N_position, $N_desc, $N_facebook, $N_instagram, $N_twitter, $Photo_ImgLink, $ThmbPhoto_ImgLink, $N_publish, $global['root-url']);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Team <i><b>'" . $N_name . "'</b></i> has been succesfully edited.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $O_title = mysql_real_escape_string(check_input($_GET['title']));
        
        $result = $obj_team->delete_data($O_id, $global['root-url']);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Team <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>