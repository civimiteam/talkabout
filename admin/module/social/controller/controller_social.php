<?php
$file_json = $global['root-url']."uploads/json/social.json";
if(!isset($_GET['action'])){
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $J_facebook = $json['facebook'];
    $J_twitter = $json['twitter'];
    $J_google = $json['gplus'];
    $J_instagram = $json['instagram'];
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
} else if(isset($_GET['action'])){
    if($_GET['action'] == "update"){
        $N_facebook = (check_input($_POST['facebook']));
        $N_twitter = (check_input($_POST['twitter']));
        $N_google_plus = (check_input($_POST['gplus']));
        $N_instagram = (check_input($_POST['instagram']));
        
        $data['facebook'] = $N_facebook;
        $data['twitter'] = $N_twitter;
        $data['gplus'] = $N_google_plus;
        $data['instagram'] = $N_instagram;
        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Social media has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Social media failed to update!";
            $_SESSION['alert'] = "error";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
    }
}
?> 