<?php 
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();
require_once($global['root-url']."model/Banner.php");
$obj_banner = new Banner(); 
if(!isset($_GET['action'])){
    $obj_con->up();
    
    $datas = $obj_banner->get_data();
   
    if(is_array($datas)){
        $total_data = $datas[0]['total_data'];
    }else{
        $total_data = 0;
    }
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();
}
?> 