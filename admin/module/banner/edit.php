<?php 
include("../../packages/require.php");
include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
include("controller/controller_banner_detail.php");
$curpage="banner";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['banner'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- Add fancyBox -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->
	<div class="main-wrapper"> 
		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->
		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->
		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->
		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">
				<div class="container">
					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Banner Management 
									<small>Editing your banner here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->
					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['banner'];?>">
										Banner Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['banner_title'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->
					<!-- start: PAGE CONTENT -->
					<?php if(is_array($datas)){?>
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Banner <span class="text-bold">(<?=$datas[0]['banner_title'];?>)</span></h4>
								</div>
								<form name="editBanner" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();" >
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Page <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-position" name="position" type="text" class="form-control" required placeholder=" banner page" value="<?=$datas[0]['banner_page'];?>" disabled/>
													<div id="error-title" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Position <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-position" name="position" type="text" class="form-control" required placeholder="banner image title" value="<?=$datas[0]['banner_position'];?>" disabled/>
													<div id="error-title" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Title <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-title" name="title" type="text" class="form-control" required placeholder="banner image title" value="<?=$datas[0]['banner_title'];?>" />
													<div id="error-title" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Url <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-link" name="link" type="text" class="form-control" required placeholder="banner url" value="<?=$datas[0]['banner_url'];?>" />
													<div id="error-link" class="is-error"></div>
												</div>
											</div>
											<div class="row">       
			                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong></div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<select id="input-publish" name="publish" class="form-control" >
			                                    		<option value="Publish" <?php if($datas[0]['banner_publish'] == "Publish"){ echo "selected"; }?>>Publish</option>
			                                    		<option value="Not Publish" <?php if($datas[0]['banner_publish'] != "Publish"){ echo "selected"; }?>>Not Publish</option>
			                                    	</select>
			                                    	<div id="error-publish" class="is-error"></div>
			                                    </div>
			                                </div>
											<div class="row">       
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Image Upload <span class="symbol required"></span></strong></div>
												<div class="col-sm-6 col-xs-12 up1">
													<a class="fancybox" href="<?=$global['absolute-url'].$datas[0]['banner_img'];?>">
														<img style="width: 40px;" class="img-circle" src="<?=$global['absolute-url'].$datas[0]['banner_imgThmb'];?>">
													</a>
													<input name="image" type="file" class="text up05" maxlength="256" placeholder="banner image" />
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 hidden-xs up1"></div>
												<div class="col-sm-6 col-xs-12 up1"><strong><span class="help-block marg0"><i class="fa fa-info-circle"></i> <?php echo "Image will be best shown in ". $datas[0]['banner_size']. ".</strong><small>image format has to be jpg, jpeg, gif, png.";?></small></span></div>
											</div>
											
										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['banner_ID'];?>">
											<div class="col-sm-10 col-xs-12 text-right">
												<div class="btn-group">
													<a href="<?=$path['banner'];?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<?php }else{?>
					<div class="row">
						<div class="col-xs-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">There is no data in this page!</h4>
								</div>
							</div>
						</div>
					</div>
					<?php } ?>
					<!-- end: PAGE CONTENT-->
				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>
			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->
		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->
		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->
	</div>
	<?php include("../../packages/footer-js.php");?>
	<!-- Add fancyBox -->
	<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
	<script type="text/javascript">
		function validateForm(){
			var title = $("#input-title").val();
			var link = $("#input-link").val();
			var urlformat = /^HTTP|HTTP|http(s)?:\/\/(www\.)?[A-Za-z0-9]+([\-\.]{1}[A-Za-z0-9]+)*\.[A-Za-z]{2,40}(:[0-9]{1,40})?(\/.*)?$/;
			if(title != ""){
				$("#error-title").html("");
				$("#error-title").hide();
				$("#input-title").removeClass("input-error");
			} else {
				$("#error-title").show();
				$("#error-title").html("<i class='fa fa-warning'></i> This field is required.");
				$("#input-title").addClass("input-error");
				return false;
			}
			if(link != ""){
				if(link.match(urlformat)){
					$("#error-link").html("");
					$("#error-link").hide();
					$("#input-link").removeClass("input-error");
				} else {
					$("#error-link").show();
					$("#error-link").html("<i class='fa fa-warning'></i> Please enter a valid URL.");
					$("#input-link").addClass("input-error");
					return false;
				}
			} else {
				$("#error-link").show();
				$("#error-link").html("<i class='fa fa-warning'></i> This field is required.");
				$("#input-link").addClass("input-error");
				return false;
			}
		}
		$(document).ready(function() {
			$(".fancybox").fancybox({
				padding : 0
			});
		});
		//use session here for alert success/failed
		var alertText = "success/error text"; //teks for alert
		//success alert
		//successAlert(alertText); 
		//error alert
		//errorAlert(alertText); 
		//function confirmation delete
		function confirmDelete(num, text){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "index.php?action=delete&id="+num+"&title="+text;
				} else {
	                   //nothing
	               }
	           });
		}
		</script>
	</body>
	<!-- end: BODY -->
	</html>