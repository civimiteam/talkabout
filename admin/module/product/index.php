<?php 
	include("../../packages/require.php");
	include("../../packages/productFunction.php");//RECURSIVE CATEGORY
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_product.php");
	$curpage="product";
	$page_name = "index.php";
	$tag_name = "tag=$O_tag";
	$cat_name = "cat=$O_cat";
?>
<!DOCTYPE html>
<html lang="en">
	<!-- start: HEAD -->
	<head>
		<title><?=$title['product'];?></title>
		<?php include("../../packages/module-head.php");?>
		<!-- Add fancyBox -->
		<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.css?v=2.1.5" type="text/css" media="screen" />
	</head>
	<!-- end: HEAD -->
	<!-- start: BODY -->
	<body>
		<!-- start: SLIDING BAR (SB) -->
		<?php include("../../parts/part-sliding_bar.php");?>
		<!-- end: SLIDING BAR -->

		<div class="main-wrapper">

			<!-- start: TOPBAR -->
			<?php include("../../parts/part-top_bar.php");?>
			<!-- end: TOPBAR -->

			<!-- start: PAGESLIDE LEFT -->
			<?php include("../../parts/part-pageslide_left.php");?>
			<!-- end: PAGESLIDE LEFT -->

			<!-- start: PAGESLIDE RIGHT -->
			<?php include("../../parts/part-pageslide_right.php");?>
			<!-- end: PAGESLIDE RIGHT -->

			<!-- start: MAIN CONTAINER -->
			<div class="main-container inner">
				<!-- start: PAGE -->
				<div class="main-content">
					
					<div class="container">

						<!-- start: PAGE HEADER -->
						<div class="toolbar row">
							<div class="col-sm-6">
								<div class="page-header">
									<h1>
										Product Management 
										<small>Adding and Editing your product here.</small>
									</h1>
								</div>
							</div>
							<div class="col-sm-6 col-xs-12"></div>
						</div>
						<!-- end: PAGE HEADER -->

						<!-- start: BREADCRUMB -->
						<div class="row">
							<div class="col-md-12">
								<ol class="breadcrumb">
									<li class="active">
										Product Management
									</li>
								</ol>
							</div>
						</div>
						<!-- end: BREADCRUMB -->

						<!-- start: PAGE CONTENT -->
						<div class="row">
							<div class="col-md-12">
								<div class="panel panel-white">
									<div class="panel-heading border-light text-right">
										<a class="btn btn-light-azure" href="<?=$path['product-add'];?>">
											<i class="fa fa-plus"></i> Add New Data
										</a>
									</div>
									<?php if(is_array($datas)){?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												<select name="category" class="form-control" style="width: 200px;max-width: 100%;" onchange="javascript:location.href = this.value;">
													<option value="<?=$path['product'];?>">All Category</option>
													<?php if(is_array($categorys)){ foreach($categorys as $category){?>
													<option <?php if($O_cat == $category['category_ID']){echo "selected";}?> value="<?=$path['product']."?page=1&tag=".$O_tag."&cat=".$category['category_ID'];?>"><?=$category['category_name'];?></option>
													<?php }}?>
												</select>
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Product : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>

										<form name="updateProduct" action="index.php?action=update" method="post" enctype="multipart/form-data">
											<div class="product-list">
												<input type="hidden" name="page" value="<?=$O_page;?>">
												<?php $num=1; foreach($datas as $data){ ?>
												<div class="product-wrap">
													<div class="row">
														<div class="col-xs-12 pad0">
															<div class="row">
																<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12">
																	<a href="<?=$global['absolute-url'].$data['photo_imgLoc'];?>" class="product-img fancybox" style="background-image: url('<?=$global['absolute-url'].$data['photo_imgLocThmb'];?>');"></a>
																</div>
																<div class="col-lg-5 col-md-5 col-sm-5 col-xs-12">
																	<div class="product-title up05"><?=correctDisplay($data['product_name']);?></div>
																	<div class="product-cat up05"><?=correctDisplay($data['category_name']);?></div>
																	<!--<div class="product-cat up05">
																		<?php if($data['brand_img']){ ?>
																			<img src="<?=$global['absolute-url'].$data['brand_img'];?>" style="max-height:50px;border:1px solid #000;"/>
																		<?php }else{?>
																		<span><b>No Brand, please edit and add brand</b></span>
																		<?php } ?>
																	</div>!-->
																	<div class="product-cat up05">
																		<label class="checkbox-inline">
																		  	<input <?php if($data['product_trending'] == 1){echo "checked";}?> type="checkbox" name="tagTrending-<?=$num;?>" value="1"> <span class="label label-success">T</span>
																		</label>
																		<label class="checkbox-inline">
																		  	<input <?php if($data['product_bestSeller'] == 1){echo "checked";}?> type="checkbox" name="tagBestSeller-<?=$num;?>" value="1"> <span class="label label-warning">BS</span>
																		</label>
																		<!--<label class="checkbox-inline" style="display: none;">
																		  	<input <?php if($data['product_freeShipping'] == 1){echo "checked";}?> type="checkbox" name="tagFreeShipping-<?=$num;?>" value="1"> <span class="label label-danger">FS</span>
																		</label>!-->
																		<label class="checkbox-inline">
																		  	<input <?php if($data['product_limitedStock'] == 1){echo "checked";}?> type="checkbox" name="tagOutOfStock-<?=$num;?>" value="1"> <span class="label label-inverse">OS</span>
																		</label>
																		<!--<label class="checkbox-inline">
																		  	<input <?php if($data['product_limitedEdition'] == 1){echo "checked";}?> type="checkbox" name="tagLimitedEdition-<?=$num;?>" value="1"> <span class="label label-primary">LE</span>
																		</label>!-->
																	</div>
																</div>
																<div class="col-lg-3 col-md-3 col-sm-3 col-xs-12">
																	<div class="input-group up1">
																	  	<span class="input-group-addon" style="color:green;background-color: #ddd;border-color: #ddd;">Rp.</span>
																	  	<input type="text" class="form-control input-sm" name="price[]" value="<?=$data['product_price'];?>">
																	</div>
																	<div class="input-group up1">
																	  	<span class="input-group-addon" style="color:red;background-color: #ddd;border-color: #ddd;">Rp.</span>
																	  	<input type="text" class="form-control input-sm" name="specialPrice[]" value="<?=$data['product_specialPrice'];?>">
																	</div>
																</div>
																<div class="col-lg-2 col-md-2 col-sm-2 col-xs-12 text-center">
																	<input type="hidden" name="id[]" value="<?=$data['product_ID'];?>">
																	<input type="hidden" name="name[]" value="<?=$data['product_name'];?>">
																	<div class="btn-group-vertical up1" role="group">
																		<a href="<?=$path['product-edit'].$data['product_ID']."&page=".$O_page."&".$tag_name."&".$cat_name;?>" class="btn btn-sm btn-blue tooltips" data-placement="top" data-original-title="Edit">
																			<i class="fa fa-edit"></i> Edit
																		</a>
																		<a href="javascript:void(0)" onclick="confirmDelete('<?=$data['product_ID'];?>', '<?=$data['product_name'];?>', '<?=$O_page;?>', '<?=$O_tag;?>', '<?=$O_cat;?>');" class="btn btn-sm btn-red tooltips" data-placement="top" data-original-title="Remove">
																			<i class="fa fa-trash-o fa fa-white"></i> Delete
																		</a>
																	</div>
																</div>
															</div>
														</div>
													</div>
												</div>
												<?php $num++;} ?>
											</div>
											<input type="hidden" name="url" value="<?=$page_name."?page=".$O_page."&".$tag_name."&".$cat_name;?>">
											<div class="product-btn">
												<button type="submit" class="btn btn-success up2">
													<i class="fa fa-check fa fa-white"></i> Change All
												</button>
											</div>
										</form>

										<!-- start pagination -->
										<div class="part-pagination part-pagination-customer" style="border-top: solid 1px #ddd;padding-top: 20px;">
											<ul class="pagination pagination-blue margin-bottom-10">
												<?php
												$batch = getBatchProduct($O_page);
												if($batch < 1){$batch = 1;}
												$prevLimit = 1 +(10*($batch-1));
												$nextLimit = 10 * $batch;

												if($nextLimit > $total_page){
													$nextLimit = $total_page;
												}
												if ($total_page > 1 && $O_page > 1) {
													echo "<li><a href='".$page_name."?page=1"."&".$tag_name."&".$cat_name."'><i class='fa fa-chevron-left'></i><i class='fa fa-chevron-left'></i> First</a></li>";
												}
												if($batch > 1 && $O_page > 10){
													echo "<li><a href='".$page_name."?page=".($prevLimit-1)."&".$tag_name."&".$cat_name."'><i class='fa fa-chevron-left'></i> Previous 10</a></li>";
												}
												for($mon = $prevLimit; $mon <= $nextLimit;$mon++){ ?>
													<li class="<?php if($mon == $O_page){echo 'active';}?>"><a href="<?php echo $page_name."?page=".$mon."&".$tag_name."&".$cat_name;?>" ><?php echo $mon;?></a></li>
													<?php if($mon == $nextLimit && $mon < $total_page){
														if($mon >= $total_page){ 
															$mon -=1;
														}
														echo "<li><a href='".$page_name."?page=".($mon+1)."&".$tag_name."&".$cat_name."'>Next 10 <i class='fa fa-chevron-right'></i></a></li>";
													}                               
												}
												if ($total_page > 1 &&  ($O_page != $total_page)) {
													echo "<li><a href='".$page_name."?page=".$total_page."&".$tag_name."&".$cat_name."'>Last <i class='fa fa-chevron-right'></i><i class='fa fa-chevron-right'></i></a></li>";
												}
												?>
											</ul>
										</div>
										<!-- end pagination -->

									</div>
									<?php }else{?>
									<div class="panel-body">
										<div class="row">
											<div class="col-sm-6 col-xs-12 pad0">
												<select name="category" class="form-control" style="width: 200px;max-width: 100%;" onchange="javascript:location.href = this.value;">
													<option value="<?=$path['product'];?>">All Category</option>
													<?php if(is_array($categorys)){ foreach($categorys as $category){?>
													<option <?php if($O_cat == $category['category_ID']){echo "selected";}?> value="<?=$path['product']."?page=1&tag=".$O_tag."&cat=".$category['category_ID'];?>"><?=$category['category_name'];?></option>
													<?php }}?>
												</select>
											</div>
											<div class="col-sm-6 col-xs-12 pad0">
												<div style="text-align:right;margin: 10px 0;">
													Total Product : <span class="label label-info"><?=$total_data;?></span>
												</div>
											</div>
										</div>
										<div class="row">
											<div style="text-align:left;margin: 10px 0;">
												There is no product right now!
											</div>
										</div>
									</div>
									<?php }?>
								</div>
							</div>
						</div>
						<!-- end: PAGE CONTENT-->

					</div>
					<div class="subviews">
						<div class="subviews-container"></div>
					</div>

					<!-- start: PANEL ADD MODAL FORM -->
					<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header form-header">
									<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
										&times;
									</button>
									<h4 class="modal-title form-title">Add New Product</h4>
								</div>
								<form name="addProduct" action="index.php?action=add" enctype="multipart/form-data" method="post" >
									<div class="modal-body">
										<div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Title *</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="title" type="text" class="form-control" required placeholder="product image title" />
		                                    </div>
		                                </div>
		                                <div class="row">
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Link *</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="link" type="text" class="form-control" required placeholder="product link" />
		                                    </div>
		                                </div>
		                                <div class="row">       
		                                    <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Image Upload *</strong> :</div>
		                                    <div class="col-sm-8 col-xs-12 up1">
		                                    	<input name="image" type="file" class="text" placeholder="product image" required />
		                                    </div>
		                                </div>
		                                <div class="row">
		                                	<div class="col-sm-4 hidden-xs up1"></div>
		                                    <div class="col-sm-6 col-xs-12 up1"><small><span class="help-block"><i class="fa fa-info-circle"></i> <?php echo $infoSize['product'];?></span></small></div>
		                                </div>
									</div>
									<div class="modal-footer f5-bg">
										<div class="btn-group">
											<button type="reset" class="btn btn-default" data-dismiss="modal">
												<i class="fa fa-times"></i> Cancel
											</button>
											<button type="submit" class="btn btn-success">
												<i class="fa fa-check fa fa-white"></i> Create
											</button>
										</div>
									</div>
								</form>
							</div>
							<!-- /.modal-content -->
						</div>
						<!-- /.modal-dialog -->
					</div>
					<!-- /.modal -->
					<!-- end: SPANEL CONFIGURATION MODAL FORM -->

				</div>
				<!-- end: PAGE -->
			</div>
			<!-- end: MAIN CONTAINER -->

			<!-- start: FOOTER -->
			<?php include("../../parts/part-footer.php");?>
			<!-- end: FOOTER -->

			<!-- start: SUBVIEW SAMPLE CONTENTS -->
			<?php include("../../parts/part-sample_content.php");?>
			<!-- end: SUBVIEW SAMPLE CONTENTS -->

		</div>

		<?php include("../../packages/footer-js.php");?>
		<!-- Add fancyBox -->
		<script type="text/javascript" src="<?=$global['absolute-url-admin'];?>packages/fancybox/jquery.fancybox.pack.js?v=2.1.5"></script>
		<script type="text/javascript">
			$(document).ready(function() {
				$(".fancybox").fancybox({
					padding : 0
				});
			});
		</script>
		<script type="text/javascript">
			<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
				<?php if($alert != "success"){ ?>
					//error alert
					errorAlert(alertText);
				<?php } else { ?>
					//success alert
					successAlert(alertText); 
				<?php } ?>
			 
			<?php } ?>

			//function confirmation delete
			function confirmDelete(num, text, page, tag, cat){
				swal({
	                title: "Are you sure?",
	                text: "You will not be able to recover this file!",
	                type: "warning",
	                showCancelButton: true,
	                confirmButtonColor: "#DD6B55",
	                confirmButtonText: "Delete ! ",
	                cancelButtonText: "Cancel !",
	                closeOnConfirm: false,
	                closeOnCancel: true
	            },
	            function (isConfirm) {
	                if (isConfirm) {
	                    window.location.href = "index.php?action=delete&id="+num+"&title="+text+"&page="+page+"&tag="+tag+"&cat="+cat;
	                } else {
	                    //nothing
	                }
	            });
			}
		</script>
	</body>
	<!-- end: BODY -->
</html>