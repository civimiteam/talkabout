<?php 
	include("../../packages/require.php");
	include("../../packages/productFunction.php");//RECURSIVE CATEGORY
	include("../../packages/check_login.php");//USED BY ALL PAGE BUT index.php
	include("controller/controller_product_detail.php");
	$curpage="product";
	$page = "page=$O_page";
	$tag_name = "tag=$O_tag";
	$cat_name = "cat=$O_cat";
?>
<!DOCTYPE html>
<html lang="en">
<!-- start: HEAD -->
<head>
	<title><?=$title['product'];?></title>
	<?php include("../../packages/module-head.php");?>
	<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/css/lightbox.css">
	<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->
	<link href="<?=$global['absolute-url-admin'];?>packages/colorpicker/css/bootstrap-colorpicker.min.css" rel="stylesheet">
	<?php include("../../packages/footer-js.php");?>
    <script src="<?=$global['absolute-url-admin'];?>packages/colorpicker/js/bootstrap-colorpicker.js"></script>
</head>
<!-- end: HEAD -->
<!-- start: BODY -->
<body>
	<!-- start: SLIDING BAR (SB) -->
	<?php include("../../parts/part-sliding_bar.php");?>
	<!-- end: SLIDING BAR -->

	<div class="main-wrapper">

		<!-- start: TOPBAR -->
		<?php include("../../parts/part-top_bar.php");?>
		<!-- end: TOPBAR -->

		<!-- start: PAGESLIDE LEFT -->
		<?php include("../../parts/part-pageslide_left.php");?>
		<!-- end: PAGESLIDE LEFT -->

		<!-- start: PAGESLIDE RIGHT -->
		<?php include("../../parts/part-pageslide_right.php");?>
		<!-- end: PAGESLIDE RIGHT -->

		<!-- start: MAIN CONTAINER -->
		<div class="main-container inner">
			<!-- start: PAGE -->
			<div class="main-content">

				<div class="container">

					<!-- start: PAGE HEADER -->
					<div class="toolbar row">
						<div class="col-sm-6">
							<div class="page-header">
								<h1>
									Product Management 
									<small>Editing your product data here.</small>
								</h1>
							</div>
						</div>
						<div class="col-sm-6 col-xs-12"></div>
					</div>
					<!-- end: PAGE HEADER -->

					<!-- start: BREADCRUMB -->
					<div class="row">
						<div class="col-md-12">
							<ol class="breadcrumb">
								<li>
									<a href="<?=$path['product']."?".$page."&".$tag_name."&".$cat_name;?>">
										Product Management
									</a>
								</li>
								<li class="active">
									<?=$datas[0]['product_name'];?>
								</li>
							</ol>
						</div>
					</div>
					<!-- end: BREADCRUMB -->	

					<!-- start: PAGE CONTENT -->
					<div class="row">
						<div class="col-md-12">
							<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h4 class="panel-title">Product <span class="text-bold">(<?=$datas[0]['product_name'];?>)</span></h4>
								</div>
								<form name="editProduct" action="edit.php?action=edit" enctype="multipart/form-data" method="post" onsubmit="return validateForm();">
									<div class="panel-body">
										<div class="form-body">
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Product Category <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<select id="input-category" name="category" class="form-control">
														<option value="">Choose Category</option>
														<?php if(is_array($categorys)){ foreach($categorys as $category){?>
														<option <?php if($datas[0]['product_ref_categoryID'] == $category['category_ID']){echo "selected";}?> value="<?=$category['category_ID'];?>"><?=$category['category_name'];?></option>
														<?php }}?>
													</select>
                                                	<div id="error-category" class="is-error"></div>
													<!--<small><span class="help-block up05"><i class="fa fa-info-circle"></i> min. 1 category</span></small>!-->
													<!--<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Text yang "bold" sebagai parent category. Tidak bisa dipilih.</span></small>!-->
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Product Name <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input id="input-name" name="name" type="text" class="form-control" placeholder="product name"  value="<?=$datas[0]['product_name'];?>" maxlength="50" />
                                                	<div id="error-name" class="is-error"></div>
                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Product name max 50 chars.</span></small>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Short Description :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<textarea name="shortDesc" class="form-control" rows="3" placeholder="short description" maxlength="75"><?=correctDisplay($datas[0]['product_shortDesc']);?></textarea>
													<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> deskripsi singkat product, hanya tampil di <strong>product list</strong> max 75 chars.</span></small>
												</div>
											</div>
											<div class="row up1">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Product Description <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<textarea id="input-desc" name="desc" class="tinymce form-control" name="desc" rows="5" placeholder="product description"><?=correctDisplay($datas[0]['product_desc']);?></textarea>
													<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> deskripsi lengkap dari product dan tampil di product detail.</span></small>
                                                	<div id="error-desc" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Product Price <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<div class="input-group">
														<span class="input-group-addon" style="color:#000;background-color: #ddd;border-color: #ddd;">Rp.</span>
														<input id="input-price" type="text" class="form-control" name="price" value="<?=$datas[0]['product_price'];?>">
													</div>
                                                	<div id="error-price" class="is-error"></div>
                                                	<small><span class="help-block up05" style="color: red;"><i class="fa fa-info-circle"></i> Harga tidak tampil jika diisi 0.</span></small>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Special Price :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<div class="input-group">
														<span class="input-group-addon" style="color:#000;background-color: #ddd;border-color: #ddd;">Rp.</span>
														<input type="text" class="form-control" name="specialPrice" value="<?=$datas[0]['product_specialPrice'];?>">
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label"><strong>Weight (in Kg) <span class="symbol required"></span></strong> :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<div class="input-group">
														<input id="input-weight" type="text" class="form-control" name="weight" value="<?=$datas[0]['product_weight'];?>">
														<span class="input-group-addon" style="color:#000;background-color: #ddd;border-color: #ddd;">Kg.</span>
													</div>
                                                	<div id="error-weight" class="is-error"></div>
												</div>
											</div>
											<div class="row">
												<div class="col-sm-4 col-xs-12 up1 form-label">Tag :</div>
												<div class="col-sm-6 col-xs-12 up1">
													<input name="tag" type="text" class="form-control" placeholder="i.e. neklace,watch, etc" value="<?=$datas[0]['product_tag'];?>" />
												</div>
											</div>
											<div class="row">       
				                                <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Publish <span class="symbol required"></span></strong> :</div>
			                                    <div class="col-sm-6 col-xs-12 up1">
			                                    	<select id="input-publish" name="publish" class="form-control" >
			                                    		<option <?php if($datas[0]['product_publish'] == "Publish"){echo "selected";}?> value="Publish">Publish</option>
			                                    		<option <?php if($datas[0]['product_publish'] == "Not Publish"){echo "selected";}?> value="Not Publish">Not Publish</option>
			                                    	</select>
			                                    	<div id="error-publish" class="is-error"></div>
			                                    </div>
				                            </div>
											<div class="row-feature">
												<div class="row row-cat">
													<div class="col-sm-4 col-xs-5 up1 form-label hidden-xs">Trending Product :</div>
													<div class="col-sm-6 col-xs-12 up1 xs-pad0">
														<label class="checkbox-inline">
															<input <?php if($datas[0]['product_trending'] == 1){echo "checked";}?> type="checkbox" name="tagTrending" value="1">
															<span class="label label-success">T</span>
														</label>
													</div>
												</div>
												<div class="row row-cat">
													<div class="col-sm-4 col-xs-5 up1 form-label hidden-xs">Best Seller Product :</div>
													<div class="col-sm-6 col-xs-12 up1 xs-pad0">
														<label class="checkbox-inline">
															<input <?php if($datas[0]['product_bestSeller'] == 1){echo "checked";}?> type="checkbox" name="tagBestSeller" value="1">
															<span class="label label-warning">BS</span>
														</label>
													</div>
												</div>
												<!-- <div class="row row-cat" style="display: none;">
													<div class="col-sm-4 col-xs-5 up1 form-label hidden-xs">Free Shipping </div>
													<div class="col-sm-6 col-xs-12 up1 xs-pad0">
														<label class="checkbox-inline">
															<input <?php if($datas[0]['product_freeShipping'] == 1){echo "checked";}?> type="checkbox" name="tagFreeShipping" value="1">
															<span class="label label-danger">FS</span>
														</label>
													</div>
												</div> 
												<div class="row row-cat">
													<div class="col-sm-4 col-xs-5 up1 form-label hidden-xs">Limited Edition </div>
													<div class="col-sm-6 col-xs-12 up1 xs-pad0">
														<label class="checkbox-inline">
															<input <?php if($datas[0]['product_limitedEdition'] == 1){echo "checked";}?> type="checkbox" name="tagLimitedEdition" value="1">
															<span class="label label-primary">LE</span>
														</label>
													</div>
												</div> -->
												<div class="row row-cat">
													<div class="col-sm-4 col-xs-5 up1 form-label hidden-xs">Out Of Stock :</div>
													<div class="col-sm-6 col-xs-12 up1 xs-pad0">
														<label class="checkbox-inline">
															<input <?php if($datas[0]['product_limitedStock'] == 1){echo "checked";}?> type="checkbox" name="tagOutOfStock" value="1">
															<span class="label label-inverse">OS</span>
														</label>
													</div>
												</div>
											</div>
											<hr/>
											<h4 style="margin: 15px 0;">--Photo(s)--</h4>
											<hr/>
											<?php if(is_array($datas[0]['photo'])){?>
											<ul id="Grid" class="list-unstyled up3" style="min-height: auto">
												<?php $num=1; foreach($datas[0]['photo'] as $data_photo){ ?>
												<li class="col-md-3 col-sm-6 col-xs-6 mix category_<?=$num;?> gallery-img" data-cat="<?=$num;?>" style="display: inline-block;">
													<div class="portfolio-item">
														<a class="thumb-info" href="<?=$global['absolute-url'].$data_photo['photo_imgLoc'];?>" data-lightbox="gallery">
															<img src="<?=$global['absolute-url'].$data_photo['photo_imgLocThmb'];?>" class="img-responsive" alt="" width="100%" style="border: solid 1px #ddd;border-bottom: 0 !important;">
														</a>
														<?php if($data_photo['photo_main'] != 1){?>
														<div class="tools tools-bottom">
															<a href="javascript:void(0)" onclick="confirmDeletePhoto('<?=$data_photo['photo_ID'];?>', '<?=$datas[0]['product_ID'];?>', '<?=$O_page;?>', '<?=$O_tag;?>', '<?=$O_cat;?>', '<?=$datas[0]['product_name'];?>');">
																<i class="fa fa-trash-o"></i>
															</a>
														</div>
														<?php }?>
													</div>
													<label class="radio-inline" style="margin: 0 auto !important;width: 100%;padding: 0 !important;">
														<input type="hidden" name="photoID[]" value="<?=$data_photo['photo_ID'];?>">
														<input type="text" maxlength="96" checked="form-control" name="photoTitle[]" value="<?=$data_photo['photo_title'];?>" placeholder="photo title. Max 96 char" style="width: 100%;padding: 9px 12px;">
													</label>
													<label class="radio-inline">
														<input <?php if($data_photo['photo_main'] == 1){echo "checked";}?> type="radio" name="radioPrimary" value="<?=$data_photo['photo_ID'];?>" style="margin: 2px 0 0 -20px;"> Set As Primary
													</label>
												</li>
												<?php } ?>
											</ul>
											<?php }else{?>
											<h5 style="margin: 15px 0;">There is no photo product</h5>
											<?php } ?>

										</div>
									</div>
									<div class="panel-footer">
										<div class="row">
											<input type="hidden" name="id" value="<?=$datas[0]['product_ID'];?>">
											<input type="hidden" name="url" value="<?=$path['product-edit'].$O_id."&".$page."&".$tag_name."&".$cat_name;?>">
											<div class="col-sm-4 col-xs-12 pad0">
												<div class="link-delete">
													<a href="javascript:void(0)" onclick="confirmDelete('<?=$datas[0]['product_ID'];?>', '<?=$datas[0]['product_name'];?>', '<?=$O_page;?>', '<?=$O_tag;?>', '<?=$O_cat;?>');">
														Delete
													</a>
												</div>
											</div>
											<div class="col-sm-8 col-xs-12 pad0 text-right">
												<div class="btn-group text-right hidden-xs">
													<a href="<?=$path['product']."?".$page."&".$tag_name."&".$cat_name;?>" type="reset" class="btn btn-default">
														<i class="fa fa-times"></i> Cancel
													</a>
													<a class="btn btn-info" href="#panel-add" data-toggle="modal" data-target="#panel-add">
														<i class="fa fa-plus"></i> Add Photo(s)
													</a>
													<button type="submit" class="btn btn-success">
														<i class="fa fa-check fa fa-white"></i> Update
													</button>
												</div>
												<div class="btn-group visible-xs">
													<div class="row">
														<div class="col-xs-12 text-right">
															<a href="<?=$path['product']."?".$page."&".$tag_name."&".$cat_name;?>" type="reset" class="btn btn-default">
																<i class="fa fa-times"></i> Cancel
															</a>
															<a class="btn btn-info" href="#panel-add" data-toggle="modal" data-target="#panel-add">
																<i class="fa fa-plus"></i> Add Photo(s)
															</a>
															<button type="submit" class="btn btn-success">
																<i class="fa fa-check fa fa-white"></i> Update
															</button>
														</div>
													</div>

												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
						</div>
					</div>
					<!-- end: PAGE CONTENT-->

				</div>
				<div class="subviews">
					<div class="subviews-container"></div>
				</div>

			</div>
			<!-- end: PAGE -->
		</div>
		<!-- end: MAIN CONTAINER -->

		<!-- start: FOOTER -->
		<?php include("../../parts/part-footer.php");?>
		<!-- end: FOOTER -->

		<!-- start: SUBVIEW SAMPLE CONTENTS -->
		<?php include("../../parts/part-sample_content.php");?>
		<!-- end: SUBVIEW SAMPLE CONTENTS -->

	</div>

	<!-- start: PANEL ADD MODAL FORM -->
	<div class="modal fade" id="panel-add" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header form-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">
						&times;
					</button>
					<h4 class="modal-title form-title">Add More Photo(s)</h4>
				</div>
				<form name="addPhoto" action="edit.php?action=addPhoto" enctype="multipart/form-data" method="post" onsubmit="return validateFormPhoto();">
					<div class="modal-body">
					    <div class="row">       
					        <div class="col-sm-4 col-xs-12 up1 form-label"><strong>Photo(s) </strong></div>
					        <div class="col-sm-8 col-xs-12 up1">
					            <div id="photoField">
					            	<input type="text" maxlength="96" checked="form-control" name="photoTitle[]" value="" placeholder="photo title. Max 96 char">
					                <input id="input-photo" name="photo[]" type="file" class="file" style="display: inline-block;"/>
					                <span><strong><span class="symbol required"></span></strong></span>
					            </div>
                                <div id="error-photo" class="is-error"></div>
					            <small><span class="help-block up1"><i class="fa fa-info-circle"></i> image format has to be jpg, jpeg, gif, png.</span></small>
					        </div>
					    </div>
					</div>
					<div class="modal-footer f5-bg">
						<input type="hidden" name="id" value="<?=$datas[0]['product_ID'];?>">
						<input type="hidden" name="url" value="<?=$path['product-edit'].$O_id."&".$page."&".$tag_name."&".$cat_name;?>">
						<div class="btn-group">
							<button type="reset" class="btn btn-default" data-dismiss="modal">
								<i class="fa fa-times"></i> Cancel
							</button>
							<button type="button" class="btn btn-info" onClick="addInput('photoField');">
								<i class="fa fa-plus"></i> Add Photo
							</button>
							<button type="submit" class="btn btn-success">
								<i class="fa fa-upload fa fa-white"></i> Upload
							</button>
						</div>
					</div>
				</form>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.modal -->
	<!-- end: SPANEL CONFIGURATION MODAL FORM -->

	<!-- start: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/mixitup/src/jquery.mixitup.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/plugins/lightbox2/js/lightbox.min.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>assets/js/pages-gallery.js"></script>
	<script src="<?=$global['absolute-url-admin'];?>packages/tinymce/tinymce.min.js"></script>
		<!-- for select2 in brand  -->
	<link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/js/select2.min.js"></script>
	<!-- end: JAVASCRIPTS REQUIRED FOR THIS PAGE ONLY -->
	<script>
        tinymce.init({
            selector: ".tinymce",
            plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste",
            "textcolor"
            ],
            toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | forecolor backcolor",
            relative_urls: false,
            forced_root_block : false,
            height : 200
        });
    </script>
	<script type="text/javascript">
		function validateForm(){
			var category = $("#input-category").val();
            var name = $("#input-name").val();
            var desc = tinyMCE.get('input-desc').getContent();
            var price = $("#input-price").val();
            var weight = $("#input-weight").val();
            var numFormat = /^[0-9.,\b]+$/;
            var weightFormat = /^[0-9.,\b]+$/;
            
            //if (document.forms["editProduct"]["category[]"].selectedIndex >= 1) {
            if(category != ""){
                $("#error-category").html("");
                $("#error-category").hide();
                $("#input-category").removeClass("input-error");
			} else {
                $("#error-category").show();
                $("#error-category").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-category").addClass("input-error");
                return false;
			}
            if(name != ""){
                $("#error-name").html("");
                $("#error-name").hide();
                $("#input-name").removeClass("input-error");
            } else {
                $("#error-name").show();
                $("#error-name").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-name").addClass("input-error");
                return false;
            }
            if(desc != ""){
                $("#error-desc").html("");
                $("#error-desc").hide();
            } else {
                $("#error-desc").show();
                $("#error-desc").html("<i class='fa fa-warning'></i> This field is required.");
                return false;
            }
            if(price != ""){
                if(price.match(numFormat)){
                    $("#error-price").html("");
                    $("#error-price").hide();
                    $("#input-price").removeClass("input-error");
                } else {
                    $("#error-price").show();
                    $("#error-price").html("<i class='fa fa-warning'></i> This field must contain number only.");
                    $("#input-price").addClass("input-error");
                    return false;
                }
            } else {
                $("#error-price").show();
                $("#error-price").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-price").addClass("input-error");
                return false;
            }
            if(weight != ""){
                if(weight.match(weightFormat)){
                    $("#error-weight").html("");
                    $("#error-weight").hide();
                    $("#input-weight").removeClass("input-error");
                } else {
                    $("#error-weight").show();
                    $("#error-weight").html("<i class='fa fa-warning'></i> This field must contain number, comma(,), and dot(.) only.");
                    $("#input-weight").addClass("input-error");
                    return false;
                }
            } else {
                $("#error-weight").show();
                $("#error-weight").html("<i class='fa fa-warning'></i> This field is required.");
                $("#input-weight").addClass("input-error");
                return false;
            }
        }

		function validateFormPhoto(){
            var photo = $("#input-photo").val();
            if(photo != ""){
                $("#error-photo").html("");
                $("#error-photo").hide();
                $("#input-photo").removeClass("input-error");
            } else {
                $("#error-photo").show();
                $("#error-photo").html("<i class='fa fa-warning'></i> Please insert first photo.");
                $("#input-photo").addClass("input-error");
                return false;
            }
       	}

		<?php if($message != "") { ?>
			//use session here for alert success/failed
			var alertText = "<?=$message;?>"; //teks for alert
			
			<?php if($alert != "success"){ ?>
				//error alert
				errorAlert(alertText);
			<?php } else { ?>
				//success alert
				successAlert(alertText); 
			<?php } ?>
			 
		<?php } ?>

		//function confirmation delete
		function confirmDelete(num, text, page, tag, cat){
			swal({
                title: "Are you sure?",
                text: "You will not be able to recover this file!",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Delete ! ",
                cancelButtonText: "Cancel !",
                closeOnConfirm: false,
                closeOnCancel: true
            },
            function (isConfirm) {
                if (isConfirm) {
                    window.location.href = "index.php?action=delete&id="+num+"&title="+text+"&page="+page+"&tag="+tag+"&cat="+cat;
                } else {
                    //nothing
                }
            });
		}

		//function confirmation delete photo
		function confirmDeletePhoto(num, id, page, tag, cat, title){
			swal({
				title: "Are you sure?",
				text: "You will not be able to recover this file!",
				type: "warning",
				showCancelButton: true,
				confirmButtonColor: "#DD6B55",
				confirmButtonText: "Delete ! ",
				cancelButtonText: "Cancel !",
				closeOnConfirm: false,
				closeOnCancel: true
			},
			function (isConfirm) {
				if (isConfirm) {
					window.location.href = "edit.php?action=deletePhoto&photo_id="+num+"&id="+id+"&page="+page+"&tag="+tag+"&cat="+cat+"&title="+title;
				} else {
	                   //nothing
	               }
	           });
		}
	</script>
</body>
<!-- end: BODY -->
</html>