<?php 
require_once("../model/Connection.php");
$obj_con = new Connection();

require_once("../model/Banner.php");
$obj_banner = new Banner();

//get data
if($_GET['action'] == ""){
	$obj_con->up();

	$result = $obj_banner->get_data();
	var_dump($result);

	$obj_con->down();
}
//insert
else if($_GET['action'] == "insert"){
	$obj_con->up();

	$N_title = "test title";
	$N_link = "test link";
	$N_img = "test img";
	$N_img_thmb = "test img thmb";
	$N_page = "test page";
	$N_size = "test size";

	$result = $obj_banner->insert_data($N_title, $N_link, $N_img, $N_img_thmb, $N_page, $N_size);
	var_dump($result);

	$obj_con->down();
}
//update
else if($_GET['action'] == "update"){
	$obj_con->up();

	$N_id = 25;
	$N_title = "test title123";
	$N_link = "test link123";
	$N_img = "test img123";
	$N_img_thmb = "test img thmb123";
	$N_page = "test page123";
	$N_size = "test size123";

	$result = $obj_banner->update_data_image($N_id, $N_title, $N_link, $N_img, $N_img_thmb, $N_page, $N_size);
	//$result = $obj_banner->update_data($N_id, $N_title, $N_link, $N_page, $N_size);
	var_dump($result);

	$obj_con->down();
}
//get data detail
else if($_GET['action'] == "detail" && $_GET['id'] != ""){
	$obj_con->up();

	$O_id = $_GET['id'];
	$result = $obj_banner->get_data_detail($O_id);
	var_dump($result);

	$obj_con->down();
}
//delete
else if($_GET['action'] == "delete" && $_GET['id'] != ""){
	$obj_con->up();

	$O_id = $_GET['id'];
	$result = $obj_banner->delete_data($O_id);
	var_dump($result);

	$obj_con->down();
}
//error
else{
	echo "error";
}
?>