<?php
require_once($global['root-url']."model/Connection.php");
$obj_con = new Connection();

require_once($global['root-url']."model/User.php");
$obj_user = new User();

if(isset($_GET['action'])){

    if($_GET['action'] == 'login'){
        $obj_con->up();

        $O_username = mysql_real_escape_string(check_input($_POST['username']));
        $O_password = mysql_real_escape_string(check_input($_POST['password']));

        $result = $obj_user->login($O_username, $O_password);
    	if($result == 1){
            header("Location:{$path['user']}");
    	}else{
    	    header("Location:index.php?unauthorized&attempt=1");
    	}

        $obj_con->down();
    }
}
?>