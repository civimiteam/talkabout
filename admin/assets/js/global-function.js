//success alert
function successAlert(text){
  Command: toastr["success"](text)
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
}
//error alert
function errorAlert(text){
  Command: toastr["error"](text)
  toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut"
  }
}
var counter = 1;
var limit = 6;
var num = 1;
function addInput(divName){
 if (counter == limit)  {
  alert("You may upload as many photos as you like, but you can only upload "+counter+" photos at a time.");
}
else {
  var newdiv = document.createElement('div');
  newdiv.id = "Photo"+ (num + 1); 
  newdiv.innerHTML = "<br/><input type='text' maxlength='96' checked='form-control' name='photoTitle[]' value='' placeholder='photo title. Max 96 char'>";
  newdiv.innerHTML += "<input name='photo[]' type='file' class='file' style='display:inline-block;' /></div>";
  newdiv.innerHTML += "<a href='#js' title='remove field' onClick=\"removeInput('Photo" + (num + 1) + "');\" ><i class='fa fa-remove' style='color:#444;'></i></a>";
  document.getElementById(divName).appendChild(newdiv);
  counter++;
  num++;
}
}
function removeInput(childId)  
{  
  var ele = document.getElementById(childId);  
  
  var parentEle = document.getElementById('photoField');  
  
  parentEle.removeChild(ele);  
  counter--;
}