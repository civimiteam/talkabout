<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/Customer.php");
$obj_cust = new Customer(); 

require_once("../../../model/PO.php");
$obj_po = new PO(); 

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();
    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }
    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_cust->get_data_detail($O_id);
    $data_pos = $obj_po->get_po_by_customer($O_page, $O_id);
    
    if(is_array($data_pos)){
        $total_data = $data_pos[0]['total_data_all'];
        $total_page = $data_pos[0]['total_page'];
    }else{
        $total_data = 0;
        $total_page = 0;
    }

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} else if(isset($_GET['action'])){

    if($_GET['action'] == "edit"){
        $obj_con->up();

        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_name = mysql_real_escape_string(check_input($_POST['name']));
        $N_phone = mysql_real_escape_string(check_input($_POST['phone']));
        $N_fax = mysql_real_escape_string(check_input($_POST['fax']));
        $N_note = mysql_real_escape_string(check_input($_POST['note']));

        $result = $obj_cust->update_data($N_id, $N_name, $N_phone, $N_fax, $N_note);
        if($result <= 0){
            $message = "Something is wrong with your submission.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Customer <i><b>'" . $N_name . "'</b></i> has been succesfully edited.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
    
        $_SESSION['status'] = $message;
        header("Location:edit.php?id=".$N_id);
        $obj_con->down();
    }
}
?>