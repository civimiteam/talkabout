<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();
require_once("../../../model/AT_Wishlist.php");
$obj_wishlist = new AT_Wishlist();

if(!isset($_GET['action'])){

    $obj_con->up();
    error_reporting(E_ALL^E_NOTICE); //remove notice

    //get list wishlist
    $data_wishlists = $obj_wishlist->get_data_list();
    //var_dump($data_wishlists);
    
    if(is_array($data_wishlists)){
        $total_data = $data_wishlists[0]['total_data'];
    }else{
        $total_data = 0;
    }
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();

} 