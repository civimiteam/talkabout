<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();
require_once("../../../model/Brand.php");
$obj_brand = new Brand(); 
if(!isset($_GET['action'])){
    $obj_con->up();
    
    $datas = $obj_brand->get_data();
    if(is_array($datas)){
        $total_data = $datas[0]['total_data'];
    }else{
        $total_data = 0;
    }
    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    } 
    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();
} else if(isset($_GET['action'])){
    if($_GET['action'] == "add"){
        $obj_con->up();
        // $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_title = str_replace("'", "’", $_POST['title']);
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));

        $ran = rand();
        $timestamp = time();
        $Photo_ImgLink = "uploads/brand/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        $ThmbPhoto_ImgLink = "uploads/brand-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
        if ((($_FILES["image"]["type"] == "image/gif") || ($_FILES["image"]["type"] == "image/jpeg") || ($_FILES["image"]["type"] == "image/jpg") || ($_FILES["image"]["type"] == "image/JPG") || ($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")) {
        require_once("../../packages/imageProcessor.php");
        if ($_FILES["image"]["error"] > 0) {
            //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
        } else {
            $file = $_FILES["image"]["tmp_name"];
            //Resizing images
            if (true !== ($pic_error = @resampimage(1300, 600, "$file", "../../../$Photo_ImgLink", 0))) {
                echo $pic_error;
                unlink($Photo_ImgLink);
            }
            if (true !== ($pic_error1 = @resampimagethmb("$file", "../../../$ThmbPhoto_ImgLink", 0))) {
                echo $pic_error1;
                unlink($ThmbPhoto_ImgLink);
            }
            $result = $obj_brand->insert_data($N_title, $Photo_ImgLink, $ThmbPhoto_ImgLink, $N_publish);
            if($result <= 0){
                $message = "Something is wrong with your submission.<br />";
                $_SESSION['alert'] = "error";
            }else if($result == 1){
                $message = "Brand <i><b>'" . $N_title . "'</b></i> has been succesfully added.<br />";
                $_SESSION['alert'] = "success";
            }else{
                $_SESSION['alert'] = "error";
                die();
            }
        }
    }else if ($_FILES["image"]["name"] != "") {
        $message = "Something is wrong with your uploaded brand image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
        $_SESSION['alert'] = "error";
    }
    $_SESSION['status'] = $message;
    header("Location:index.php");
    $obj_con->down();
    
    } else if($_GET['action'] == "delete"){
        $obj_con->up();
        $O_id = mysql_real_escape_string(check_input($_GET['id']));
        $O_title = mysql_real_escape_string(check_input($_GET['title']));
        
        $result = $obj_brand->delete_data($O_id);
        if($result <= 0){
            $message = "Something is wrong while deleting the Data<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "Brand <b><i>'" . $O_title . "'</i></b> has been deleted successfully.<br />";
            $_SESSION['alert'] = "success";
        }
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>