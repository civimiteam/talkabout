<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/Blog.php");
$obj_blog = new Blog();

require_once("../../../model/Blog_Category.php");
$obj_cat = new Blog_Category();

if(!isset($_GET['action']) && $_GET['id'] != ""){
    $obj_con->up();   

    $O_id = mysql_real_escape_string(check_input($_GET['id']));
    $datas = $obj_blog->get_data_detail($O_id);
    $data_categorys = $obj_cat->get_list();

    $obj_con->down();

} else if(isset($_GET['action'])){
    if($_GET['action'] == "edit"){
        $obj_con->up();

        $N_id = mysql_real_escape_string(check_input($_POST['id']));
        $N_category_id = mysql_real_escape_string(check_input($_POST['category']));
        $N_title = mysql_real_escape_string(check_input($_POST['title']));
        $N_desc = str_replace("'", "’", $_POST['desc']);
        $N_writer = mysql_real_escape_string(check_input($_POST['writer']));
        $N_writer_url = mysql_real_escape_string(check_input($_POST['writer_link']));
        $N_video = mysql_real_escape_string(check_input($_POST['video']));
        $N_publish = mysql_real_escape_string(check_input($_POST['publish']));

        if($_FILES["image"]["name"]!=null){
            $ran = rand();
            $timestamp = time();
            $Photo_ImgLink = "uploads/blog/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            $ThmbPhoto_ImgLink = "uploads/blog-thmb/" . $timestamp . $ran . cleanSpace($_FILES["image"]["name"]);
            if ((($_FILES["image"]["type"] == "image/gif")|| ($_FILES["image"]["type"] == "image/jpeg")|| ($_FILES["image"]["type"] == "image/jpg")||($_FILES["image"]["type"] == "image/JPG")||($_FILES["image"]["type"] == "image/png")) && ($_FILES["image"]["size"] < 10048576) && ($_FILES["image"]["name"] != "")){
                require_once("../../packages/imageProcessor.php");
                if ($_FILES["image"]["error"] > 0){
                    //echo "Return Code: " . $_FILES["file"]["error"][$k] . "<br />";
                }else{
                    $file = $_FILES["image"]["tmp_name"];
                    
                    //Resizing images
                    if (true !== ($pic_error = @resampimage(1300,600,"$file","../../../$Photo_ImgLink",0))) {
                        echo $pic_error;
                        unlink($Photo_ImgLink);
                    }
                    
                    if (true !== ($pic_error1 = @resampimagethmb("$file","../../../$ThmbPhoto_ImgLink",0))) {
                        echo $pic_error1;
                        unlink($ThmbPhoto_ImgLink);
                    }

                    $result = $obj_blog->update_data_image($N_id, $N_category_id, $N_title, $N_desc, $N_writer, $N_writer_url, $N_video, $Photo_ImgLink, $ThmbPhoto_ImgLink, $N_publish);
                    if($result <= 0){
                        $message = "Something is wrong with your submission.<br />";
                        $_SESSION['alert'] = "error";
                    }else if($result == 1){
                        $message = "Blog News <i><b>'" . $N_title . "'</b></i> has been succesfully edited.<br />";
                        $_SESSION['alert'] = "success";
                    }else{
                        $_SESSION['alert'] = "error";
                        die();
                    }
                }
            }
            else if($_FILES["image"]["name"] != ""){
                $message = "Something is wrong with your uploaded blog news image, please make sure that the format is (jpg, png, gif) and the size is less than 9.5MB.<br />";
                $_SESSION['alert'] = "error";
            }
        }else{
            $result = $obj_blog->update_data($N_id, $N_category_id, $N_title, $N_desc, $N_writer, $N_writer_url, $N_video, $N_publish);
            if($result <= 0){
                $message = "Something is wrong with your submission.<br />";
                $_SESSION['alert'] = "error";
            }else if($result == 1){
                $message = "Blog News <i><b>'" . $N_title . "'</b></i> has been succesfully edited.<br />";
                $_SESSION['alert'] = "success";
            }else{
                $_SESSION['alert'] = "error";
                die();
            }
        }
    
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?>