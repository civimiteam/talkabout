<?php 
require_once("../../../model/Connection.php");
$obj_con = new Connection();

require_once("../../../model/Shipping.php");
$obj_shipping = new Shipping();

$file_json = "../../../uploads/json/shipping_name.json";

if(!isset($_GET['action'])){
    $obj_con->up();
    
    $json = json_decode(file_get_contents($file_json),TRUE);
    
    $O_shippingName1 = $json['Shipping_name_1'];
    $O_shippingName2 = $json['Shipping_name_2'];
    $O_shippingName3 = $json['Shipping_name_3'];
    $O_shippingName4 = $json['Shipping_name_4'];

    $O_page = 1;
    if(isset($_GET['page'])){
        $O_page = mysql_real_escape_string(check_input($_GET['page']));
    }

    $datas = $obj_shipping->get_data_by_page($O_page);
    //var_dump($datas);
    if(is_array($datas)){
        $total_data = $datas[0]['total_data_all'];
        $total_page = $datas[0]['total_page'];
    }else{
        $total_data = 0;
        $total_page = 0;
    }

    if(isset($_SESSION['status'])){
        $message = $_SESSION['status'];
        unset($_SESSION['status']);
    } else {
        $message = "";
    }

    if(isset($_SESSION['alert'])){
        $alert = $_SESSION['alert'];
        unset($_SESSION['alert']);
    } else {
        $alert = "";
    }
    
    $obj_con->down();
} else if(isset($_GET['action'])){

    if($_GET['action'] == "add"){
        $obj_con->up();

        $N_shippingCity = mysql_real_escape_string(check_input($_POST['Shipping_city']));
        $N_shippingCost1 = mysql_real_escape_string(check_input($_POST['Shipping_cost_1']));
        $N_shippingCost2 = mysql_real_escape_string(check_input($_POST['Shipping_cost_2']));
        $N_shippingCost3 = mysql_real_escape_string(check_input($_POST['Shipping_cost_3']));
        $N_shippingCost4 = mysql_real_escape_string(check_input($_POST['Shipping_cost_4']));


        $result = $obj_shipping->insert_data($N_shippingCity, $N_shippingCost1, $N_shippingCost2, $N_shippingCost3, $N_shippingCost4);
        if($result <= 0){
            $message = "There has been error on shipping creation.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "$N_shippingCity has been successfully created.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }
      
        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    } 
    else if($_GET['action'] == "setName"){
        $obj_con->up();

        $N_shippingName1 = mysql_real_escape_string(check_input($_POST['Shipping_name_1']));
        $N_shippingName2 = mysql_real_escape_string(check_input($_POST['Shipping_name_2']));
        $N_shippingName3 = mysql_real_escape_string(check_input($_POST['Shipping_name_3']));
        $N_shippingName4 = mysql_real_escape_string(check_input($_POST['Shipping_name_4']));
        
        $data['Shipping_name_1'] = $N_shippingName1;
        $data['Shipping_name_2'] = $N_shippingName2;
        $data['Shipping_name_3'] = $N_shippingName3;
        $data['Shipping_name_4'] = $N_shippingName4;

        file_put_contents($file_json, json_encode($data,TRUE));
        $result = 1;
        if($result == 1){
            $message = "Shipping name has been successfully updated.";
            $_SESSION['alert'] = "success";
        }else{
            $message = "Shipping name failed to update!";
            $_SESSION['alert'] = "error";
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
    else if($_GET['action'] == "delete"){
        $obj_con->up();

        $N_shippingID = mysql_real_escape_string(check_input($_REQUEST["id"]));

        $result = $obj_shipping->delete_data($N_shippingID);
        if($result <= 0){
            $message = "Something is incorrect with the shipping deletion.<br />";
            $_SESSION['alert'] = "error";
        }else if($result == 1){
            $message = "The shipping has been succefully deleted.<br />";
            $_SESSION['alert'] = "success";
        }else{
            $_SESSION['alert'] = "error";
            die();
        }

        $_SESSION['status'] = $message;
        header("Location:index.php");
        $obj_con->down();
    }
}
?> 