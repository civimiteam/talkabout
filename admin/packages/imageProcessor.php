<?php
$absolute_path = "../images/photos"; //Absolute path to where files are uploaded
$thumb_path = "../images/thmb-photos";  //Absolute path to where thumbs are to be stored if you want this
$size_limit = "yes"; //do you want a size limit yes or no.
$limit_size = "10048576"; //How big do you want size limit to be in bytes
$limit_ext = "yes"; //do you want to limit the extensions of files uploaded
$ext_count = "3"; //total number of extensions in array below
$extensions = array(".jpg", ".png", ".gif"); //List extensions you want files uploaded to be

function resampimage($maxwidth, $maxheight, $sourcefile, $destLoc, $imgcomp=0)
   {
   $g_imgcomp=100-$imgcomp;
   if(file_exists($sourcefile))
       {
       $g_is=getimagesize($sourcefile);
       $w_adjust = ($maxwidth / $g_is[0]);
       $h_adjust = ($maxheight / $g_is[1]);
       
       //To check the size of the original file; if smaller than the resized size, ignore resize.
       if($w_adjust>=1 && $h_adjust>=1)
            {
            $new_width=($g_is[0]*1);
            $new_height=($g_is[1]*1);
            }
       else //If bigger, then resize
            {
            if($w_adjust <= $h_adjust)
                {
                $new_width=($g_is[0]*$w_adjust);
                $new_height=($g_is[1]*$w_adjust);
                }
            else
                {
                $new_width=($g_is[0]*$h_adjust);
                $new_height=($g_is[1]*$h_adjust);
                }
            }
       	//SEARCHES IMAGE NAME STRING TO SELECT EXTENSION (EVERYTHING AFTER . )
	    $image_type = strtolower(strrchr($destLoc, "."));

	    //SWITCHES THE IMAGE CREATE FUNCTION BASED ON FILE EXTENSION
		switch($image_type) {
			case ".jpg":
				$img_src = imagecreatefromjpeg($sourcefile);
                $img_dst=imagecreatetruecolor($new_width,$new_height);
                imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $new_width, $new_height, $g_is[0], $g_is[1]);
				break;
                        case ".jpeg":
				$img_src = imagecreatefromjpeg($sourcefile);
                $img_dst=imagecreatetruecolor($new_width,$new_height);
                imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $new_width, $new_height, $g_is[0], $g_is[1]);
				break;
			case ".png":
				$img_src = imagecreatefrompng($sourcefile);
                $img_dst=imageResizeAlpha($img_src, $new_width, $new_height);
				break;
			case ".gif":
				$img_src = imagecreatefromgif($sourcefile);
                $img_dst=imageResizeAlpha($img_src, $new_width, $new_height);
				break;
			default:
				echo("<br />Error Invalid Image Type - $image_type");
				die;
				break;
			}
 
       switch($image_type){
        case ".gif": imagegif($img_dst, $destLoc); break;
        case ".jpg": imagejpeg($img_dst, $destLoc, 100); break;
        case ".jpeg": imagejpeg($img_dst, $destLoc, 100); break;
        case ".png": imagepng($img_dst, $destLoc); break;
            }
       imagedestroy($img_dst);
       imagedestroy($img_src);
        return true;
       }
       else
       return false;
   }
   
 /**
* Resize a PNG file with transparency to given dimensions
* and still retain the alpha channel information
* Author: Alex Le - http://www.alexle.net
*/
function imageResizeAlpha(&$src, $w, $h)
{
/* create a new image with the new width and height */
$temp = imagecreatetruecolor($w, $h);

/* making the new image transparent */
$background = imagecolorallocate($temp, 255, 255, 255);
imagefilledrectangle($temp, 0, 0, $w, $h, $background);

imagecolortransparent($temp, $background); // make the new temp image all transparent
imagealphablending($temp, false); // turn off the alpha blending to keep the alpha channel
imagesavealpha( $temp, true ); // Save full alpha
//imageantialias($temp,true);
/* Resize the PNG file */
/* use imagecopyresized to gain some performance but loose some quality */
//imagecopyresized($temp, $src, 0, 0, 0, 0, $w, $h, imagesx($src), imagesy($src));
/* use imagecopyresampled if you concern more about the quality */
imagecopyresampled($temp, $src, 0, 0, 0, 0, $w, $h, imagesx($src), imagesy($src));
return $temp;
}

function resampimagethmb($sourcefile, $destLoc, $imgcomp=0)
{
if(file_exists($sourcefile))
       {
       $g_is=getimagesize($sourcefile);   
$width = $g_is[0];
$height = $g_is[1];

$thumb_width = 200;
$thumb_height = 200;

$original_aspect = $width / $height;
$thumb_aspect = $thumb_width / $thumb_height;

if($original_aspect >= $thumb_aspect) {
   // If image is wider than thumbnail (in aspect ratio sense)
   $new_height = $thumb_height;
   $new_width = $width / ($height / $thumb_height);
} else {
   // If the thumbnail is wider than the image
   $new_width = $thumb_width;
   $new_height = $height / ($width / $thumb_width);
}
//SEARCHES IMAGE NAME STRING TO SELECT EXTENSION (EVERYTHING AFTER . )
	    $image_type = strtolower(strrchr($destLoc, "."));

	    //SWITCHES THE IMAGE CREATE FUNCTION BASED ON FILE EXTENSION
		switch($image_type) {
			case ".jpg":
				$img_src = imagecreatefromjpeg($sourcefile);
                $img_dst=imagecreatetruecolor($new_width,$new_height);
                imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $new_width, $new_height, $g_is[0], $g_is[1]);
				break;
                        case ".jpeg":
				$img_src = imagecreatefromjpeg($sourcefile);
                $img_dst=imagecreatetruecolor($new_width,$new_height);
                imagecopyresampled($img_dst, $img_src, 0, 0, 0, 0, $new_width, $new_height, $g_is[0], $g_is[1]);
				break;
			case ".png":
				$img_src = imagecreatefrompng($sourcefile);
                $img_dst=imageResizeAlpha($img_src, $new_width, $new_height);
				break;
			case ".gif":
				$img_src = imagecreatefromgif($sourcefile);
                $img_dst=imageResizeAlpha($img_src, $new_width, $new_height);
				break;
			default:
				echo("<br />Error Invalid Image Type - $image_type");
				die;
				break;
			}
//set the will be image size for the thumbnail and fill it with white background
$thumb = imagecreatetruecolor($thumb_width, $thumb_height);
$background = imagecolorallocate($thumb, 255, 255, 255);
imagefilledrectangle($thumb, 0, 0, $thumb_width, $thumb_height, $background);
// Resize and crop
imagecopyresampled($thumb,
                   $img_dst,
                   0 - ($new_width - $thumb_width) / 2, // Center the image horizontally
                   0 - ($new_height - $thumb_height) / 2, // Center the image vertically
                   0, 0,
                   $new_width, $new_height,
                   $new_width, $new_height);

switch($image_type){
        case ".gif": imagegif($thumb, $destLoc); break;
        case ".jpg": imagejpeg($thumb, $destLoc, 100); break;
        case ".jpeg": imagejpeg($thumb, $destLoc, 100); break;
        case ".png": imagepng($thumb, $destLoc); break;
            }
       imagedestroy($thumb);
       imagedestroy($img_dst);
       imagedestroy($img_src);
        return true;
       }
       else
       return false;
}