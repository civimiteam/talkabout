<?php 
$global['absolute-url'] = "https://www.eannovate.com/dev/talkabout/";
$global['root-url'] = $_SERVER['DOCUMENT_ROOT']."/dev/talkabout/";
$global['api'] = $global['absolute-url']."api/";
$global['absolute-url-captcha'] = $global['absolute-url']."packages/captcha/captcha.php";
$global['img-url'] = $global['absolute-url'];
$global['absolute-url-admin'] = $global['absolute-url']."admin/";
$global['path-head'] = "packages/head.php";
$global['path-config'] = "packages/front_config.php";
$global['favicon'] = $global['absolute-url']."img/icon/favicon.ico";
$global['logo-mobile'] = $global['absolute-url']."img/logo/volia.png";
$global['logo-desktop'] = $global['absolute-url']."img/logo/volia.png";
$path['home'] = $global['absolute-url'];
$path['login'] = $global['absolute-url-admin']."index.php";
$path['logout'] = "?action=logout";
$seo['company-name'] = "Volia";
$global['copyright'] = "&copy; Volia ".date('Y')." All rights reserved.";
$global['powered'] = date('Y')." &copy; Powered by <a id='copyright-link' href='https://www.eannovate.com' target='_blank'>Eannovate</a>.";
$seo['robot_yes'] = "index, follow";
$seo['robot_no'] = "noindex, nofollow";
//config for smtp email
$smtp['url'] = "www.eannovate.com";
$smtp['to'] = "dean11.cliff@gmail.com";
$smtp['from'] = "developer@eannovate.com";
$smtp['password'] = "Developer88";

// module homepage
$title['homepage'] = $seo['company-name']." | Module Homepage";
$infoSize['homepage'] = "Image will be best shown in 1200px by 450px.<br>image format has to be jpg, jpeg, gif, png.";
$path['homepage'] = $global['absolute-url-admin']."module/homepage/index.php";
$path['homepage-edit'] = $global['absolute-url-admin']."module/homepage/edit.php?id=";

// module team
$title['team'] = $seo['company-name']." | Module Team";
$infoSize['team'] = "Image will be best shown in 1200px by 450px.<br>image format has to be jpg, jpeg, gif, png.";
$path['team'] = $global['absolute-url-admin']."module/team/index.php";
$path['team-add'] = $global['absolute-url-admin']."module/team/insert.php";
$path['team-edit'] = $global['absolute-url-admin']."module/team/edit.php?id=";

/*// module brand
$title['brand'] = $seo['company-name']." | Module Brand";
$infoSize['brand'] = "Image will be best shown in 400px by 400px.<br>image format has to be jpg, jpeg, gif, png.";
$path['brand'] = $global['absolute-url-admin']."module/brand/index.php";
$path['brand-edit'] = $global['absolute-url-admin']."module/brand/edit.php?id=";*/

// module gallery
$title['gallery'] = $seo['company-name']." | Module Gallery";
$infoSize['gallery'] = "Image will be best shown in 1300px by 600px.<br>image format has to be jpg, jpeg, gif, png.";
$path['gallery'] = $global['absolute-url-admin']."module/gallery/index.php";
$path['gallery-edit'] = $global['absolute-url-admin']."module/gallery/edit.php?id=";

// module customer
$title['customer'] = $seo['company-name']." | Module Customer";
$path['customer'] = $global['absolute-url-admin']."module/customer/index.php";
$path['customer-edit'] = $global['absolute-url-admin']."module/customer/edit.php?id=";
$path['customer-address'] = $global['absolute-url-admin']."module/customer/address.php?id=";
$path['customer-order'] = $global['absolute-url-admin']."module/customer/order.php?id=";
$path['customer-wishlist'] = $global['absolute-url-admin']."module/customer/wishlist.php?id=";

// module category
$title['category'] = $seo['company-name']." | Module Category";
$infoSize['category'] = "Suggested image size is (W X H) 1200 X 600 or horizontal-rectangle.";
$path['category'] = $global['absolute-url-admin']."module/category/index.php";
$path['category-edit'] = $global['absolute-url-admin']."module/category/edit.php?id=";

/*// module blog category
$title['blogcat'] = $seo['company-name']." | Module Blog Category";
$infoSize['blogcat'] = "Image will be best shown in 1000px by 400px.<br>image format has to be jpg, jpeg, gif, png.";
$path['blogcat'] = $global['absolute-url-admin']."module/blogcat/index.php";
$path['blogcat-edit'] = $global['absolute-url-admin']."module/blogcat/edit.php?id=";*/

// module coupon
$title['coupon'] = $seo['company-name']." | Module Coupon";
$path['coupon'] = $global['absolute-url-admin']."module/coupon/index.php";
$path['coupon-edit'] = $global['absolute-url-admin']."module/coupon/edit.php?id=";

/*// module blog
$title['blog'] = $seo['company-name']." | Module Blog News";
$infoSize['blog'] = "Image will be best shown in 1000px by 400px.<br>image format has to be jpg, jpeg, gif, png.";
$path['blog'] = $global['absolute-url-admin']."module/blog/index.php";
$path['blog-add'] = $global['absolute-url-admin']."module/blog/insert.php";
$path['blog-edit'] = $global['absolute-url-admin']."module/blog/edit.php?id=";*/

// module product
$title['product'] = $seo['company-name']." | Module Product";
$path['product'] = $global['absolute-url-admin']."module/product/index.php";
$path['product-edit'] = $global['absolute-url-admin']."module/product/edit.php?id=";
$path['product-add'] = $global['absolute-url-admin']."module/product/insert.php";

// module order history
$title['order'] = $seo['company-name']." | Module Order History";
$path['order'] = $global['absolute-url-admin']."module/order/index.php";
$path['order-edit'] = $global['absolute-url-admin']."module/order/edit.php?id=";

/*// module wish list
$title['wishlist'] = $seo['company-name']." | Module Wish List";
$path['wishlist'] = $global['absolute-url-admin']."module/wishlist/index.php";*/

// module confirmation
$title['confirmation'] = $seo['company-name']." | Module Payment Confirmation";
$path['confirmation'] = $global['absolute-url-admin']."module/confirmation/index.php";

// module social
$title['social'] = $seo['company-name']." | Module Social Media";
$path['social'] = $global['absolute-url-admin']."module/social/index.php";

// module shipping
$title['shipping'] = $seo['company-name']." | Module Shipping";
$path['shipping'] = $global['absolute-url-admin']."module/shipping/index.php";
$path['shipping-edit'] = $global['absolute-url-admin']."module/shipping/edit.php?id=";

// module newsletter
$title['newsletter'] = $seo['company-name']." | Module Newsletter";
$path['newsletter'] = $global['absolute-url-admin']."module/newsletter/index.php";
$path['newsletter-export_all'] = $global['absolute-url-admin']."module/newsletter/excel-all_newsletter.php?action=export";
$path['newsletter-active'] = $global['absolute-url-admin']."module/newsletter/active.php";
$path['newsletter-inactive'] = $global['absolute-url-admin']."module/newsletter/inactive.php";

// module banner
$title['banner'] = $seo['company-name']." | Module Banner";
$path['banner'] = $global['absolute-url-admin']."module/banner/index.php";
$path['banner-edit'] = $global['absolute-url-admin']."module/banner/edit.php?id=";
$infoSize['banner'] = "image format has to be jpg, jpeg, gif, png.";

// module content
$title['content'] = $seo['company-name']." | Module Content";
$path['content'] = $global['absolute-url-admin']."module/content/index.php";
$path['content-return'] = $global['absolute-url-admin']."module/content/return.php";
$path['content-terms'] = $global['absolute-url-admin']."module/content/terms.php";
$path['content-how'] = $global['absolute-url-admin']."module/content/how.php";
$path['content-faq'] = $global['absolute-url-admin']."module/content/faq.php";
$path['content-email'] = $global['absolute-url-admin']."module/content/email.php";
$path['content-shipping'] = $global['absolute-url-admin']."module/content/shipping.php";
$path['content-payment'] = $global['absolute-url-admin']."module/content/payment.php";

// module profile
$title['profile'] = $seo['company-name']." | Module Profile";
$path['profile'] = $global['absolute-url-admin']."module/profile/index.php";

// module user
$title['user'] = $seo['company-name']." | Module User Administrator";
$path['user'] = $global['absolute-url-admin']."module/user/index.php";
$path['user-edit'] = $global['absolute-url-admin']."module/user/edit.php?id=";
?>