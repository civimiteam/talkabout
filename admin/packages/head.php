<!-- start: META -->
<meta charset="utf-8" />
<!--[if IE]><meta http-equiv='X-UA-Compatible' content="IE=edge,IE=9,IE=8,chrome=1" /><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimum-scale=1.0, maximum-scale=1.0">
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="apple-mobile-web-app-status-bar-style" content="black">
<meta content="" name="description" />
<meta content="PT. Eannovate Creative Technology - www.eannovate.com" name="author" />
<!-- end: META -->
<!-- start: MAIN CSS -->
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/animate.css/animate.min.css">
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/css/styles.css">
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/css/styles-responsive.css">
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/iCheck/skins/all.css">
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/stylesheets/global-style.css"/>
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/stylesheets/libcvm.css"/>
<link rel="stylesheet" href="<?=$global['absolute-url-admin'];?>assets/plugins/sweetalert/lib/sweet-alert.css"/>
<link rel="shortcut icon" href="<?=$global['favicon'];?>" />
<!--[if IE 7]>
<link rel="stylesheet" href="https://www.cliptheme.com/demo/Eannovate/assets/plugins/font-awesome/css/font-awesome-ie7.min.css">
<![endif]-->
<!-- end: MAIN CSS -->
<!-- start: CSS REQUIRED FOR THIS PAGE ONLY -->
<!-- end: CSS REQUIRED FOR THIS PAGE ONLY -->